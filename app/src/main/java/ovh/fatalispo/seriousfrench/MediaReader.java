package ovh.fatalispo.seriousfrench;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import ovh.fatalispo.seriousfrench.CustomWebview;

public class MediaReader {
	private Context context;
	private static String urlToPlay;
	private static CustomWebview webview;
	private static ProgressBar pbWebView;
	private TextView tvUrl;
	private static ArrayList<String> urlsList;

	public MediaReader(Context context, final String urlToPlay, final CustomWebview webView, final ProgressBar pbWebView, final TextView tvUrl, ArrayList<String> urlsList){
		this.context = context;
		this.webview = webView;
		this.pbWebView = pbWebView;
		this.urlToPlay = urlToPlay;
		this.urlsList = urlsList;
	}

	public MediaReader(final String urlToPlay, final CustomWebview webView, final ProgressBar pbWebView, final ArrayList<String> urlsList){
		this.webview = webView;
		this.pbWebView = pbWebView;
		this.urlToPlay = urlToPlay;
		this.urlsList = urlsList;
	}

	public static void setMedia() {
		WebSettings webSettings = webview.getSettings();
//		webview.setWebChromeClient(new WebChromeClient() {
//			public void onProgressChanged(WebView view, int progress) {
//				pbWebView.setProgress(progress);
//			}
//		});

		webview.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				pbWebView.setVisibility(View.VISIBLE);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				for(String urlFromDB : urlsList) {
					if (url.startsWith(urlFromDB)) {
						// This is my web site, so do not override; let my WebView load the page
						return false;
					}
				}

				// reject anything other
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				if(pbWebView.getVisibility() != View.INVISIBLE) {
					webview.setVisibility(View.VISIBLE);
					pbWebView.setVisibility(View.GONE);
				}
			}
		});
		webSettings.setJavaScriptEnabled(true);
		webview.setZ(1);
		webSettings.setDomStorageEnabled(true);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setSupportZoom(true);
		CookieSyncManager.createInstance(webview.getContext());
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		webview.getSettings().setDisplayZoomControls(false);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.getSettings().setMediaPlaybackRequiresUserGesture(true);
		webview.getSettings().setPluginState(WebSettings.PluginState.ON);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webview.loadUrl(urlToPlay);
	}

	public static void stopMedia() {
		if(webview != null) {
			webview.stopLoading();
			webview.clearCache(true);
		}
	}

	public static void pauseMedia() {
		if(webview != null) {
			webview.reload();
		}
	}
}
