package ovh.fatalispo.seriousfrench;

public class Definition {
	private String name;
	private String contentFr;
	private String contentOther;
	public Definition(String name) {
		this.name = name;
	}

	public Definition(String name, String contentFr,String contentOther) {
		this.name = name;
		this.contentFr = contentFr;
		this.contentOther = contentOther;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentFr() {
		return contentFr;
	}

	public void setContentFr(String content) {
		this.contentFr = content;
	}

	public String getContentOther() {
		return contentOther;
	}

	public void setContentOther(String contentOther) {
		this.contentOther = contentOther;
	}
}
