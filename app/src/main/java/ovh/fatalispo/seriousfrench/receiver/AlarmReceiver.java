package ovh.fatalispo.seriousfrench.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import ovh.fatalispo.seriousfrench.MainActivity;
import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;

public class AlarmReceiver extends BroadcastReceiver {
	String TAG = "AlarmReceiver";
	SharedPreferences sharedPreferences;

	@Override

	public void onReceive(Context context, Intent intent) {
		sharedPreferences = context.getSharedPreferences("settings", 0);

		if (intent.getAction() != null && context != null) {

			if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {

				// Set the alarm here.

				Log.d(TAG, "onReceive: BOOT_COMPLETED");
				String value = sharedPreferences.getString("timeReminder","12:00");
				boolean alarmRecur = sharedPreferences.getBoolean("alarmRecur",false);
				String[] tabvalue = value.split(":");
				Utils.setReminder(context, AlarmReceiver.class, Integer.parseInt(tabvalue[0]), Integer.parseInt(tabvalue[1]), alarmRecur);

				return;

			}

		}
		//Trigger the notification

		Utils.showNotification(context, MainActivity.class,
				context.getString(R.string.alarm_train_title), context.getString(R.string.alarm_train_content));
	}

}
