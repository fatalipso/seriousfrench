package ovh.fatalispo.seriousfrench.radio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;

import java.io.IOException;

import ovh.fatalispo.seriousfrench.MassMedia;


public class RadioManager {
	static Context context;
	static String url;
	static  MediaPlayer mediaPlayer;

	public RadioManager(Context context, String url, MediaPlayer mediaPlayer) {
		this.context = context;
		this.url = url;
		this.mediaPlayer = mediaPlayer;
	}



	public void play() {
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		try {
			mediaPlayer.reset();
			mediaPlayer.setDataSource(url);
			mediaPlayer.prepareAsync();
			mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
				@Override
				public void onPrepared(final MediaPlayer mp) {
					mp.start();
					if(mp.isPlaying())
						MassMedia.SHOW_PROGRESS = 100;
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		// might take long! (for buffering, etc)
	}

	public static class BecomingNoisyReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
				mediaPlayer.stop();
			}
		}
	}



	public void pause() {
		mediaPlayer.pause();
	}

	public void stop() {mediaPlayer.stop();}

	public void mute() {
		mediaPlayer.setVolume(0,0);
	}

	public void unmute() {
		mediaPlayer.setVolume(1,1);
	}

	public void release() {
		mediaPlayer.release();
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		RadioManager.context = context;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		RadioManager.url = url;
	}

	public MediaPlayer getMediaPlayer() {
		return mediaPlayer;
	}

	public void setMediaPlayer(MediaPlayer mediaPlayer) {
		RadioManager.mediaPlayer = mediaPlayer;
	}
}
