package ovh.fatalispo.seriousfrench.radio;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.MediaSessionManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.text.Html;
import android.util.Log;
import android.webkit.WebView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import ovh.fatalispo.seriousfrench.MassMedia;
import ovh.fatalispo.seriousfrench.R;

import static android.content.ContentValues.TAG;
import static android.media.AudioManager.STREAM_MUSIC;
import static ovh.fatalispo.seriousfrench.MassMedia.CHANNEL_ID;

public class RadioPlayer extends Service implements AudioManager.OnAudioFocusChangeListener {

	public static String artistName, title, musicType;
	private SharedPreferences radioConfig;
	private boolean isRunning = false;
	private Looper looper;
	private static WebView wvRadio;
	private MyServiceHandler myServiceHandler;
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor editor;
	private static Context context;
	protected static String url;
	public static MediaPlayer mediaPlayer;
	private static int audioSessionId = 0, resetAction=1, sc_playAction=2, playAction=3, pauseAction=4;
	private static AudioManager audioManager;
	private static MediaSessionManager mediaSessionManager;
	private static MediaSession mediaSession;
	private ServiceReceiver serviceReceiver;
	public static boolean mCancel;


	private PendingIntent pendingIntent(int action) {
		Intent intent = new Intent("service_broadcast");
		intent.putExtra("Control", action);
		return PendingIntent.getBroadcast(this, action, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public class musicBinder extends Binder {
		public RadioPlayer getService() {
			return RadioPlayer.this;
		}
	}

	@Override
	public void onCreate() {
		radioConfig = getSharedPreferences("radio", 0);
		editor = getSharedPreferences("radio", 0).edit();
		HandlerThread handlerthread = new HandlerThread("MyThread", 10);
		handlerthread.start();
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setAudioStreamType(STREAM_MUSIC);
		audioSessionId = mediaPlayer.getAudioSessionId();
		if (mediaSessionManager == null) {
			mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
			mediaSession = new MediaSession(getApplicationContext(), "MusicPlayer");
			mediaSession.setActive(true);
			mediaSession.setCallback(new MediaSession.Callback() {

			});
		}
		IntentFilter head_set_intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
		registerReceiver(becomingNoisyReceiver, head_set_intentFilter);
		serviceReceiver = new ServiceReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("service_broadcast");
		registerReceiver(serviceReceiver, intentFilter);
		looper = handlerthread.getLooper();
		myServiceHandler = new MyServiceHandler(looper);
		isRunning = true;
		super.onCreate();
	}

	@Override
	public IBinder onBind(Intent intent) {
//        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
		sharedPreferences = getApplicationContext().getSharedPreferences("radio", 0);
		String urlRadio = sharedPreferences.getString("radioToPlay", null);
		return new musicBinder();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}


	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(intent != null) {
			url = intent.getStringExtra("radioToPlay");
			if (intent != null) {
				if (intent.getIntExtra("ACTION", -2) == sc_playAction) {
					play(url);
				}
			}
		}
		return super.onStartCommand(intent, flags, startId);
	}

	public static class BecomingNoisyReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
				mediaPlayer.stop();
			}
		}
	}

	public void releaseMedia() {
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public int getAudioSessionId() {
		return audioSessionId;
	}

	public void play(String url) {
		if (requestAudioFocus()) {
			if (mediaPlayer == null) {
				mediaPlayer = new MediaPlayer();
				mediaPlayer.setAudioStreamType(STREAM_MUSIC);
				audioSessionId = mediaPlayer.getAudioSessionId();
			}
			mediaPlayer.reset();
			try {
				new AsyncTask<String, String, ArrayList<String>>() {
					Document doc = null;
					Elements content = null;

					@Override
					protected ArrayList<String> doInBackground(String... url) {
						ArrayList<String> content = new ArrayList<>();
						try {
							doc = Jsoup.connect(radioConfig.getString("currentMetaData", "")).get();
							content.add(doc.select("meta[property=og:title]").attr("content"));
							if (doc.select("meta[name=description]").attr("content").isEmpty()) {
								content.add(doc.select("meta[property=og:description]").attr("content"));
							} else {
								content.add(doc.select("meta[name=description]").attr("content"));
							}


						} catch (Exception e) {
							//DONOTHING
						}
						return content;
					}

					@Override
					protected void onPostExecute(ArrayList<String> s) {
						if (!s.isEmpty()) {
							editor.putString("metaDataTitle", Html.fromHtml(s.get(0)).toString()).apply();
							editor.putString("metaDataDescription", Html.fromHtml(s.get(1)).toString()).apply();
						}
						super.onPostExecute(s);
					}
				}.execute();
			} catch (Exception e) {
				Log.e("error", e.toString());
				MassMedia.tvTitle.setText(" ");
				MassMedia.tvDescription.setText(" ");
			}
			try {
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mediaPlayer.setDataSource(url);
				mediaPlayer.prepareAsync();
				mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					@Override
					public void onPrepared(final MediaPlayer mp) {
						if(cancel()) {
							pause();
							mCancel = false;
							return;
						}
						mp.start();
						if (mp.isPlaying()) MassMedia.SHOW_PROGRESS = 100;
						makeRadioNotification(getApplicationContext(), radioConfig.getString("listeningTo","unknown"));
						createRadioNotificationChannel(getApplicationContext());
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean cancel() {
		return mCancel;
	}


	public void pause() {
		if (mediaPlayer != null) {
			mediaPlayer.stop();
			Intent intent = new Intent("play_broadcast");
			intent.putExtra("UIChange", pauseAction);
			sendBroadcast(intent);
		}
	}

	public void resume(String url) {
		if (mediaPlayer != null) {
			requestAudioFocus();
			mediaPlayer.start();
		} else if (mediaPlayer == null) {
			play(url);
		}
		Intent intent = new Intent("play_broadcast");
		intent.putExtra("UIChange", playAction);
		sendBroadcast(intent);
	}

	public static void mute() {
		mediaPlayer.setVolume(0,0);
	}

	public static void unmute() {
		mediaPlayer.setVolume(1,1);
	}

	public void makeRadioNotification(Context context, String listeningTo) {
		// Create an explicit intent for an Activity in your app
		Intent intent = new Intent(context, MassMedia.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		pendingIntent(playAction);
		Intent startMain = new Intent(RadioPlayer.this, MassMedia.class);
		PendingIntent startMainActivity = PendingIntent.getActivity(RadioPlayer.this, 0, startMain, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID).setSmallIcon(R.drawable.ic_stat_name).setColor(Color.parseColor("#2d7ec3")).setContentTitle(listeningTo).setContentText(context.getString(R.string.close_notif_radio)).setPriority(NotificationCompat.PRIORITY_DEFAULT).setContentIntent(startMainActivity).setAutoCancel(false).addAction(R.drawable.ic_stop_black_24dp,"STOP",pendingIntent(pauseAction)).setDeleteIntent(pendingIntent(pauseAction));

		NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

// notificationId is a unique int for each notification that you must define
		notificationManager.notify(666, builder.build());
	}

	public static void createRadioNotificationChannel(Context context) {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = context.getString(R.string.app_name);
			String description = context.getString(R.string.radio_background);
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
			channel.setDescription(description);
			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}

	@Override
	public void onAudioFocusChange(int i) {
		switch (i) {
			case AudioManager.AUDIOFOCUS_GAIN:
				//The service gained audio focus, so it needs to start playing.
				if (mediaPlayer == null) {

				} else if (!mediaPlayer.isPlaying()) resume(url);
				mediaPlayer.setVolume(1.0f, 1.0f);
				break;
			case AudioManager.AUDIOFOCUS_LOSS:
				removeAudioFocus();
//                audioManager.abandonAudioFocus(this);
				//The service lost audio focus, the user probably moved to playing media on another app, so release the media player.
				if (mediaPlayer != null) {
					if (mediaPlayer.isPlaying()) pause();
				}
//                releaseMedia();
				break;
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
				//Focus lost for a short time, pause the MediaPlayer.
				if (mediaPlayer != null) {
					if (mediaPlayer.isPlaying()) {
						pause();
					}
				}
				break;
			case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
				// Lost focus for a short time, probably a notification arrived on the device, lower the playback volume.
				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
				Boolean lost_focus = sharedPref.getBoolean("lost_focus", false);
				if (lost_focus) {
					if (mediaPlayer != null) {
						if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
					}
				}
				break;
		}
	}

	private boolean requestAudioFocus() {
		try {
			audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			int result = audioManager.requestAudioFocus(this, STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
			if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Could not gain focus
		return false;
	}

	public boolean removeAudioFocus() {
		try {
			if (audioManager != null) {
				return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == audioManager.abandonAudioFocus(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}


	private final class MyServiceHandler extends Handler {
		public MyServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			synchronized (this) {
				for (int i = 0; i < 10; i++) {
					try {
						Log.i(TAG, "MyService running...");
						Thread.sleep(1000);
					} catch (Exception e) {
						Log.i(TAG, e.getMessage());
					}
					if (!isRunning) {
						break;
					}
				}
			}
			//stops the service for the start id.
			stopSelfResult(msg.arg1);
		}
	}

	private class ServiceReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			url = intent.getStringExtra("radioToPlay");
			if (intent.getIntExtra("ACTION", -2) == playAction) {
				play(url);
			}
			if (intent.getIntExtra("Control", 0) == pauseAction) {
				removeAudioFocus();
				pause();
			}
			if (intent.getIntExtra("Control", 0) == sc_playAction) {
				resume(url);
			}
			if (intent.getIntExtra("Control", 0) == resetAction){
				if (mediaPlayer !=null) {
					mediaPlayer.reset();
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseMedia();
		removeAudioFocus();
		unregisterReceiver(serviceReceiver);
		unregisterReceiver(becomingNoisyReceiver);
	}

	//Becoming noisy
	private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(RadioPlayer.this);
			Boolean headset_unplug = sharedPref.getBoolean("headset_unplug", false);
			if (headset_unplug) {
				pause();
			}
		}
	};
}
