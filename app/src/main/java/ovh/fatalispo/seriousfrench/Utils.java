package ovh.fatalispo.seriousfrench;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.LocaleList;
import android.speech.tts.TextToSpeech;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import ovh.fatalispo.seriousfrench.databases.MySQLiteHelper;
import ovh.fatalispo.seriousfrench.databases.streamer.StreamDataSource;
import ovh.fatalispo.seriousfrench.databases.streamer.StreamsLocalDB;
import ovh.fatalispo.seriousfrench.databases.words.WordDataSource;
import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;
import ovh.fatalispo.seriousfrench.scheduler.DailyJobScheduler;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.CONNECTIVITY_SERVICE;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;
import static ovh.fatalispo.seriousfrench.MySettings.DAILY_REMINDER_REQUEST_CODE;

public class Utils {
	public static int compteur = 0, progress;
	public static TextToSpeech repeatTTS;

//	// schedule the start of the service every 10 - 30 seconds
//	@RequiresApi(api = Build.VERSION_CODES.M)
//	public static void scheduleJob(Context context) {
//		compteur++;
//		ComponentName serviceComponent = new ComponentName(context, DailyJobScheduler.class);
//		JobInfo.Builder builder = new JobInfo.Builder(666, serviceComponent);
////		builder.setMinimumLatency(12 * 3600 * 1000); // wait at least
//		Log.d("Job", "job " + compteur);
//		builder.setPeriodic(3600 * 12 * 1000);
//		builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY); // require unmetered network
//		builder.setPersisted(true);
//		//builder.setRequiresDeviceIdle(true); // device should be idle
//		//builder.setRequiresCharging(false); // we don't care if the device is charging or not
//		JobScheduler jobScheduler = null;
//		jobScheduler = context.getSystemService(JobScheduler.class);
//		jobScheduler.schedule(builder.build());
//	}

	public static String sizeScreen(Context mContext) {
		String size="";
		switch (mContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) {
			case Configuration.SCREENLAYOUT_SIZE_NORMAL:
				size = "normal";
			case Configuration.SCREENLAYOUT_SIZE_LARGE:
				size = "large";
			case Configuration.SCREENLAYOUT_SIZE_XLARGE:
				size = "xlarge";
		}
		return size;
	}

	public static ArrayList<Integer> uniqueRand(int minRand, int maxRand) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		ArrayList<Integer> listReturn = new ArrayList<>();
		for (int i=minRand; i<maxRand; i++) {
			list.add(new Integer(i));
		}
		Collections.shuffle(list);
		for (int i=0; i<4; i++) {
			listReturn.add(list.get(i));
		}
		return listReturn;
	}

	public static String languageInit() {
		try {
			for (int i = 0; i < 54; i++) {
				if(Locale.getDefault().getLanguage().equals(LANGAGES[i].getInitial())){
					return Locale.getDefault().getLanguage();
				}
			}
		} catch (Exception e) {
			//DO-NOTHING
		}
		return "en";
	}

	public static void setupUI(View view, final Activity activity) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {
			view.setOnTouchListener(new View.OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					Utils.hideSoftInputKeyboard(activity);
					return false;
				}
			});
		}

		//If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				View innerView = ((ViewGroup) view).getChildAt(i);
				setupUI(innerView,activity);
			}
		}

	}

	public static void gamePointToast(Activity activity, Context context, int points, boolean loseOrWin) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View view = inflater.inflate(R.layout.toast_unlocked_lvl,
				(ViewGroup) activity.findViewById(R.id.relativeLayout1));

		Toast toast = new Toast(context);
		toast.setView(view);
		toast.setGravity(Gravity.TOP,0,150);
		toast.setDuration(4*1000);
		toast.show();
	}

	public static boolean readTheWord(ImageButton ivListenTTS, final String wordToRead, final Context mContext) {
		TextToSpeech.OnInitListener onInitListener = new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				if (status == TextToSpeech.SUCCESS) {
					int ttsLang = repeatTTS.setLanguage(Locale.FRANCE);

					if (ttsLang == TextToSpeech.LANG_MISSING_DATA
							|| ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
						Log.e("TTS", "The Language is not supported!");
					} else {
						Log.i("TTS", "Language Supported.");
					}
					Log.i("TTS", "Initialization success.");
				} else {
					Toast.makeText(mContext, "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
				}
			}
		};
		repeatTTS = new TextToSpeech(mContext, onInitListener);

		if(repeatTTS.getEngines().size()==0) {
			return false;
		}

		ivListenTTS.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("TTS", "button clicked: " + wordToRead);
				int speechStatus = repeatTTS.speak(wordToRead, TextToSpeech.QUEUE_FLUSH, null);

				if (speechStatus == TextToSpeech.ERROR) {
					Log.e("TTS", "Error in converting Text to Speech!");
				}
			}
		});
		return true;
	}

	public static boolean isWifiConnected(Context context, boolean Display) {
		ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (mWifi.isConnected()) return true;
		else {
			if(Display) {
				try {
					Toast.makeText(context, context.getString(R.string.wifi_required), Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					//DONOTHING
				}
			}
			return false;
		}
	}

	public static void hideNavBar(Activity mActivity) {
		final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

		mActivity.getWindow().getDecorView().setSystemUiVisibility(flags);

		// Code below is to handle presses of Volume up or Volume down.
		// Without this, after pressing volume buttons, the navigation bar will
		// show up and won't hide
		final View decorView = mActivity.getWindow().getDecorView();
		decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

			@Override
			public void onSystemUiVisibilityChange(int visibility) {
				if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
					decorView.setSystemUiVisibility(flags);
				}
			}
		});
	}

	public static List<WebData> initializeStreams(Context context, String streamType) {
		StreamDataSource dataSource;
		List<StreamsLocalDB> dBStreamsList = new ArrayList<>();
		List<WebData> streamsList = new ArrayList<WebData>();
		dataSource = new StreamDataSource(context);
		dataSource.open();
		int size_list = dataSource.getAllStreamOfThisType(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_TYPE, streamType).size();
		dBStreamsList = dataSource.getAllStreamOfThisType(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_TYPE, streamType);
		for (int i = 0; i < size_list; i++)
			streamsList.add(new WebData(dBStreamsList.get(i).getName(), dBStreamsList.get(i).getUrl(), loadImageBitmap(context, "Images", dBStreamsList.get(i).getImage()), dBStreamsList.get(i).getMetadata()));
		dataSource.close();
		progress +=5;
		return streamsList;
	}

	public static List<String> initializeUrls(Context context) {
		StreamDataSource dataSource;
		List<StreamsLocalDB> dBStreamsList = new ArrayList<>();
		List<String> urlsList = new ArrayList<String>();
		dataSource = new StreamDataSource(context);
		dataSource.open();
		int size_list = dataSource.getAllUrl(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_TYPE).size();
		dBStreamsList = dataSource.getAllUrl(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_TYPE);
		for (int i = 0; i < size_list; i++)
			urlsList.add(dBStreamsList.get(i).getUrl());
		dataSource.close();
		progress +=5;
		return urlsList;
	}



	public static List<WordLocalDB> initializeWordList(Context context) {
		WordDataSource dataSource;
		List<WordLocalDB> wordLocalDBS = new ArrayList<>();
		List<WordLocalDB> wordLocalDBList = new ArrayList<WordLocalDB>();
		dataSource = new WordDataSource(context);
		dataSource.open();
		if (dataSource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null) != null) {
			wordLocalDBS = dataSource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null);
			for (int i = 0; i < dataSource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null).size(); i++)
				wordLocalDBList.add(new WordLocalDB(wordLocalDBS.get(i).getTargetLg(), wordLocalDBS.get(i).getDate(), wordLocalDBS.get(i).getSourceTxt(), wordLocalDBS.get(i).getTargetTxtGoogle(), wordLocalDBS.get(i).getTargetTxtGlosbe()));
		}
		dataSource.close();
		progress +=5;
		return wordLocalDBList;
	}

	public static void setProgress(int progression) {
		progress += progression;
	}

	public static int getProgress() {
		return progress;
	}

	public static void resetProgress() {
		progress = 70;
	}

	public static Bitmap downloadImageBitmap(String sUrl) {
		Bitmap bitmap = null;
		try {
			InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
			bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
			inputStream.close();
		} catch (Exception e) {
			Log.d("DownloadImage", "Exception 1, Something went wrong!");
			e.printStackTrace();
		}
		return bitmap;
	}

	public static void download(final String url, final File destination) throws IOException {
		final URLConnection connection = new URL(url).openConnection();
		connection.setConnectTimeout(200000);
		connection.setReadTimeout(200000);
		connection.addRequestProperty("User-Agent", "Mozilla/5.0");
		final FileOutputStream output = new FileOutputStream(destination, false);
		final byte[] buffer = new byte[2048];
		int read;
		final InputStream input = connection.getInputStream();
		while((read = input.read(buffer)) > -1)
			output.write(buffer, 0, read);
		output.flush();
		output.close();
		input.close();
	}

	public static String getPDF(String urlToShow) {
		File dir = new File(Environment.getExternalStorageDirectory(),"FrenchImmersion_Books");
		if(!dir.exists()) dir.mkdir();
		File file = new File(dir,urlToShow);
		if(!file.exists()) {
			try {
				file.createNewFile();
				download("https://fatalispo.fr/frenchimmersion/books/" + urlToShow, file);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return null; //added return
	}

	public static void saveImage(Context context, Bitmap b, String directory, String imageName) {
		FileOutputStream foStream;
		try {
			File resDir = context.getDir(directory, Context.MODE_PRIVATE);
			File resFile = new File(resDir, imageName);
			FileOutputStream os = new FileOutputStream(resFile);
			b.compress(Bitmap.CompressFormat.PNG, 100, os);
			os.close();
		} catch (Exception e) {
			Log.d("saveImage", "Exception 2, Something went wrong!");
			e.printStackTrace();
		}
	}

	public static Bitmap loadImageBitmap(Context context, String directory, String imageName) {
		Bitmap bitmap = null;
		FileInputStream fiStream;
		try {
			File resDir = context.getDir(directory, Context.MODE_PRIVATE);
			File resFile = new File(resDir, imageName);
			FileInputStream is = new FileInputStream(resFile);
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
		} catch (Exception e) {
			Log.d("saveImage", "Exception 3, Something went wrong!");
			e.printStackTrace();
		}
		return bitmap;
	}

	public static void hideSoftInputKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}


	public static void trimCache(Context context) {
		try {
			File dir = context.getCacheDir();
			if (dir != null && dir.isDirectory()) {
				deleteDir(dir);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}

	public static void setReminder(Context context,Class<?> cls,int hour, int min, boolean alarmRecur) {

		Calendar calendar = Calendar.getInstance();

		Calendar setcalendar = Calendar.getInstance();

		setcalendar.set(Calendar.HOUR_OF_DAY, hour);

		setcalendar.set(Calendar.MINUTE, min);

		setcalendar.set(Calendar.SECOND, 0);

		if(setcalendar.before(calendar))

			setcalendar.add(Calendar.DATE,1);


		cancelReminder(context,cls);

		// Enable a receiver

		ComponentName receiver = new ComponentName(context, cls);

		PackageManager pm = context.getPackageManager();

		pm.setComponentEnabledSetting(receiver,

				PackageManager.COMPONENT_ENABLED_STATE_ENABLED,

				PackageManager.DONT_KILL_APP);




		Intent intent1 = new Intent(context, cls);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,

				DAILY_REMINDER_REQUEST_CODE, intent1,

				PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

		if(alarmRecur) {
			am.setInexactRepeating(AlarmManager.RTC_WAKEUP, setcalendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
		} else {
			am.set(AlarmManager.RTC_WAKEUP, setcalendar.getTimeInMillis(), pendingIntent);
		}


	}

	public static void showNotification(Context context,Class<?> cls,String title,String content) {

		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


		Intent notificationIntent = new Intent(context, cls);

		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

		stackBuilder.addParentStack(cls);

		stackBuilder.addNextIntent(notificationIntent);


		PendingIntent pendingIntent = stackBuilder.getPendingIntent(

				DAILY_REMINDER_REQUEST_CODE,PendingIntent.FLAG_UPDATE_CURRENT);


		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

		Notification notification = builder.setContentTitle(title)

				.setContentText(content).setAutoCancel(true)

				.setSound(alarmSound).setSmallIcon(R.drawable.ic_alarm_black_24dp)

				.setContentIntent(pendingIntent).build();



		NotificationManager notificationManager = (NotificationManager)

				context.getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(DAILY_REMINDER_REQUEST_CODE, notification);

	}

	public static void cancelReminder(Context context,Class<?> cls) {

		// Disable a receiver

		ComponentName receiver = new ComponentName(context, cls);

		PackageManager pm = context.getPackageManager();

		pm.setComponentEnabledSetting(receiver,

				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,

				PackageManager.DONT_KILL_APP);

		Intent intent1 = new Intent(context, cls);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,

				DAILY_REMINDER_REQUEST_CODE, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

		am.cancel(pendingIntent);

		pendingIntent.cancel();

	}

	public static ContextWrapper wrap(Context context, String language) {
		Resources res = context.getResources();
		Configuration configuration = res.getConfiguration();
		Locale newLocale = new Locale(language);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			configuration.setLocale(newLocale);
			LocaleList localeList = new LocaleList(newLocale);
			LocaleList.setDefault(localeList);
			configuration.setLocales(localeList);
			context = context.createConfigurationContext(configuration);

		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			configuration.setLocale(newLocale);
			context = context.createConfigurationContext(configuration);

		} else {
			configuration.locale = newLocale;
			res.updateConfiguration(configuration, res.getDisplayMetrics());
		}

		return new ContextWrapper(context);
	}


}
