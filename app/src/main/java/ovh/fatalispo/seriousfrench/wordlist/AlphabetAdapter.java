package ovh.fatalispo.seriousfrench.wordlist;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;

public class AlphabetAdapter extends RecyclerView.Adapter<AlphabetAdapter.ViewHolder> {
	private static ArrayList<String> listLetters = new ArrayList<>();
	private static ArrayList<WordLocalDB> wordListToDisplay = new ArrayList<WordLocalDB>();
	private static ArrayList<String> listDataHeader = new ArrayList<>();
	private static HashMap<String, List<String>> _listDataChild = new HashMap<>();
	private static ExpandableListAdapter listAdapter;
	private static ExpandableListView expListView;
	private static Context context;


	public AlphabetAdapter(ArrayList<String> listLetters, ArrayList<WordLocalDB> wordListToDisplay, ExpandableListView expListView,Context context) {
		this.listLetters = listLetters;
		this.wordListToDisplay = wordListToDisplay;
		this.expListView = expListView;
		this.context = context;
	}

	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ceil_abcd, null);
		ViewHolder viewHolder = new ViewHolder(itemLayoutView);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		holder.tvLetter.setText(listLetters.get(position));
		holder.tvLetter.setTextColor(Color.BLACK);
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public TextView tvLetter;

		public ViewHolder(final View itemLayoutView) {
			super(itemLayoutView);
			tvLetter = itemLayoutView.findViewById(R.id.tvLetter);
			itemLayoutView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!listDataHeader.isEmpty()) listDataHeader.clear();
					for (int i = 0; i < wordListToDisplay.size(); i++) {
						String valueOf = String.valueOf(wordListToDisplay.get(i).getSourceTxtFirstLetter());
						if (listLetters.get(getAdapterPosition()).toLowerCase().equals(valueOf.toLowerCase())) {
							listDataHeader.add(wordListToDisplay.get(i).getSourceTxt());
							_listDataChild.put(wordListToDisplay.get(i).getSourceTxt(), Arrays.asList(wordListToDisplay.get(i).getTargetTxtGoogle(), wordListToDisplay.get(i).getTargetTxtGlosbe()));
						}
					}

					listAdapter = new ExpandableListAdapter(context, listDataHeader, _listDataChild);

					expListView.setAdapter(listAdapter);

					expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
						@Override
						public void onGroupExpand(int groupIndex) {
							// Get total group size.
							int groupListSize = listDataHeader.size();

							// Close other expanded group.
							for(int i=0;i < groupListSize; i++) {
								if(i!=groupIndex) {
									expListView.collapseGroup(i);
								}
							}
						}
					});


				}
			});


		}
	}

	@Override
	public int getItemCount() {
		return listLetters.size();
	}
}
