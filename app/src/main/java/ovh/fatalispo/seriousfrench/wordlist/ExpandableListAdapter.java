package ovh.fatalispo.seriousfrench.wordlist;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ovh.fatalispo.seriousfrench.R;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> _listDataChild;
	public static ArrayList<String> wordsToDelete = new ArrayList<>();

	public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
	                         boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item, null);
		}

		TextView tvLgroupWord = (TextView) convertView.findViewById(R.id.lblListItem);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			tvLgroupWord.setText(Html.fromHtml(childText, Html.FROM_HTML_MODE_COMPACT));
			tvLgroupWord.setBackgroundColor(Color.WHITE);
		} else {
			tvLgroupWord.setText(Html.fromHtml(childText));
		}

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
	                         View convertView, final ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		ImageButton ibLgroupDeleteWord = (ImageButton) convertView.findViewById(R.id.ibLgroupDeleteWord);
		TextView tvLgroupWordHeader = (TextView) convertView.findViewById(R.id.tvLgroupWordHeader);

		ibLgroupDeleteWord.setFocusable(false);

		tvLgroupWordHeader.setTypeface(null, Typeface.BOLD);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			tvLgroupWordHeader.setText(Html.fromHtml(headerTitle, Html.FROM_HTML_MODE_COMPACT));
		} else {
			tvLgroupWordHeader.setText(Html.fromHtml(headerTitle));
		}

		final View finalparent = convertView;
		final int finalGroupposition = groupPosition;
		if(isExpanded) {
			ibLgroupDeleteWord.setVisibility(View.VISIBLE);
			ibLgroupDeleteWord.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					wordsToDelete.add(_listDataHeader.get(finalGroupposition));
					Log.d("Title",_listDataHeader.get(finalGroupposition));
					_listDataChild.remove(_listDataHeader.get(finalGroupposition));
					_listDataHeader.remove(finalGroupposition);
					if(!ExpandableListAdapter.wordsToDelete.isEmpty()) {
						WordList.onDelete(wordsToDelete);
					}
					notifyDataSetChanged();
				}
			});
		} else
			ibLgroupDeleteWord.setVisibility(View.INVISIBLE);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
