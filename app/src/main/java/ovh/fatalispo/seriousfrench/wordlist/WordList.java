package ovh.fatalispo.seriousfrench.wordlist;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;

import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;
import ovh.fatalispo.seriousfrench.WelcomeScreen;
import ovh.fatalispo.seriousfrench.databases.words.WordDataSource;
import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;

public class WordList extends Activity {

	private static ArrayList<WordLocalDB> wordList = new ArrayList<>();
	private LinkedList<WordLocalDB> wordListToDisplay = new LinkedList<>();
	private static ArrayList<String> alphabetLetterToShow = new ArrayList<>();
	private static RecyclerView rvAlphabet;
	private static ConstraintLayout rvNoWordlist;
	ExpandableListAdapter listAdapter;
	static ExpandableListView expListView;
	public static int DISPLAY_PROGRESS = 0;
	private static Handler handler;
	public static ProgressBar progressBar;
	public static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Utils.hideNavBar(this);
		setContentView(R.layout.activity_wordlist);

		rvAlphabet = findViewById(R.id.rvAlphabet);
		expListView = findViewById(R.id.lvExp);
		progressBar = findViewById(R.id.progressBarWordList);
		rvNoWordlist = findViewById(R.id.rvNoWordlist);
		context = getApplicationContext();

	}

	@Override
	protected void onResume() {
		super.onResume();

		wordList.addAll(Utils.initializeWordList(getApplicationContext()));

		if(wordList.isEmpty()) rvNoWordlist.setVisibility(View.VISIBLE);
		else rvNoWordlist.setVisibility(View.INVISIBLE);

		for (String letter : WelcomeScreen.theAlphabet) {
			for (int i = 0; i < wordList.size(); i++) {
				String valueOf = String.valueOf(wordList.get(i).getSourceTxtFirstLetter());
				if (letter.toLowerCase().equals(valueOf.toLowerCase())) {
					if (!alphabetLetterToShow.contains(letter)) alphabetLetterToShow.add(letter);
				}
			}
		}
		rvAlphabet.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
		rvAlphabet.setAdapter(new AlphabetAdapter(alphabetLetterToShow, wordList, expListView,getApplicationContext()));

	}

	@Override
	protected void onPause() {
		if (!alphabetLetterToShow.isEmpty()) alphabetLetterToShow.clear();
		if (!wordList.isEmpty()) wordList.clear();

		super.onPause();
	}

	public static void onDelete(final ArrayList<String> toDelete) {
		handler = new Handler();

		progressBar.setVisibility(View.VISIBLE);
		Thread deleting = new Thread(new Runnable() {
			@Override
			public void run() {
				while (DISPLAY_PROGRESS < toDelete.size()) {
					// Update the progress status


					// Try to sleep the thread for 20 milliseconds
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// Update the progress bar
					handler.post(new Runnable() {
						@Override
						public void run() {
							progressBar.setProgress(DISPLAY_PROGRESS);
							if (DISPLAY_PROGRESS == toDelete.size()-1) {
								progressBar.setVisibility(View.INVISIBLE);
								ExpandableListAdapter.wordsToDelete.clear();
							}
						}
					});
				}
				DISPLAY_PROGRESS = 0;
			}
		});

		deleting.start();
		WordDataSource dataSource;
		dataSource = new WordDataSource(context);
		dataSource.open();
		for (String wordToDelete : toDelete) {
			DISPLAY_PROGRESS++;
			dataSource.deleteWord(wordToDelete);
			Toast.makeText(context,"« "+wordToDelete+" »  was deleted",Toast.LENGTH_SHORT).show();
		}
		dataSource.close();
		if(deleting.isAlive()) deleting.interrupt();
		if (!alphabetLetterToShow.isEmpty()) alphabetLetterToShow.clear();
		if (!wordList.isEmpty()) wordList.clear();
		wordList.addAll(Utils.initializeWordList(context));


		for (String letter : WelcomeScreen.theAlphabet) {
			for (int i = 0; i < wordList.size(); i++) {
				String valueOf = String.valueOf(wordList.get(i).getSourceTxtFirstLetter());
				if (letter.toLowerCase().equals(valueOf.toLowerCase())) {
					if (!alphabetLetterToShow.contains(letter)) alphabetLetterToShow.add(letter);
				}
			}
		}

		rvAlphabet.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
		rvAlphabet.setAdapter(new AlphabetAdapter(alphabetLetterToShow, wordList, expListView,context));

	}
}
