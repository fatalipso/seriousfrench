package ovh.fatalispo.seriousfrench.databases.streamer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ovh.fatalispo.seriousfrench.databases.MySQLiteHelper;

import static ovh.fatalispo.seriousfrench.databases.MySQLiteHelper.COLUMN_STREAM_URL;

public class StreamDataSource {
	// Champs de la base de données
	private static SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = {MySQLiteHelper.COLUMN_STREAM_ID, MySQLiteHelper.COLUMN_STREAM_TYPE, MySQLiteHelper.COLUMN_STREAM_NAME, COLUMN_STREAM_URL, MySQLiteHelper.COLUMN_STREAM_IMAGE};

	public StreamDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public synchronized void streamUpdater(StreamsLocalDB DBStreams) {
		ContentValues values = new ContentValues();
		String metadata;
		values.put(MySQLiteHelper.COLUMN_STREAM_ID, "" + DBStreams.getId());
		values.put(MySQLiteHelper.COLUMN_STREAM_TYPE, DBStreams.getType());
		values.put(MySQLiteHelper.COLUMN_STREAM_NAME, DBStreams.getName());
		values.put(COLUMN_STREAM_URL, DBStreams.getUrl());
		values.put(MySQLiteHelper.COLUMN_STREAM_IMAGE, DBStreams.getImage());
		metadata = DBStreams.getMetadata() == null ? "" : DBStreams.getMetadata();
		values.put(MySQLiteHelper.COLUMN_STREAM_METADATA, metadata);
		boolean idIsAlreadyInDB = isDataAlreadyInDB(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_ID, "" + DBStreams.getId());

		if (idIsAlreadyInDB && !isDataAlreadyInDB(MySQLiteHelper.TABLE_STREAMS, COLUMN_STREAM_URL, DBStreams.getUrl()) || idIsAlreadyInDB && !isDataAlreadyInDB(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_NAME, DBStreams.getName()) || idIsAlreadyInDB && !isDataAlreadyInDB(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_IMAGE, DBStreams.getImage()) || idIsAlreadyInDB && DBStreams.getMetadata() != null && !isDataAlreadyInDB(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_METADATA, DBStreams.getMetadata())) {
			database.update(MySQLiteHelper.TABLE_STREAMS, values, "stream_id=" + DBStreams.getId(), null);
			Log.d("DB STREAM Entries", "UPDATED LOCAL DB id: " + DBStreams.getId() + "  name: " + DBStreams.getName() + "  url: " + DBStreams.getUrl() + "  image: " + DBStreams.getImage() + "  metadata: " + DBStreams.getMetadata() + "\n");

		} else if (!idIsAlreadyInDB) {
			Log.d("DB STREAM Entries", "NEW LOCAL DB  id: " + DBStreams.getId() + "  name: " + DBStreams.getName() + "  url: " + DBStreams.getUrl() + "  image: " + DBStreams.getImage() + "  metadata: " + DBStreams.getMetadata() + "\n");
			database.insert(MySQLiteHelper.TABLE_STREAMS, null, values);

		} else
			Log.d("DB STREAM Entries", "ALREADY IN LOCAL DB id: " + DBStreams.getId() + "  name: " + DBStreams.getName() + "  url: " + DBStreams.getUrl() + "  image: " + DBStreams.getImage() + "  metadata: " + DBStreams.getMetadata() + "\n");
	}

	public synchronized void deleteStreams(Context context,Collection<String> idList) {
		if (!idList.isEmpty()) {
			List<String> localDBList = new ArrayList<>();
			Map<String,String> localDBListName = new LinkedHashMap<>();
			Cursor cursor = database.query(true, MySQLiteHelper.TABLE_STREAMS, null, null, null, null, null, null, null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				StreamsLocalDB stream = cursorToStream(cursor);
				localDBList.add(stream.getId());
				localDBListName.put(stream.getId(),stream.getImage());
				cursor.moveToNext();
			}
			if (localDBList.size() > idList.size()) {
				localDBList.removeAll(idList);
				for (String idToDelete : localDBList) {
					Log.d("deleting", "WebData deleted with id: " + idToDelete);
					File file = context.getFileStreamPath(localDBListName.get(idToDelete));
					if (file.delete()) Log.d("deleted", localDBListName.get(idToDelete));
					database.delete(MySQLiteHelper.TABLE_STREAMS, MySQLiteHelper.COLUMN_STREAM_ID + " = " + idToDelete, null);
				}
			}
		}
	}

	public List<StreamsLocalDB> getAllStreamOfThisType(String TableName, String columnName, String fieldValue) {
		List<StreamsLocalDB> streamListDB = new ArrayList<StreamsLocalDB>();
		Cursor cursor = database.query(true, TableName, null, columnName + " LIKE ?", new String[]{fieldValue}, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			streamListDB.add(cursorToStream(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return streamListDB;
	}

	public List<StreamsLocalDB> getAllUrl(String TableName, String columnName) {
		List<StreamsLocalDB> streamListDB = new ArrayList<StreamsLocalDB>();
		Cursor cursor = database.rawQuery("SELECT * FROM "+TableName,new String[]{});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			streamListDB.add(cursorToStream(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return streamListDB;
	}

	private StreamsLocalDB cursorToStream(Cursor cursor) {
		StreamsLocalDB DBStreams = new StreamsLocalDB();
		DBStreams.setId(cursor.getString(0));
		DBStreams.setType(cursor.getString(1));
		DBStreams.setName(cursor.getString(2));
		DBStreams.setUrl(cursor.getString(3));
		DBStreams.setImage(cursor.getString(4));
		DBStreams.setMetadata(cursor.getString(5));
		return DBStreams;
	}

	public synchronized static boolean isDataAlreadyInDB(String TableName, String dbfield, String fieldValue) {
		Cursor cursor = database.query(true, TableName, null, dbfield + " LIKE ?", new String[]{fieldValue}, null, null, null, null);
		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}
}
