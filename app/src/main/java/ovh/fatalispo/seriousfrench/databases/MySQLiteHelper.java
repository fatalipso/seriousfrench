package ovh.fatalispo.seriousfrench.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	//Streamer table
	public static final String TABLE_STREAMS = "urls_stream";

	public static final String COLUMN_STREAM_ID = "stream_id";
	public static final String COLUMN_STREAM_NAME = "stream_nom";
	public static final String COLUMN_STREAM_URL = "stream_url";
	public static final String COLUMN_STREAM_IMAGE = "stream_image";
	public static final String COLUMN_STREAM_TYPE = "stream_type";
	public static final String COLUMN_STREAM_METADATA = "stream_metadata";

	//Users table
	public static final String TABLE_USER = "users_table";

	public static final String COLUMN_USER_ID = "user_id";
	public static final String COLUMN_USER_NAME = "user_nom";
	public static final String COLUMN_USER_PREFERED_LG = "user_prefered_language";
	public static final String COLUMN_USER_POINTS = "user_points";
	public static final String COLUMN_USER_WORDS_LEFT = "user_words_left";
	public static final String COLUMN_USER_RADIO_TIME_SPENT = "user_radio_time";
	public static final String COLUMN_USER_TV_TIME_SPENT = "user_tv_time";
	public static final String COLUMN_USER_VIDEO_TIME_SPENT = "user_video_time";

	//Text translated table
	public static final String TABLE_WORDS = "words_table";

	public static final String COLUMN_WORDS_TRANSLATED_ID = "word_id";
	public static final String COLUMN_WORDS_DATE = "word_date";
	public static final String COLUMN_WORDS_TRANSLATED_TARGET_LANGUAGE = "word_target_lg";
	public static final String COLUMN_WORDS_TRANSLATED_SOURCE_TEXT = "word_source_txt";
	public static final String COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GLOSBE = "word_target_txt_glosbe";
	public static final String COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GOOGLE = "word_target_txt_google";

	private static final String DATABASE_NAME = "stream.db";
	private static final int DATABASE_VERSION = 1;

	// Commande sql pour la création de la base de données
	private static final String CREATE_TABLE_STREAM = "create table " + TABLE_STREAMS + "(" + COLUMN_STREAM_ID + " integer primary key not null, " + COLUMN_STREAM_TYPE + " text not null, " + COLUMN_STREAM_NAME + " text not null, " + COLUMN_STREAM_URL + " text not null, " + COLUMN_STREAM_IMAGE + " text not null, " + COLUMN_STREAM_METADATA + " text not null);";
	private static final String CREATE_TABLE_USER= "create table " + TABLE_USER + "(" + COLUMN_USER_ID + " integer primary key not null, " + COLUMN_USER_NAME + " text not null, " + COLUMN_USER_PREFERED_LG + " text not null, " + COLUMN_USER_POINTS + " text not null, " + COLUMN_USER_WORDS_LEFT + " text not null, " + COLUMN_USER_RADIO_TIME_SPENT + " text not null, " + COLUMN_USER_TV_TIME_SPENT + " text not null, " + COLUMN_USER_VIDEO_TIME_SPENT + " text not null);";
	private static final String CREATE_TABLE_WORDS= "create table " + TABLE_WORDS + "(" + COLUMN_WORDS_TRANSLATED_ID + " integer primary key autoincrement not null, " + COLUMN_WORDS_DATE + " date not null, " + COLUMN_WORDS_TRANSLATED_TARGET_LANGUAGE + " text not null, " + COLUMN_WORDS_TRANSLATED_SOURCE_TEXT + " text not null, " + COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GLOSBE + " text not null, " + COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GOOGLE + " text not null);";


	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(CREATE_TABLE_STREAM);
		database.execSQL(CREATE_TABLE_USER);
		database.execSQL(CREATE_TABLE_WORDS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STREAMS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORDS);
		onCreate(db);
	}
}
