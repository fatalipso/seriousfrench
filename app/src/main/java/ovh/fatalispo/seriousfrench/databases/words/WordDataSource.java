package ovh.fatalispo.seriousfrench.databases.words;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ovh.fatalispo.seriousfrench.databases.MySQLiteHelper;
import ovh.fatalispo.seriousfrench.databases.streamer.StreamsLocalDB;

public class WordDataSource {
	// Champs de la base de données
	private static SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = {MySQLiteHelper.COLUMN_WORDS_TRANSLATED_ID, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_TARGET_LANGUAGE, MySQLiteHelper.COLUMN_WORDS_DATE, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_SOURCE_TEXT, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GLOSBE, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GOOGLE};

	public WordDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public synchronized void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void wordUpdater(WordLocalDB DBWords) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_WORDS_TRANSLATED_TARGET_LANGUAGE, DBWords.getTargetLg());
		values.put(MySQLiteHelper.COLUMN_WORDS_DATE, DBWords.getDate().getTime());
		values.put(MySQLiteHelper.COLUMN_WORDS_TRANSLATED_SOURCE_TEXT, DBWords.getSourceTxt());
		values.put(MySQLiteHelper.COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GLOSBE, DBWords.getTargetTxtGlosbe());
		values.put(MySQLiteHelper.COLUMN_WORDS_TRANSLATED_TARGET_TEXT_GOOGLE, DBWords.getTargetTxtGoogle());
		boolean idIsAlreadyInDB = isDataAlreadyInDB(MySQLiteHelper.TABLE_WORDS, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_SOURCE_TEXT, DBWords.getSourceTxt());

		if (idIsAlreadyInDB) {
			database.update(MySQLiteHelper.TABLE_WORDS, values, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_SOURCE_TEXT + "=?", new String[]{DBWords.getSourceTxt()});
			Log.d("DB WORDS Entries", "UPDATED LOCAL DB Target language: " + DBWords.getTargetLg() + "  source text: " + DBWords.getSourceTxt() + "  target text Glosbe: " + DBWords.getTargetTxtGlosbe() + "  target text Google: " + DBWords.getTargetTxtGoogle() + "\n");

		} else if (!idIsAlreadyInDB) {
			database.insert(MySQLiteHelper.TABLE_WORDS, null, values);
			Log.d("DB WORDS Entries", "NEW LOCAL DB Target language: " + DBWords.getTargetLg() + "  source text: " + DBWords.getSourceTxt() + "  target text Glosbe: " + DBWords.getTargetTxtGlosbe() + "  target text Google: " + DBWords.getTargetTxtGoogle() + "\n");
		} else
			Log.d("DB WORDS Entries", "ALREADY IN LOCAL DB Target language: " + DBWords.getTargetLg() + "  source text: " + DBWords.getSourceTxt() + "  target text Glosbe: " + DBWords.getTargetTxtGlosbe() + "  target text Google: " + DBWords.getTargetTxtGoogle() + "\n");
	}

	public void deleteWord(String wordToDelete) {
		if(isDataAlreadyInDB(MySQLiteHelper.TABLE_WORDS,MySQLiteHelper.COLUMN_WORDS_TRANSLATED_SOURCE_TEXT,wordToDelete)) {
			Log.d("deleting", "Word deleted with word: " + wordToDelete);
			database.delete(MySQLiteHelper.TABLE_WORDS, MySQLiteHelper.COLUMN_WORDS_TRANSLATED_SOURCE_TEXT+ "=?", new String[]{wordToDelete});
		}
	}

	public List<WordLocalDB> getAllStreamOfThisType(String TableName, String columnName, String fieldValue) {
		List<WordLocalDB> wordListDB = new ArrayList<WordLocalDB>();
		Cursor cursor;
		if(columnName == null || fieldValue == null){
			cursor = database.query(true, TableName, null, null, null, null, null, null, null);
		} else {
			cursor = database.query(true, TableName, null, columnName + " LIKE ?", new String[]{fieldValue}, null, null, null, null);
		}
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			wordListDB.add(cursorToWord(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return wordListDB;
	}

	private WordLocalDB cursorToWord(Cursor cursor) {
		WordLocalDB DBWords = new WordLocalDB();
		DBWords.setTargetLg(cursor.getString(1));
		DBWords.setDate( new Date(cursor.getLong(2)*1000));
		DBWords.setSourceTxt(cursor.getString(3));
		DBWords.setTargetTxtGlosbe(cursor.getString(4));
		DBWords.setTargetTxtGoogle(cursor.getString(5));
		return DBWords;
	}

	public synchronized static boolean isDataAlreadyInDB(String TableName, String dbfield, String fieldValue) {
		Cursor cursor = database.query(true, TableName, null, dbfield + " LIKE ?", new String[]{fieldValue}, null, null, null, null);
		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}
}
