package ovh.fatalispo.seriousfrench.databases.users;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ovh.fatalispo.seriousfrench.databases.MySQLiteHelper;
import ovh.fatalispo.seriousfrench.databases.streamer.StreamsLocalDB;

public class UserDataSource {
	// Champs de la base de données
	private static SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = {MySQLiteHelper.COLUMN_USER_ID, MySQLiteHelper.COLUMN_USER_NAME, MySQLiteHelper.COLUMN_USER_PREFERED_LG, MySQLiteHelper.COLUMN_USER_POINTS, MySQLiteHelper.COLUMN_USER_WORDS_LEFT, MySQLiteHelper.COLUMN_USER_RADIO_TIME_SPENT, MySQLiteHelper.COLUMN_USER_TV_TIME_SPENT, MySQLiteHelper.COLUMN_USER_VIDEO_TIME_SPENT};

	public UserDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void userUpdater(UserLocalDB DBUsers) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_USER_ID, "" + DBUsers.getId());
		values.put(MySQLiteHelper.COLUMN_USER_NAME, DBUsers.getName());
		values.put(MySQLiteHelper.COLUMN_USER_PREFERED_LG, DBUsers.getPrefered_language());
		values.put(MySQLiteHelper.COLUMN_USER_POINTS, DBUsers.getPoints());
		values.put(MySQLiteHelper.COLUMN_USER_WORDS_LEFT, DBUsers.getWords_left());
		values.put(MySQLiteHelper.COLUMN_USER_RADIO_TIME_SPENT, DBUsers.getTime_spent_radio());
		values.put(MySQLiteHelper.COLUMN_USER_TV_TIME_SPENT, DBUsers.getTime_spent_tv());
		values.put(MySQLiteHelper.COLUMN_USER_VIDEO_TIME_SPENT, DBUsers.getTime_spent_radio());
		boolean idIsAlreadyInDB = isDataAlreadyInDB(MySQLiteHelper.TABLE_USER, MySQLiteHelper.COLUMN_USER_ID, "" + DBUsers.getId());

		if (idIsAlreadyInDB) {
			database.update(MySQLiteHelper.TABLE_USER, values, "user_id=" + DBUsers.getId(), null);
			Log.d("DB USERS Entries", "UPDATED LOCAL DB id: " + DBUsers.getId() + "  name: " + DBUsers.getName() + "  prefered language: " + DBUsers.getPrefered_language() + "  points: " + DBUsers.getPoints() + "  words left: " + DBUsers.getWords_left() + "  time spent for radio: " + DBUsers.getTime_spent_radio() + "  time spent for TV: " + DBUsers.getTime_spent_tv() + "  time spent for videos: " + DBUsers.getTime_spent_video() +"\n");

		} else if (!idIsAlreadyInDB) {
			database.insert(MySQLiteHelper.TABLE_USER, null, values);
			Log.d("DB USERS Entries", "NEW LOCAL DB id: " + DBUsers.getId() + "  name: " + DBUsers.getName() + "  points: " + DBUsers.getPoints() + "  words left: " + DBUsers.getWords_left() + "  time spent for radio: " + DBUsers.getTime_spent_radio() + "  time spent for TV: " + DBUsers.getTime_spent_tv() + "  time spent for videos: " + DBUsers.getTime_spent_video() +"\n");
		} else
			Log.d("DB USERS Entries", "ALREADY IN LOCAL DB id: " + DBUsers.getId() + "  name: " + DBUsers.getName() + "  points: " + DBUsers.getPoints() + "  words left: " + DBUsers.getWords_left() + "  time spent for radio: " + DBUsers.getTime_spent_radio() + "  time spent for TV: " + DBUsers.getTime_spent_tv() + "  time spent for videos: " + DBUsers.getTime_spent_video() +"\n");
	}

	public void deleteUser(String idToDelete) {
		if(isDataAlreadyInDB(MySQLiteHelper.TABLE_USER,MySQLiteHelper.COLUMN_USER_ID,idToDelete)) {
			Log.d("deleting", "User deleted with id: " + idToDelete);
			database.delete(MySQLiteHelper.TABLE_USER, MySQLiteHelper.COLUMN_USER_ID + " = " + idToDelete, null);
		}
	}

	public List<UserLocalDB> getInfoUser(String TableName, String columnName, String fieldValue) {
		List<UserLocalDB> userListDB = new ArrayList<UserLocalDB>();
		Cursor cursor = database.query(true, TableName, null, columnName + " LIKE ?", new String[]{fieldValue}, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			userListDB.add(cursorToUser(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return userListDB;
	}

	private UserLocalDB cursorToUser(Cursor cursor) {
		UserLocalDB userListDB = new UserLocalDB();
		userListDB.setId(cursor.getString(0));
		userListDB.setName(cursor.getString(1));
		userListDB.setPrefered_language(cursor.getString(2));
		userListDB.setPoints(cursor.getString(3));
		userListDB.setWords_left(cursor.getString(4));
		userListDB.setTime_spent_radio(cursor.getString(5));
		userListDB.setTime_spent_tv(cursor.getString(6));
		userListDB.setTime_spent_video(cursor.getString(7));
		return userListDB;
	}

	public static boolean isDataAlreadyInDB(String TableName, String dbfield, String fieldValue) {
		Cursor cursor = database.query(true, TableName, null, dbfield + " LIKE ?", new String[]{fieldValue}, null, null, null, null);
		if (cursor.getCount() <= 0) {
			cursor.close();
			return false;
		}
		cursor.close();
		return true;
	}
}
