package ovh.fatalispo.seriousfrench.databases.words;

import java.util.Date;

public class WordLocalDB {
	private String targetLg;
	private String sourceTxt;
	private Date queryDate;
	private String targetTxtGlosbe;
	private String targetTxtGoogle;

	public WordLocalDB(){}

	public WordLocalDB(String targetTxtGoogle, String targetTxtGlosbe) {
		this.targetTxtGlosbe = targetTxtGlosbe;
		this.targetTxtGoogle = targetTxtGoogle;
	}

	public WordLocalDB(String targetLg, Date queryDate, String sourceTxt, String targetTxtGoogle, String targetTxtGlosbe) {
		this.targetLg = targetLg;
		this.queryDate = queryDate;
		this.sourceTxt = sourceTxt;
		this.targetTxtGlosbe = targetTxtGlosbe;
		this.targetTxtGoogle = targetTxtGoogle;
	}

	public String getTargetLg() {
		return targetLg;
	}

	public void setTargetLg(String targetLg) {
		this.targetLg = targetLg;
	}

	public String getSourceTxt() {
		return sourceTxt;
	}

	public void setSourceTxt(String sourceTxt) {
		this.sourceTxt = sourceTxt;
	}

	public Date getDate() {
		return queryDate;
	}

	public void setDate(Date queryDate) {
		this.queryDate = queryDate;
	}

	public char getSourceTxtFirstLetter() {
		return sourceTxt.charAt(0);
	}

	public String getTargetTxtGlosbe() {
		return targetTxtGlosbe;
	}

	public void setTargetTxtGlosbe(String targetTxtGlosbe) {
		this.targetTxtGlosbe = targetTxtGlosbe;
	}

	public String getTargetTxtGoogle() {
		return targetTxtGoogle;
	}

	public void setTargetTxtGoogle(String targetTxtGoogle) {
		this.targetTxtGoogle = targetTxtGoogle;
	}
}
