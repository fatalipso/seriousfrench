package ovh.fatalispo.seriousfrench.databases.users;

public class UserLocalDB {
	private String id;
	private String name;
	private String prefered_language;
	private String points;
	private String words_left;
	private String time_spent_radio;
	private String time_spent_tv;
	private String time_spent_video;

	public UserLocalDB() {
	}

	public UserLocalDB(String id, String name, String prefered_language, String points, String words_left, String time_spent_radio, String time_spent_tv, String time_spent_video) {
		this.id = id;
		this.name = name;
		this.prefered_language = prefered_language;
		this.points = points;
		this.words_left = words_left;
		this.time_spent_radio = time_spent_radio;
		this.time_spent_tv = time_spent_tv;
		this.time_spent_video = time_spent_video;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrefered_language() {
		return prefered_language;
	}

	public void setPrefered_language(String prefered_language) {
		this.prefered_language = prefered_language;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public String getWords_left() {
		return words_left;
	}

	public void setWords_left(String words_left) {
		this.words_left = words_left;
	}

	public String getTime_spent_radio() {
		return time_spent_radio;
	}

	public void setTime_spent_radio(String time_spent_radio) {
		this.time_spent_radio = time_spent_radio;
	}

	public String getTime_spent_tv() {
		return time_spent_tv;
	}

	public void setTime_spent_tv(String time_spent_tv) {
		this.time_spent_tv = time_spent_tv;
	}

	public String getTime_spent_video() {
		return time_spent_video;
	}

	public void setTime_spent_video(String time_spent_video) {
		this.time_spent_video = time_spent_video;
	}
}
