package ovh.fatalispo.seriousfrench.databases;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ovh.fatalispo.seriousfrench.Utils;
import ovh.fatalispo.seriousfrench.WebData;
import ovh.fatalispo.seriousfrench.databases.streamer.StreamDataSource;
import ovh.fatalispo.seriousfrench.databases.streamer.StreamsLocalDB;


public class DBAsync extends AsyncTask<ArrayList<WebData>, Integer, ArrayList<WebData>> {
	private StreamDataSource datasource;
	private final static String URL_STREAM = "https://fatalispo.fr/frenchimmersion/mysqlstreams.php";
	public final static String URL_IMAGE = "https://fatalispo.fr/frenchimmersion/images_apps/streams/fr/";
	protected Context mContext;
	public static int progress = 60;
	public static ArrayList<WebData> radiosList = new ArrayList<>(), newspaperList = new ArrayList<>(), videoList = new ArrayList<>(), channelsList = new ArrayList<>();


	public DBAsync(Context mContext) {
		this.mContext = mContext;
	}

	protected List<StreamsLocalDB> dBStreamsList = new ArrayList<>();
	protected ArrayList<WebData> imagesList = new ArrayList<>();

	@Override
	protected ArrayList<WebData> doInBackground(ArrayList<WebData>... string) {
		Document doc = null;
		List<String> tvValues, radioValues, videoValues, newspaper, books;
		Collection<String> idCheckForDeleting = new ArrayList<>();
		String name, urls, image;
		try {
			doc = Jsoup.connect(URL_STREAM).get();
			tvValues = doc.select("ul.tv_channel li").eachText();
			radioValues = doc.select("ul.radio_channel li").eachText();
			videoValues = doc.select("ul.videos li").eachText();
			newspaper = doc.select("ul.newspaper li").eachText();
			books = doc.select("ul.book li").eachText();
			datasource = new StreamDataSource(mContext);
			datasource.open();
			for (int i = 0; i < tvValues.size(); i += 4) {
				datasource.streamUpdater(new StreamsLocalDB("tv_channel", tvValues.get(i), tvValues.get(i + 1), tvValues.get(i + 2), tvValues.get(i + 3)));
				dBStreamsList.add(new StreamsLocalDB("tv_channel", tvValues.get(i), tvValues.get(i + 1), tvValues.get(i + 2), tvValues.get(i + 3)));
				idCheckForDeleting.add(tvValues.get(i));
			}
			for (int i = 0; i < radioValues.size(); i += 5) {
				datasource.streamUpdater(new StreamsLocalDB("radio_channel", radioValues.get(i), radioValues.get(i + 1), radioValues.get(i + 2), radioValues.get(i + 3), radioValues.get(i + 4)));
				dBStreamsList.add(new StreamsLocalDB("radio_channel", radioValues.get(i), radioValues.get(i + 1), radioValues.get(i + 2), radioValues.get(i + 3), radioValues.get(i + 4)));
				idCheckForDeleting.add(radioValues.get(i));
			}
			for (int i = 0; i < videoValues.size(); i += 4) {
				datasource.streamUpdater(new StreamsLocalDB("video", videoValues.get(i), videoValues.get(i + 1), videoValues.get(i + 2), videoValues.get(i + 3)));
				dBStreamsList.add(new StreamsLocalDB("video", videoValues.get(i), videoValues.get(i + 1), videoValues.get(i + 2), videoValues.get(i + 3)));
				idCheckForDeleting.add(videoValues.get(i));
			}
			for (int i = 0; i < newspaper.size(); i += 4) {
				datasource.streamUpdater(new StreamsLocalDB("newspaper", newspaper.get(i), newspaper.get(i + 1), newspaper.get(i + 2), newspaper.get(i + 3)));
				dBStreamsList.add(new StreamsLocalDB("newspaper", newspaper.get(i), newspaper.get(i + 1), newspaper.get(i + 2), newspaper.get(i + 3)));
				idCheckForDeleting.add(newspaper.get(i));
			}
			for (int i = 0; i < books.size(); i += 4) {
				datasource.streamUpdater(new StreamsLocalDB("book", books.get(i), books.get(i + 1), books.get(i + 2), books.get(i + 3)));
				dBStreamsList.add(new StreamsLocalDB("book", books.get(i), books.get(i + 1), books.get(i + 2), books.get(i + 3)));
				idCheckForDeleting.add(books.get(i));
			}
			datasource.deleteStreams(mContext, idCheckForDeleting);
			datasource.close();
		} catch (IOException e) {
			Log.e("create db", "pb while creating: " + e);
		}

		for (int i = 0; i < dBStreamsList.size(); i++) {
			File file = mContext.getFileStreamPath(dBStreamsList.get(i).getImage());
			if (!file.exists()) {
				Log.d("File Check before Save", file.getName() + " doesn't exists!");
				imagesList.add(new WebData(dBStreamsList.get(i).getImage(), URL_IMAGE + dBStreamsList.get(i).getImage(), Utils.downloadImageBitmap(URL_IMAGE + dBStreamsList.get(i).getImage())));
			}
		}
		return imagesList;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}

	protected void onPostExecute(ArrayList<WebData> webDataList) {
		for (int i = 0; i < webDataList.size(); i++) {
			File file = mContext.getFileStreamPath(webDataList.get(i).getName());
			if (!file.exists() && webDataList.get(i).getImage() != null) {
				Utils.saveImage(mContext, webDataList.get(i).getImage(), "Images", webDataList.get(i).getName());
				Log.d("Image Creation", "Nom: " + webDataList.get(i).getName());
			}
		}
	}

}
