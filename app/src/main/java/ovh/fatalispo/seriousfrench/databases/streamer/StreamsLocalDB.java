package ovh.fatalispo.seriousfrench.databases.streamer;

import java.sql.Blob;

public class StreamsLocalDB {
	private String id;
	private String type;
	private String name;
	private String url;
	private String image;
	private byte[] imgBlob;
	private String metadata;

	public StreamsLocalDB() {
	}

	public StreamsLocalDB(String type, String id, String name, String url, String image) {
		this.type = type;
		this.name = name;
		this.url = url;
		this.image = image;
		this.id = id;
	}

	public StreamsLocalDB(String type, String id, String name, String url, String image, String metadata) {
		this.type = type;
		this.name = name;
		this.url = url;
		this.image = image;
		this.id = id;
		this.metadata = metadata;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public byte[] getImgBlob() {
		return imgBlob;
	}

	public void setImgBlob(byte[] imgBlob) {
		this.imgBlob = imgBlob;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
}
