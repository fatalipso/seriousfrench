package ovh.fatalispo.seriousfrench;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ortiz.touchview.TouchImageView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourcePolicy;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ovh.fatalispo.seriousfrench.book.Books;
import ovh.fatalispo.seriousfrench.databases.DBAsync;
import ovh.fatalispo.seriousfrench.game.Game;
import ovh.fatalispo.seriousfrench.wordlist.WordList;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.urlsList;


public class MainActivity extends Activity {

	private Button btnMedia, btnWordList, btnBooks, btnGames, btnMusic, btnSettings;
	private TextView tvUrlMain, tvCopyrightMap;
	private ImageButton ibCloseMap, ibCloseMain, canceButtonBigScreen, ibPreviousWvMain, ibNextWvMain;
	public ImageView ivMainDownloaded, ivMainRope, bLocation, bHistory, ivMainLeft, ivMainRight;
	public TouchImageView ivMainDownloadedBig;
	public final static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 25;
	private MapView map;
	private CustomWebview ivMainDownloadedInfo;
	private ConstraintLayout clWvGlobalMain, clMainDownloadBig;
	private ScrollView svMenu;
	public ProgressBar pbWallpaper, pbWebView2;
	private SharedPreferences sharedPreferences;
	public static String location;
	public final static String URL_DB_WALLPAPER = "https://fatalispo.fr/frenchimmersion/mysqlwallpapers.php";
	public final static String URL_WALLPAPER = "https://fatalispo.fr/frenchimmersion/images_apps/wallpapers/fr/";
	public static int wallpaper_day, wallpaper_year, day;
	public static double wallpaper_lat, wallpaper_lng, beginZoomLvl, maxZoomLvl;
	public static String wallpaper_location, wallpaper_wiki_info, screen_density;
	private int dayRecorded;
	private static MediaReader mediaReader;
	private static ArrayList<OverlayItem> items;

	@RequiresApi(api = Build.VERSION_CODES.N)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreferences = getSharedPreferences("settings", 0);
		DictionnaryFetcher.setLangages();
		Utils.hideNavBar(this);

		Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);
		Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int orientation = getResources().getConfiguration().orientation;
		int width = size.x;
		int height = size.y;
		if (width < height) { // portrait
			setContentView(R.layout.activity_main);
		} else { //land
			setContentView(R.layout.activity_main_land);
		}


		checkExternalStoragePermission(this);

		clWvGlobalMain = findViewById(R.id.clWvGlobalMain);
		clMainDownloadBig = findViewById(R.id.clMainDownloadBig);
		map = findViewById(R.id.map);
		svMenu = findViewById(R.id.svMenu);
		bLocation = findViewById(R.id.bLocation);
		bHistory = findViewById(R.id.bHistory);
		btnMedia = findViewById(R.id.medias);
		btnWordList = findViewById(R.id.wordlist);
		btnBooks = findViewById(R.id.books);
		btnGames = findViewById(R.id.games);
		btnMusic = findViewById(R.id.musics);
		btnSettings = findViewById(R.id.settings);
		ivMainLeft = findViewById(R.id.ivMainLeft);
		ivMainRight = findViewById(R.id.ivMainRight);
		ivMainDownloaded = findViewById(R.id.ivMainDownloaded);
		ivMainDownloadedBig = findViewById(R.id.ivMainDownloadedBig);
		ivMainDownloadedInfo = findViewById(R.id.ivMainDownloadedInfo);
		ibCloseMain = findViewById(R.id.ibCloseMain);
		ibCloseMap = findViewById(R.id.ibCloseMap);
		canceButtonBigScreen = findViewById(R.id.canceButtonBigScreen);
		ibPreviousWvMain = findViewById(R.id.ibPreviousWvMain);
		ibNextWvMain = findViewById(R.id.ibNextWvMain);
		tvUrlMain = findViewById(R.id.tvUrlMain);
		tvCopyrightMap = findViewById(R.id.tvCopyrightMap);
		ivMainRope = findViewById(R.id.ivMainRope);
		pbWebView2 = findViewById(R.id.pbWebView2);
		pbWallpaper = findViewById(R.id.pbWallpaper);


		Log.d("day", "" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK));

		day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

		dayRecorded = sharedPreferences.getInt("wallpaper_day", 1);

		items = new ArrayList<OverlayItem>();
		map.setVisibility(View.INVISIBLE);

		switch (Utils.sizeScreen(getApplicationContext()))
		{
			case "normal":
				beginZoomLvl = 5.9;
				maxZoomLvl = 8d;
				break;
			case "large":
			case "xlarge":
				beginZoomLvl = 6.6;
				maxZoomLvl = 8.2;
				break;
		}

		dayCheckForWallpapers();


		btnWordList.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), WordList.class));
			}
		});

		btnMedia.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), MassMedia.class));
			}
		});

		btnGames.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intention = new Intent(getApplicationContext(), Game.class);
				startActivity(intention);
			}
		});

		btnBooks.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intention = new Intent(getApplicationContext(), Books.class);
				startActivity(intention);
			}
		});

		tvCopyrightMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("https://www.openstreetmap.org/copyright/en"));
				startActivity(i);
			}
		});

		btnSettings.setOnClickListener(new View.OnClickListener()

		{
			@Override
			public void onClick(View v) {
				Intent intention = new Intent(getApplicationContext(), MySettings.class);
				startActivity(intention);
			}
		});

		ibCloseMain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pbWebView2.setVisibility(View.INVISIBLE);
				clWvGlobalMain.setVisibility(View.INVISIBLE);
				ivMainDownloaded.setVisibility(View.VISIBLE);
			}
		});

		ibPreviousWvMain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ivMainDownloadedInfo.canGoBack()) ivMainDownloadedInfo.goBack();
			}
		});

		ibNextWvMain.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ivMainDownloadedInfo.canGoForward()) ivMainDownloadedInfo.goForward();
			}
		});

		canceButtonBigScreen.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				clMainDownloadBig.setVisibility(View.INVISIBLE);
			}
		});

		ibCloseMap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bLocation.setVisibility(View.VISIBLE);
				ibCloseMap.setVisibility(View.INVISIBLE);
				tvCopyrightMap.setVisibility(View.INVISIBLE);
				ivMainRope.setElevation(1);
				map.setVisibility(View.INVISIBLE);
				ivMainDownloaded.setVisibility(View.VISIBLE);
				bHistory.setVisibility(View.VISIBLE);
			}
		});

		bLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bLocation.setVisibility(View.INVISIBLE);
				ibCloseMap.setVisibility(View.VISIBLE);
				tvCopyrightMap.setVisibility(View.VISIBLE);
				ivMainRope.setElevation(0);
				map.setVisibility(View.VISIBLE);
				ivMainDownloaded.setVisibility(View.INVISIBLE);
				bHistory.setVisibility(View.INVISIBLE);
			}
		});

		bHistory.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Utils.isWifiConnected(getApplicationContext(), true)) {
					if (ivMainDownloadedInfo.getUrl() == null) {
						tvUrlMain.setText("Wikipedia: " + location);
						mediaReader = new MediaReader(getApplicationContext(), wallpaper_wiki_info, ivMainDownloadedInfo, pbWebView2, tvUrlMain, urlsList);
						mediaReader.setMedia();
					}
				}
				clWvGlobalMain.setVisibility(View.VISIBLE);
			}
		});

		ivMainDownloadedBig.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				clMainDownloadBig.setVisibility(View.INVISIBLE);
			}
		});

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("showWV", ivMainDownloadedInfo.getVisibility());
		outState.putString("urlToLoadBack", ivMainDownloadedInfo.getUrl());
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		ivMainDownloadedInfo.setVisibility(savedInstanceState.getInt("showWV"));
		if (savedInstanceState.getInt("showWV") == View.VISIBLE) {
			mediaReader = new MediaReader(savedInstanceState.getString("urlToLoadBack"), ivMainDownloadedInfo, pbWebView2, urlsList);
			mediaReader.setMedia();
			ivMainDownloadedInfo.setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		map.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (!sharedPreferences.getBoolean("firstLaunch", true)) {
			sharedPreferences.edit().putBoolean("firstLaunch", true).apply();
		}
		map.onPause();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onDestroy() {
		sharedPreferences.edit().putBoolean("wasNotDestroyed", true).apply();
		map.onDetach();
		map.getTileProvider().clearTileCache();
		try {
			Utils.trimCache(getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		SharedPreferences sharedPreferences = newBase.getSharedPreferences("settings", Context.MODE_PRIVATE);
		DictionnaryFetcher.setLangages();
		if(!sharedPreferences.getBoolean("LanguageHasBeenSet",false)) {
			super.attachBaseContext(Utils.wrap(newBase,Utils.languageInit()));
		} else {
			super.attachBaseContext(Utils.wrap(newBase, LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()));
		}
	}

	private void dayCheckForWallpapers() {
		final File wallpaperDir = getDir("Wallpaper", Context.MODE_PRIVATE);
		if (day != dayRecorded)
		{
			new AsyncTask<String, Integer, String>() {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();
					pbWallpaper.setVisibility(View.VISIBLE);
					pbWallpaper.setMax(100);
					pbWallpaper.setProgress(0);
				}

				@Override
				protected String doInBackground(String... strings) {
					if (day != sharedPreferences.getInt("wallpaper_day", 1)) {
						if (Utils.isWifiConnected(getApplicationContext(), false)) {
							sharedPreferences = getSharedPreferences("settings", 0);
							Document doc = null;
							List<String> wallpaperValue;
							publishProgress(10);
							try {
								doc = Jsoup.connect(URL_DB_WALLPAPER).get();
								wallpaperValue = doc.select("ul.wallpaper_year li").eachText();

								for (int i = 0; i < wallpaperValue.size(); i += 6) {
									if (Integer.valueOf(wallpaperValue.get(i)) == day) {
										sharedPreferences.edit().putInt("wallpaper_day", Integer.valueOf(wallpaperValue.get(i))).apply();
										sharedPreferences.edit().putString("wallpaper_location", wallpaperValue.get(i + 1)).apply();
										sharedPreferences.edit().putString("wallpaper_latitude", wallpaperValue.get(i + 2)).apply();
										sharedPreferences.edit().putString("wallpaper_longitude", wallpaperValue.get(i + 3)).apply();
										sharedPreferences.edit().putString("wallpaper_year", wallpaperValue.get(i + 4)).apply();
										sharedPreferences.edit().putString("wallpaper_wiki_info", wallpaperValue.get(i + 5)).apply();
									}
								}
							} catch (IOException e) {
								Log.e("Error", "Error while creating " + e);
							}
						}
					}

					wallpaper_day = sharedPreferences.getInt("wallpaper_day", 1);
					wallpaper_location = sharedPreferences.getString("wallpaper_location", "menton");
					wallpaper_lat = Double.valueOf(sharedPreferences.getString("wallpaper_latitude", "43.7778167"));
					wallpaper_lng = Double.valueOf(sharedPreferences.getString("wallpaper_longitude", "7.5079319"));
					wallpaper_year = Integer.valueOf(sharedPreferences.getString("wallpaper_year", "2019"));
					wallpaper_wiki_info = sharedPreferences.getString("wallpaper_wiki_info", "https://fr.m.wikipedia.org/wiki/Menton_(Alpes-Maritimes)");

					//1-365

					publishProgress(65);
					if (Utils.isWifiConnected(getApplicationContext(), false)) {
						File file = new File(wallpaperDir,"wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png");
						if (!file.exists()) {
							if (Utils.isWifiConnected(getApplicationContext(), false))
								Utils.saveImage(getApplicationContext(), Utils.downloadImageBitmap(URL_WALLPAPER + "wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png"), "Wallpaper", "wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png");
							Log.d("Image Creation", "Nom: " + "wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png");
							if (wallpaperDir.isDirectory()) {
								String[] children = wallpaperDir.list();
								for (int i = 0; i < children.length; i++) {
									if (!new File(wallpaperDir, children[i]).getName().equals("wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png"))
										new File(wallpaperDir, children[i]).delete();
								}
							}
						}
					}

					return null;
				}

				@Override
				protected void onProgressUpdate(Integer... values) {
					super.onProgressUpdate(values);
					pbWallpaper.setProgress(values[0]);
					if (values[0] == 100) {
						pbWallpaper.setVisibility(View.INVISIBLE);
					}
				}

				@Override
				protected void onPostExecute(String s) {
					super.onPostExecute(s);
					publishProgress(100);
					location = String.valueOf(wallpaper_location.charAt(0)).toUpperCase() + wallpaper_location.substring(1);
					sharedPreferences.edit().putString("bitmapPath", new File(wallpaperDir,"wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png").getAbsolutePath());
					Bitmap toDisplay = BitmapFactory.decodeFile(new File(wallpaperDir,"wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png").getAbsolutePath());
					ivMainDownloaded = findViewById(R.id.ivMainDownloaded);
					ivMainDownloadedBig = findViewById(R.id.ivMainDownloadedBig);
					ivMainDownloaded.setVisibility(View.VISIBLE);
					if (toDisplay != null) {
						ivMainDownloaded.setImageBitmap(toDisplay);
						ivMainDownloadedBig.setImageBitmap(toDisplay);
					} else {
						ivMainDownloaded.setImageDrawable(getDrawable(R.drawable.wallpaper_1_menton));
						ivMainDownloadedBig.setImageDrawable(getDrawable(R.drawable.wallpaper_1_menton));
					}
					items.add(new OverlayItem(location, "", new GeoPoint(wallpaper_lat, wallpaper_lng)));
					MapActivity mapActivity = new MapActivity(MainActivity.this, MainActivity.this, map, wallpaper_lat, wallpaper_lng, items, beginZoomLvl, maxZoomLvl, new XYTileSource("osmfr", 5, 7, 256, ".png", new String[]{"http://a.tile.openstreetmap.fr/osmfr/", "http://b.tile.openstreetmap.fr/osmfr/", "http://c.tile.openstreetmap.fr/osmfr/"}, String.valueOf(R.string.copyright_osm), new TileSourcePolicy(2, TileSourcePolicy.FLAG_NO_BULK | TileSourcePolicy.FLAG_NO_PREVENTIVE | TileSourcePolicy.FLAG_USER_AGENT_MEANINGFUL | TileSourcePolicy.FLAG_USER_AGENT_NORMALIZED)));
					mapActivity.setMapView();
					ivMainDownloaded.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							clMainDownloadBig.setVisibility(View.VISIBLE);
						}
					});
				}
			}.execute();
		} else {
			wallpaper_day = sharedPreferences.getInt("wallpaper_day", 1);
			wallpaper_location = sharedPreferences.getString("wallpaper_location", "menton");
			wallpaper_lat = Double.valueOf(sharedPreferences.getString("wallpaper_latitude", "43.7778167"));
			wallpaper_lng = Double.valueOf(sharedPreferences.getString("wallpaper_longitude", "7.5079319"));
			wallpaper_year = Integer.valueOf(sharedPreferences.getString("wallpaper_year", "2019"));
			wallpaper_wiki_info = sharedPreferences.getString("wallpaper_wiki_info", "https://fr.m.wikipedia.org/wiki/Menton_(Alpes-Maritimes)");
			String imagePath = new File(wallpaperDir,"wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png").getAbsolutePath();
			sharedPreferences.edit().putString("bitmapPath", new File(wallpaperDir,"wallpaper_" + wallpaper_day + "_" + wallpaper_location + ".png").getAbsolutePath());
			Bitmap toDisplay = BitmapFactory.decodeFile(imagePath);
			location = String.valueOf(wallpaper_location.charAt(0)).toUpperCase() + wallpaper_location.substring(1);
			ivMainDownloaded.setVisibility(View.VISIBLE);
			if (toDisplay != null) {
				ivMainDownloaded.setImageBitmap(toDisplay);
				ivMainDownloadedBig.setImageBitmap(toDisplay);
			} else {
				ivMainDownloaded.setImageDrawable(getDrawable(R.drawable.wallpaper_1_menton));
				ivMainDownloadedBig.setImageDrawable(getDrawable(R.drawable.wallpaper_1_menton));
			}
			items.add(new OverlayItem(location, "", new GeoPoint(wallpaper_lat, wallpaper_lng)));
			MapActivity mapActivity = new MapActivity(this, this, map, wallpaper_lat, wallpaper_lng, items, beginZoomLvl, maxZoomLvl, new XYTileSource("osmfr", 5, 7, 256, ".png", new String[]{"http://a.tile.openstreetmap.fr/osmfr/", "http://b.tile.openstreetmap.fr/osmfr/", "http://c.tile.openstreetmap.fr/osmfr/"}, String.valueOf(R.string.copyright_osm), new TileSourcePolicy(2, TileSourcePolicy.FLAG_NO_BULK | TileSourcePolicy.FLAG_NO_PREVENTIVE | TileSourcePolicy.FLAG_USER_AGENT_MEANINGFUL | TileSourcePolicy.FLAG_USER_AGENT_NORMALIZED)));

			mapActivity.setMapView();

			ivMainDownloaded.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					clMainDownloadBig.setVisibility(View.VISIBLE);
				}
			});
		}
	}

	public boolean checkExternalStoragePermission(final Activity activity) {

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

			// Should we show an explanation?
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

				// Show an explanation to the user *asynchronously* -- don't block
				// this thread waiting for the user's response! After the user
				// sees the explanation, try again to request the permission.
				new AlertDialog.Builder(this).setTitle(R.string.title_external_storage).setMessage(R.string.external_storage).setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						//Prompt the user once explanation has been shown
						ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
					}
				}).create().show();
			} else {
				// No explanation needed, we can request the permission.
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
			}
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
				// If request is cancelled, the result arrays are empty.
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

					// permission was granted, yay! Do the
					// location-related task you need to do.
					if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
						Toast.makeText(this, R.string.may_not_work, Toast.LENGTH_SHORT).show();
					}

				}
				return;
			}

		}
	}
}
