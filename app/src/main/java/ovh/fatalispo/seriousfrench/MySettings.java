package ovh.fatalispo.seriousfrench;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import ovh.fatalispo.seriousfrench.adapter.Adapter_Spinner_Dico;
import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;
import ovh.fatalispo.seriousfrench.receiver.AlarmReceiver;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;
import static ovh.fatalispo.seriousfrench.Utils.cancelReminder;
import static ovh.fatalispo.seriousfrench.Utils.setReminder;


public class MySettings extends Activity {

	private Button btnLanguage, btndialog, btnClock, btnStatistics, btnFeedback, btnDonate, btnCredits;
	private ImageButton ibGiveFeedback, ibGiveDonate, ibCloseWV, ibSendMail, ratingBar, ibEditTime, ibEditDate;
	private ImageView ivLanguageSet;
	private TextView tvLanguageSelected, tvUrl, tvAppVersion, tvStatPointHistoGame, tvStatPointGeoGame, tvStatTimeSpentBook,tvStatTimeSpentGeoGame,tvStatTimeSpentHistoGame,tvStatTimeSpentMedia,tvStatWordList;
	private LinearLayout llcbEveryDay, llDate, llTime;
	private RelativeLayout rlReminder;
	private SharedPreferences sharedPreferences,sharedPreferencesGame;
	private Spinner spinner;
	private CheckBox cbEveryDayReminder,cbReminderOnOff;
	private EditText etDateReminder, etTimeReminder, etBodyMail, etMailObject;
	private int selectedLanguagePos;
	private boolean alarmRecur,reminderIsOn;
	public final static int DAILY_REMINDER_REQUEST_CODE = 999;
	public Parcelable saveSpinner;
	private ArrayList<WordLocalDB> wordList = new ArrayList<>();
	Dialog dialog;
	private int mYear, mMonth, mDay, mHour, mMinute, mSecond;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		llDate = findViewById(R.id.rldate);
		llTime = findViewById(R.id.rltime);
		llcbEveryDay = findViewById(R.id.rlcbEveryDay);
		rlReminder = findViewById(R.id.rlReminder);
		ibGiveFeedback = findViewById(R.id.ibGiveFeedback);
		ibGiveDonate = findViewById(R.id.ibGiveDonate);
		ibEditTime = findViewById(R.id.ibEditTime);
		ibEditDate = findViewById(R.id.ibEditDate);
		ivLanguageSet = findViewById(R.id.ivLanguageSet);
		tvLanguageSelected = findViewById(R.id.tvLanguageSelected);
		spinner = findViewById(R.id.psLanguagesSet);
		cbEveryDayReminder = findViewById(R.id.cbEveryDayReminder);
		cbReminderOnOff = findViewById(R.id.cbReminderOnOff);
		etDateReminder = findViewById(R.id.etDateReminder);
		etTimeReminder = findViewById(R.id.etTimeReminder);
		ratingBar = findViewById(R.id.ratingBar);
		tvAppVersion = findViewById(R.id.tvAppVersion);
		tvStatPointGeoGame = findViewById(R.id.tvStatPointGeoGame);
		tvStatPointHistoGame = findViewById(R.id.tvStatPointHistoGame);
		tvStatTimeSpentBook = findViewById(R.id.tvStatTimeSpentBook);
		tvStatTimeSpentGeoGame = findViewById(R.id.tvStatTimeSpentGeoGame);
		tvStatTimeSpentHistoGame = findViewById(R.id.tvStatTimeSpentHistoGame);
		tvStatTimeSpentMedia = findViewById(R.id.tvStatTimeSpentMedia);
		tvStatWordList = findViewById(R.id.tvStatWordList);

		sharedPreferences = getSharedPreferences("settings", 0);
		sharedPreferencesGame = getSharedPreferences("games", 0);
		dialog = new Dialog(this);
		wordList.addAll(Utils.initializeWordList(getApplicationContext()));

		DictionnaryFetcher.setLangages();

		Adapter_Spinner_Dico mCustomAdapter = new Adapter_Spinner_Dico(this, LANGAGES, R.layout.spinner_dropdown_listitem);
		spinner.setAdapter(mCustomAdapter);
		if (saveSpinner != null) spinner.onRestoreInstanceState(saveSpinner);
		spinner.setSelection(sharedPreferences.getInt("LanguageSet", 19), false);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				selectedLanguagePos = position;
				sharedPreferences.edit().putBoolean("LanguageHasBeenSet",true).apply();
				setLocale(LANGAGES[selectedLanguagePos].getInitial());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});


		ratingBar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				rateThisApp(getApplicationContext());
			}
		});

		ibGiveFeedback.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog();
			}
		});

		ibGiveDonate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse("https://www.paypal.com/"+LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()+"/webapps/mpp/send-money-online"));
				startActivity(i);
			}
		});

		ibEditTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				final Calendar c = Calendar.getInstance();
				mHour = Calendar.getInstance().getTime().getHours();
				mMinute = Calendar.getInstance().getTime().getMinutes();

				TimePickerDialog tpd = new TimePickerDialog(MySettings.this,R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						mHour = hourOfDay;
						mMinute = minute;
						String correctHours = hourOfDay<10?"0"+hourOfDay:""+hourOfDay;
						String correctMinutes = minute<10?"0"+minute:""+minute;
						etTimeReminder.setText(correctHours + ":" + correctMinutes);
					}
				}, mHour, mMinute, true);
				tpd.show();
			}
		});

		ibEditDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);

				// Launch Date Picker Dialog
				DatePickerDialog dpd = new DatePickerDialog(MySettings.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						// Display Selected date in textbox
						etDateReminder.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

					}
				}, mYear, mMonth, mDay);
				dpd.show();
			}
		});

		cbEveryDayReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					alarmRecur = true;
				} else
					alarmRecur = false;
			}
		});

		cbReminderOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					reminderIsOn = true;
					cbEveryDayReminder.setEnabled(true);
					ibEditDate.setEnabled(true);
					ibEditTime.setEnabled(true);
					etTimeReminder.setEnabled(true);
					etDateReminder.setEnabled(true);
				} else {
					reminderIsOn = false;
					cbEveryDayReminder.setEnabled(false);
					ibEditDate.setEnabled(false);
					ibEditTime.setEnabled(false);
					etTimeReminder.setEnabled(false);
					etDateReminder.setEnabled(false);
				}
			}
		});
	}

	public void showDialog() {
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.dialog_list_view);
		ibCloseWV = dialog.findViewById(R.id.ibClose2);
		ibSendMail = dialog.findViewById(R.id.ibSendMail);
		etBodyMail = dialog.findViewById(R.id.etBodyMail);
		etMailObject = dialog.findViewById(R.id.etMailObject);


		ibCloseWV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});


		ibSendMail.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (Utils.isWifiConnected(getApplicationContext(), true)) {
					if (!etMailObject.getText().toString().equals("") && !etBodyMail.getText().toString().equals("")) {
						sendMail(etMailObject.getText().toString(), etBodyMail.getText().toString());
					}
				}
			}
		});

		if (!dialog.isShowing()) dialog.show();

	}

	@Override
	protected void onResume() {
		super.onResume();
		selectedLanguagePos = sharedPreferences.getInt("LanguageSet", 19);
		alarmRecur = sharedPreferences.getBoolean("alarmRecur",false);
		reminderIsOn = sharedPreferences.getBoolean("reminderOn",false);
		etTimeReminder.setText(sharedPreferences.getString("timeReminder","12:00"));
		etDateReminder.setText(sharedPreferences.getString("dateReminder", Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+
				"-"+Calendar.getInstance().get(Calendar.MONTH)+"-"+
				Calendar.getInstance().get(Calendar.YEAR)));
		cbEveryDayReminder.setChecked(alarmRecur);
		cbReminderOnOff.setChecked(reminderIsOn);
		if(reminderIsOn) {
			ibEditDate.setEnabled(true);
			ibEditTime.setEnabled(true);
			etTimeReminder.setEnabled(true);
			etDateReminder.setEnabled(true);
			cbEveryDayReminder.setEnabled(true);
			cbReminderOnOff.setChecked(true);
		} else {
			ibEditDate.setEnabled(false);
			ibEditTime.setEnabled(false);
			etTimeReminder.setEnabled(false);
			etDateReminder.setEnabled(false);
			cbEveryDayReminder.setEnabled(false);
			cbReminderOnOff.setChecked(false);
		}
		tvAppVersion.setText(getString(R.string.app_name) + " " + getString(R.string.app_version) + ": " + BuildConfig.VERSION_NAME);
		tvStatWordList.setText(getString(R.string.wordlist_count) + " " + wordList.size());
		tvStatTimeSpentMedia.setText(getString(R.string.stat_time_media)+ " " + convertMilliToTime(sharedPreferences.getLong("timeSpentMedia",0)));
		tvStatTimeSpentHistoGame.setText(getString(R.string.stat_time_histo)+ " " + convertMilliToTime(sharedPreferences.getLong("timeSpentHisto",0)));
		tvStatTimeSpentGeoGame.setText(getString(R.string.stat_time_geo)+ " " + convertMilliToTime(sharedPreferences.getLong("timeSpentGeo",0)));
		tvStatTimeSpentBook.setText(getString(R.string.stat_time_book)+ " " + convertMilliToTime(sharedPreferences.getLong("timeSpentBook",0)));
		tvStatPointHistoGame.setText(getString(R.string.stat_histo_points)+ " " +sharedPreferencesGame.getInt("histoPoints",0));
		tvStatPointGeoGame.setText(getString(R.string.stat_geo_points)+ " " +sharedPreferencesGame.getInt("geoPoints",0));
	}

	@Override
	protected void onPause() {
		if(selectedLanguagePos != 0) {
			sharedPreferences.edit().putInt("LanguageSet", selectedLanguagePos).apply();
		}
		sharedPreferences.edit().putBoolean("alarmRecur",alarmRecur).apply();
		if (!etTimeReminder.getText().equals("")) {
			sharedPreferences.edit().putString("timeReminder", etTimeReminder.getText().toString()).apply();
			setReminder(this,AlarmReceiver.class, mHour,mMinute, alarmRecur);
		}
		if (!etDateReminder.getText().equals("")) {
			sharedPreferences.edit().putString("dateReminder", etDateReminder.getText().toString()).apply();
		}
		if(reminderIsOn) {
			sharedPreferences.edit().putBoolean("reminderOn", true).apply();
		} else {
			sharedPreferences.edit().putBoolean("reminderOn", false).apply();
			cancelReminder(this,AlarmReceiver.class);
		}
		super.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
		outState.putParcelable("spinner",spinner.onSaveInstanceState());
		super.onSaveInstanceState(outState, outPersistentState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		saveSpinner = savedInstanceState.getParcelable("spinner");
	}

	@Override
	protected void onDestroy() {
		if (dialog != null) dialog.dismiss();
		super.onDestroy();
	}

	private void rateThisApp(Context context) {
		Uri uri = Uri.parse("market://details?id=" + "ovh.fatalispo.seriousfrench&hl=fr"/* + LANGAGESDICO[selectedLanguagePos].getInitial()context.getPackageName()*/);
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		// To count with Play market backstack, After pressing back button,
		// to taken back to our application, we need to add following flags to intent.
		goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		try {
			startActivity(goToMarket);
		} catch (ActivityNotFoundException e) {
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "ovh.fatalispo.seriousfrench&hl=fr")));
		}
	}

	public void setLocale(String lang) {
		Locale myLocale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
		Intent refresh = new Intent(this, MainActivity.class);
		finish();
		startActivity(refresh);
	}

	public void sendMail(String objectMail, String bodyMail) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		i.putExtra(Intent.EXTRA_EMAIL, new String[]{"fatalispo@gmail.com"});
		i.putExtra(Intent.EXTRA_SUBJECT, objectMail);
		i.putExtra(Intent.EXTRA_TEXT, bodyMail);
		try {
			startActivity(Intent.createChooser(i, "Send an email..."));
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(MySettings.this, getText(R.string.no_mail_client), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		SharedPreferences sharedPreferences = newBase.getSharedPreferences("settings", Context.MODE_PRIVATE);
		DictionnaryFetcher.setLangages();
		if(!sharedPreferences.getBoolean("LanguageHasBeenSet",false)) {
			sharedPreferences.edit().putBoolean("firstLaunch",true).apply();
			super.attachBaseContext(Utils.wrap(newBase,Locale.getDefault().getLanguage()));
		} else {
			super.attachBaseContext(Utils.wrap(newBase, LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()));
		}
	}

	private String convertMilliToTime(long milliseconds) {
		long s = milliseconds /1000 % 60;
		long m = (milliseconds / 1000 / 60) % 60;
		long h = (milliseconds / 1000 / (60 * 60));
		return String.format("%02d:%02d:%02d", h,m,s);
	}
}
