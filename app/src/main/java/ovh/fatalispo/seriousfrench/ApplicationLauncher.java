package ovh.fatalispo.seriousfrench;

import android.app.Application;
import android.content.Intent;

import static java.lang.Thread.sleep;

public class ApplicationLauncher extends Application {

	@Override
	public void onCreate() {
		Intent myIntent = new Intent(getApplicationContext(),WelcomeScreen.class);
		myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(myIntent);
		super.onCreate();
	}
}
