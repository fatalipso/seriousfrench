package ovh.fatalispo.seriousfrench;

public class Langage {
	private String langage;
	private String initial;
	private int image;

	public Langage(String langage, String initial, int image) {
		this.langage = langage;
		this.initial = initial;
		this.image = image;
	}

	public String getLangage() {
		return langage;
	}

	public void setLangage(String langage) {
		this.langage = langage;
	}

	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}
}
