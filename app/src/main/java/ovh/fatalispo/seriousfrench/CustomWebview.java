package ovh.fatalispo.seriousfrench;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;

public class CustomWebview extends WebView {
	public CustomWebview(Context context) {
		super(context);
	}

	public CustomWebview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomWebview(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public CustomWebview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	protected void onWindowVisibilityChanged(int visibility) {
		if (visibility != View.GONE) super.onWindowVisibilityChanged(visibility);
	}
}
