package ovh.fatalispo.seriousfrench;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;
import ovh.fatalispo.seriousfrench.databases.DBAsync;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;
import static ovh.fatalispo.seriousfrench.MassMedia.sharedPreferences;

public class WelcomeScreen extends Activity {
	protected static Handler handler;
	protected static ProgressBar pbLaunchApp;
	public static final String theAlphabet[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	public static Map<String, Integer> listOfNeededAuthorization = new TreeMap<>();
	public static Set<Map.Entry<String, Integer>> requestSet;
	protected static ArrayList<WordLocalDB> wordList = new ArrayList<>();
	public static ArrayList<WebData> booksList = new ArrayList<>(), radiosList = new ArrayList<>(), newspaperList = new ArrayList<>(), videoList = new ArrayList<>(), channelsList = new ArrayList<>();
	public static ArrayList<String> urlsList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		Utils.hideNavBar(this);
		setContentView(R.layout.application_launcher);
		pbLaunchApp = findViewById(R.id.pbLaunchApp);
		requestSet = listOfNeededAuthorization.entrySet();
		sharedPreferences = getSharedPreferences("settings", 0);
		pbLaunchApp.setVisibility(View.VISIBLE);

//		new DBAsync(getApplicationContext()).execute();

		new AsyncTask<String, Integer, String>() {
			@Override
			protected String doInBackground(String... strings) {
				publishProgress(40);
				new DBAsync(getApplicationContext()).execute();
				if (wordList.isEmpty() && channelsList.isEmpty() && radiosList.isEmpty() && videoList.isEmpty() && newspaperList.isEmpty() && urlsList.isEmpty() && booksList.isEmpty()) {
					wordList.addAll(Utils.initializeWordList(getApplicationContext()));
					channelsList.addAll(Utils.initializeStreams(getApplicationContext(), "tv_channel"));
					radiosList.addAll(Utils.initializeStreams(getApplicationContext(), "radio_channel"));
					videoList.addAll(Utils.initializeStreams(getApplicationContext(), "video"));
					newspaperList.addAll(Utils.initializeStreams(getApplicationContext(), "newspaper"));
					booksList.addAll(Utils.initializeStreams(getApplicationContext(), "book"));
					urlsList.addAll(Utils.initializeUrls(getApplicationContext()));
				}
				return null;
			}

			@Override
			protected void onProgressUpdate(Integer... values) {
				super.onProgressUpdate(values);
				pbLaunchApp.setProgress(values[0]);
			}

			@Override
			protected void onPostExecute(String string) {
				super.onPostExecute(string);
				publishProgress(100);
				pbLaunchApp.setProgress(100);
				pbLaunchApp.setVisibility(View.INVISIBLE);
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();
			}
		}.execute();

		super.onCreate(savedInstanceState);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		SharedPreferences sharedPreferences = newBase.getSharedPreferences("settings", Context.MODE_PRIVATE);
		DictionnaryFetcher.setLangages();
		if (!sharedPreferences.getBoolean("LanguageHasBeenSet", false)) {
			super.attachBaseContext(Utils.wrap(newBase, Utils.languageInit()));
		} else {
			super.attachBaseContext(Utils.wrap(newBase, LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()));
		}
	}
}


