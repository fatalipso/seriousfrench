package ovh.fatalispo.seriousfrench.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import ovh.fatalispo.seriousfrench.Langage;
import ovh.fatalispo.seriousfrench.R;

public class Adapter_Spinner_Dico extends ArrayAdapter {

	SharedPreferences sharedPreferences;
	Langage spinnerLangage[];
	Context mContext;

	public Adapter_Spinner_Dico(Context context, Langage langage[], int componentList) {
		super(context, componentList);
		this.spinnerLangage = langage;
		this.mContext = context;
	}

	@Override
	public int getCount() {
		return spinnerLangage.length;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		ViewHolder mViewHolder = new ViewHolder();
		sharedPreferences = getContext().getSharedPreferences("settings", 0);
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) mContext.
					getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.spinner_dropdown_listitem, parent, false);
			mViewHolder.mFlag = (ImageView) convertView.findViewById(R.id.ivFlag);
			mViewHolder.mName = (CheckedTextView) convertView.findViewById(R.id.tvName);
			convertView.setTag(mViewHolder);
		} else {
			mViewHolder = (ViewHolder) convertView.getTag();
		}
		mViewHolder.mFlag.setFocusable(false);
		mViewHolder.mFlag.setImageResource(spinnerLangage[position].getImage());
		mViewHolder.mName.setText(spinnerLangage[position].getLangage());

		return convertView;
	}

	@Override
	public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		return getView(position, convertView, parent);
	}

	private static class ViewHolder {
		ImageView mFlag;
		CheckedTextView mName;
	}


}
