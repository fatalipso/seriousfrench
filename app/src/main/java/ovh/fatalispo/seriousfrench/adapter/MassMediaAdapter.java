package ovh.fatalispo.seriousfrench.adapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnLongPressListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageScrollListener;
import com.github.barteksc.pdfviewer.listener.OnTapListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import ovh.fatalispo.seriousfrench.CustomWebview;
import ovh.fatalispo.seriousfrench.MassMedia;
import ovh.fatalispo.seriousfrench.MediaReader;
import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;
import ovh.fatalispo.seriousfrench.WebData;
import ovh.fatalispo.seriousfrench.book.Books;
import ovh.fatalispo.seriousfrench.radio.RadioPlayer;

import static ovh.fatalispo.seriousfrench.MassMedia.onShowRadio;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.urlsList;

public class MassMediaAdapter extends RecyclerView.Adapter<MassMediaAdapter.ViewHolder> {
	private static ArrayList<WebData> webDataList;
	private static CustomWebview webView;
	protected static ImageButton cancelButton, playRadio, stopRadio, ibPreviousWv, ibNextWv, ibZoomButton, ibUnZoomButton;
	private static TextView tvUrl;
	private static RecyclerView rvVideos;
	private static String type;
	private static SharedPreferences sharedPreferences;
	private static SharedPreferences.Editor editor;
	private static String listeningTo, urls;
	protected static ImageView ivRadioPlaying;
	protected static ConstraintLayout clRadio, clWvGlobal;
	protected static WindowManager wm;
	protected static ProgressBar pbWebView;
	private static MediaReader mediaReader;
	static PDFView pdfView;
	static int pageNumber = 0, totalPage = 0;
	static boolean isNotTouched;

	public MassMediaAdapter(ArrayList<WebData> videosList, ImageButton cancelButton, RecyclerView rvVideos, String type, ImageView ivRadioPlaying, ConstraintLayout clRadio, WindowManager wm) {
		this.webDataList = videosList;
		this.cancelButton = cancelButton;
		this.rvVideos = rvVideos;
		this.type = type;
		this.ivRadioPlaying = ivRadioPlaying;
		this.clRadio = clRadio;
		this.wm = wm;
	}

	public MassMediaAdapter(ArrayList<WebData> videosList, ImageButton cancelButton, RecyclerView rvVideos, String type, ConstraintLayout clWvGlobal, TextView tvUrl, PDFView pdfView, ImageButton ibPreviousWv, ImageButton ibNextWv, ImageButton ibZoomButton, ImageButton ibUnZoomButton) {
		this.webDataList = videosList;
		this.cancelButton = cancelButton;
		this.rvVideos = rvVideos;
		this.type = type;
		this.clWvGlobal = clWvGlobal;
		this.tvUrl = tvUrl;
		this.pdfView = pdfView;
		this.ibPreviousWv = ibPreviousWv;
		this.ibNextWv = ibNextWv;
		this.ibZoomButton = ibZoomButton;
		this.ibUnZoomButton = ibUnZoomButton;
		isNotTouched = true;

	}

	public MassMediaAdapter(ArrayList<WebData> videosList, CustomWebview webView, ImageButton cancelButton, RecyclerView rvVideos, ProgressBar pbWebView, String type, ImageButton ibPreviousWv, ImageButton ibNextWv, ConstraintLayout clWvGlobal, MediaReader mediaReader, TextView tvUrl) {
		this.webDataList = videosList;
		this.webView = webView;
		this.cancelButton = cancelButton;
		this.rvVideos = rvVideos;
		this.type = type;
		this.pbWebView = pbWebView;
		this.ibPreviousWv = ibPreviousWv;
		this.ibNextWv = ibNextWv;
		this.clWvGlobal = clWvGlobal;
		this.mediaReader = mediaReader;
		this.tvUrl = tvUrl;
	}

	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ceil, null);
		sharedPreferences = parent.getContext().getSharedPreferences("radio", 0);
		editor = parent.getContext().getSharedPreferences("radio", 0).edit();
		ViewHolder viewHolder = new ViewHolder(itemLayoutView);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		holder.name.setText(webDataList.get(position).getName());
		holder.image.setImageBitmap(webDataList.get(position).getImage());
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public TextView name;
		public ImageView image;

		public ViewHolder(final View itemLayoutView) {
			super(itemLayoutView);
			name = itemLayoutView.findViewById(R.id.description);
			image = itemLayoutView.findViewById(R.id.imageViewAdapter);
			if (!type.equals("radio")) {
				itemLayoutView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						v.getContext().stopService(new Intent(v.getContext(), RadioPlayer.class));
						final String urlToShow = webDataList.get(getAdapterPosition()).getUrl();
						if (type.equals("book")) {
							new AsyncTask<String, String, String>() {
								@Override
								protected String doInBackground(String... strings) {
									Utils.getPDF(urlToShow);
									return null;
								}

								@Override
								protected void onPostExecute(String s) {
									super.onPostExecute(s);
									pdfView.fromUri(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/FrenchImmersion_Books/" + urlToShow)))
											.defaultPage(pageNumber)
											.swipeHorizontal(true)
											.enableDoubletap(true)
											.pageSnap(true)
											.autoSpacing(true)
											.pageFling(true)
											.fitEachPage(true)
											.onPageChange(new OnPageChangeListener() {
												@Override
												public void onPageChanged(int page, int pageCount) {
													pageNumber = page;
													totalPage = pageCount;
													tvUrl.setText(String.format("%s %s / %s", webDataList.get(getAdapterPosition()).getName(), page + 1, pageCount));
												}
											})
											.scrollHandle(new DefaultScrollHandle(Books.instance))
											.load();
									clWvGlobal.setVisibility(View.VISIBLE);
									clWvGlobal.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											if(isNotTouched) {
												ibPreviousWv.setVisibility(View.VISIBLE);
												ibNextWv.setVisibility(View.VISIBLE);
												ibZoomButton.setVisibility(View.VISIBLE);
												ibUnZoomButton.setVisibility(View.VISIBLE);
												isNotTouched = false;
											} else {
												ibPreviousWv.setVisibility(View.INVISIBLE);
												ibNextWv.setVisibility(View.INVISIBLE);
												ibZoomButton.setVisibility(View.INVISIBLE);
												ibUnZoomButton.setVisibility(View.INVISIBLE);
												isNotTouched = true;
											}
										}
									});

//									clWvGlobal.setOnLongClickListener(new View.OnLongClickListener() {
//										@Override
//										public boolean onLongClick(View v) {
//											pdfView.setScrollY(-clWvGlobal.getHeight());
//											pdfView.zoomWithAnimation(v.getX(),v.getY(),pdfView.getMinZoom());
//											return true;
//										}
//									});
//
//									clWvGlobal.setOnTouchListener(new View.OnTouchListener() {
//										@Override
//										public boolean onTouch(View v, MotionEvent event) {
//											switch(event.getAction()) {
//												case MotionEvent.ACTION_DOWN:
//													pdfView.setScrollY((int) event.getY()-clWvGlobal.getHeight()/2);
//													pdfView.zoomWithAnimation(event.getX(),event.getY(),pdfView.getZoom() + 0.1f);
//													break;
//											}
//											return false;
//										}
//									});
									tvUrl.setText(webDataList.get(getAdapterPosition()).getName());
									rvVideos.setVisibility(View.INVISIBLE);
									ibPreviousWv.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											if(pageNumber - 1 > -1) {
												ibPreviousWv.setAlpha(1f);
												pageNumber--;
												tvUrl.setText(String.format("%s %s / %s", webDataList.get(getAdapterPosition()).getName(), pageNumber, totalPage));
												pdfView.jumpTo(pageNumber,true);
											} else {
												ibPreviousWv.setAlpha(0.5f);
											}

										}
									});

									ibNextWv.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											if(pageNumber + 1 <= totalPage) {
												ibNextWv.setAlpha(1f);
												pageNumber++;
												tvUrl.setText(String.format("%s %s / %s", webDataList.get(getAdapterPosition()).getName(), pageNumber, totalPage));
												pdfView.jumpTo(pageNumber,true);
											} else {
												ibNextWv.setAlpha(0.5f);
											}

										}
									});

									ibZoomButton.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											if(pdfView.getZoom() < 2) {
												pdfView.zoomWithAnimation(clWvGlobal.getWidth()/2,clWvGlobal.getHeight()/2,pdfView.getZoom() + 0.1f);
											}
										}
									});

									ibUnZoomButton.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											if(pdfView.getZoom() >= 0.3) {
												pdfView.zoomWithAnimation(clWvGlobal.getWidth()/2,clWvGlobal.getHeight()/2,pdfView.getZoom() - 0.1f);
											}
										}
									});
								}
							}.execute();
						} else {
							if (Utils.isWifiConnected(v.getContext(), true)) {
								mediaReader = new MediaReader(v.getContext(), urlToShow, webView, pbWebView, tvUrl, urlsList);
								mediaReader.setMedia();
								webView.setVisibility(View.INVISIBLE);
								if (type.equals("newspaper")) {
									ibNextWv.setVisibility(View.VISIBLE);
									ibPreviousWv.setVisibility(View.VISIBLE);
								} else {
									ibNextWv.setVisibility(View.INVISIBLE);
									ibPreviousWv.setVisibility(View.INVISIBLE);
								}
								clWvGlobal.setVisibility(View.VISIBLE);
								tvUrl.setText(webDataList.get(getAdapterPosition()).getName());
								Log.d("Test", webDataList.get(getAdapterPosition()).getName());
								rvVideos.setVisibility(View.INVISIBLE);
								webView.onResume();
							}
						}
					}
				});
			} else {
				itemLayoutView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						if (Utils.isWifiConnected(v.getContext(), true)) {
							editor.putString("radioToPlay", webDataList.get(getAdapterPosition()).getUrl()).apply();
							editor.putInt("idRadioToPlay", getAdapterPosition()).apply();
							editor.putString("currentMetaData", webDataList.get(getAdapterPosition()).getMetadata()).apply();
							editor.putString("currentClassname", webDataList.get(getAdapterPosition()).getClassName()).apply();
							editor.putString("listeningTo", v.getContext().getString(R.string.listening_to) + webDataList.get(getAdapterPosition()).getName()).apply();
							MassMedia.SHOW_PROGRESS = 0;
							RadioPlayer.mCancel = false;
							//Utils.mediaReader(webDataList.get(getAdapterPosition()).getUrl(), webView);
							clRadio.setVisibility(View.VISIBLE);
							MassMedia.onPlayMusic(v.getContext(), webDataList);
							onShowRadio(wm, clRadio);
						}
					}
				});
			}
		}
	}

	@Override
	public int getItemCount() {
		return webDataList.size();
	}
}
