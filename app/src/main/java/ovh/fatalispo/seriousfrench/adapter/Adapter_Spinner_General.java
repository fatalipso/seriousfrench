package ovh.fatalispo.seriousfrench.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import ovh.fatalispo.seriousfrench.R;

public class Adapter_Spinner_General extends ArrayAdapter {

	String spinnerGeneral[];
	Context mContext;

	public Adapter_Spinner_General(Context context, String general[]) {
		super(context, R.layout.spinner_dropdown_listitem_general);
		this.spinnerGeneral = general;
		this.mContext = context;
	}

	@Override
	public int getCount() {
		return spinnerGeneral.length;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		ViewHolder mViewHolder = new ViewHolder();
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) mContext.
					getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.spinner_dropdown_listitem_general, parent, false);
			mViewHolder.mName = (CheckedTextView) convertView.findViewById(R.id.tvGeneralName);
			convertView.setTag(mViewHolder);
		} else {
			mViewHolder = (ViewHolder) convertView.getTag();
		}
		mViewHolder.mName.setText(spinnerGeneral[position]);

		return convertView;
	}

	@Override
	public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		return getView(position, convertView, parent);
	}

	private static class ViewHolder {
		CheckedTextView mName;
	}


}
