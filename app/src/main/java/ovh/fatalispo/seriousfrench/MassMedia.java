package ovh.fatalispo.seriousfrench;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import ovh.fatalispo.seriousfrench.adapter.Adapter_Spinner_Dico;
import ovh.fatalispo.seriousfrench.adapter.MassMediaAdapter;
import ovh.fatalispo.seriousfrench.databases.DBAsync;
import ovh.fatalispo.seriousfrench.databases.MySQLiteHelper;
import ovh.fatalispo.seriousfrench.databases.words.WordDataSource;
import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;
import ovh.fatalispo.seriousfrench.radio.RadioPlayer;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGESDICO;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.callDictionnary;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.glosbeTranslation;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.titleTranslation;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.setLangagesForDictionary;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.sourceText;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.targetLangage;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.channelsList;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.newspaperList;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.radiosList;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.urlsList;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.videoList;

public class MassMedia extends Activity {
	private CustomWebview wvVideo;
	private CheckBox cbRadioBackground;
	private Button bShowDico;
	public static ProgressBar progressBar, pbDico, pbWebView;
	public static ImageView ivRadioPlaying;
	public static ImageButton backButton, ibClose, ibPreviousWv, ibNextWv, ibCloseRadio, ibPlusRadio, bTV, bRadio, bVideo, ibPlay, ibMute, bNewsPaper, ivAddToList;
	private RecyclerView rvChannels;
	public static TextView tvTitleDico, etToTranslate, tvDictionary, tvTitle, tvDescription, tvAddFav, tvUrl;
	public static ArrayList<WordLocalDB> wordsListToSave = new ArrayList<>();
	private RelativeLayout rlDico;
	private ConstraintLayout clRadioPlayer, clRadio, clMassMedia, clWvGlobal;
	private Spinner spinner;
	private boolean dicoIsClicked, radioMute;
	private static boolean radioIsPlayed, isClRadioDisplayed;
	public static MassMedia instance;
	public final static String CHANNEL_ID = "Radio";
	private static Handler handler;
	public static int SHOW_PROGRESS = 0;
	protected static MediaPlayer mediaPlayer;
	protected static WordDataSource datasource;
	protected static Thread radioThread, showRadioThread;
	private RadioPlayer radioPlayer;
	public static int pauseAction = 4, playAction = 3, sc_playAction = 2, resetAction = 1;
	protected static SharedPreferences sharedPreferences, sharedPreferencesSettings;
	protected static Parcelable saveSpinnerState;
	public static MediaReader mediaReader;
	public long startTime, endTime;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Utils.hideNavBar(this);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int orientation = getResources().getConfiguration().orientation;
		int width = size.x;
		int height = size.y;
		if (width < height) { // portrait
			setContentView(R.layout.activity_mass_media);
		} else { //land
			setContentView(R.layout.activity_mass_media_land);
		}

		sharedPreferences = getSharedPreferences("radio", 0);
		sharedPreferencesSettings = getSharedPreferences("settings", 0);
		final NotificationManager nManager = ((NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE));

		setLangagesForDictionary();

		if (channelsList.isEmpty() && radiosList.isEmpty() && videoList.isEmpty() && newspaperList.isEmpty() && urlsList.isEmpty()) {
			channelsList.addAll(Utils.initializeStreams(getApplicationContext(), "tv_channel"));
			radiosList.addAll(Utils.initializeStreams(getApplicationContext(), "radio_channel"));
			videoList.addAll(Utils.initializeStreams(getApplicationContext(), "video"));
			newspaperList.addAll(Utils.initializeStreams(getApplicationContext(), "newspaper"));
			urlsList.addAll(Utils.initializeUrls(getApplicationContext()));
		}

		tvTitleDico = findViewById(R.id.tvTitleDicoTV);
		etToTranslate = findViewById(R.id.etDraftZoneTV);
		tvDictionary = findViewById(R.id.tvDictionaryTV);
		tvDescription = findViewById(R.id.tvDescription);
		tvTitle = findViewById(R.id.tvTitle);
		tvUrl = findViewById(R.id.tvUrl);
		instance = this;
		mediaPlayer = new MediaPlayer();


		bShowDico = findViewById(R.id.bShowDico);
		rlDico = findViewById(R.id.rlDico);
		bTV = findViewById(R.id.bTv);
		bNewsPaper = findViewById(R.id.bNewsPaper);
		bRadio = findViewById(R.id.bRadio);
		bVideo = findViewById(R.id.bVIdeo);
		ibPlay = findViewById(R.id.ibPlay);
		ibMute = findViewById(R.id.ibMute);
		ibClose = findViewById(R.id.ibClose);
		ibCloseRadio = findViewById(R.id.ibCloseRadio);
		ibPlusRadio = findViewById(R.id.ibPlusRadio);
		ivRadioPlaying = findViewById(R.id.ivRadioPlaying);
		progressBar = findViewById(R.id.pbRadio);
		pbDico = findViewById(R.id.pbDico);

		//rlDico.animate().translationX().setDuration(1*1000);

		ibPreviousWv = findViewById(R.id.ibPreviousWv);
		ibNextWv = findViewById(R.id.ibNextWv);
		wvVideo = findViewById(R.id.wvSendFeedback);
		pbWebView = findViewById(R.id.pbWebView);

		rvChannels = findViewById(R.id.rvChannels);
		clWvGlobal = findViewById(R.id.clWvGlobal);
		clMassMedia = findViewById(R.id.clMassMedia);
		clRadioPlayer = findViewById(R.id.clRadioPlayer);
		clRadio = findViewById(R.id.clRadio);
		spinner = findViewById(R.id.psLanguagesTV);
		ivAddToList = findViewById(R.id.ivAddToList);
		tvAddFav = findViewById(R.id.tvAddFav);

		cbRadioBackground = findViewById(R.id.cbRadioBackground);
		cbRadioBackground.setText(R.string.radio_background);

		progressBar.setMax(100);

		bShowDico.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!dicoIsClicked) {
					Display display = getWindowManager().getDefaultDisplay();
					Point size = new Point();
					display.getSize(size);
					int orientation = getResources().getConfiguration().orientation;
					int width = size.x;
					int height = size.y;
					rlDico.setX(-rlDico.getWidth());
					rlDico.animate().translationX(rlDico.getWidth() + 30).setDuration(1 * 500);
					bShowDico.animate().translationX(rlDico.getWidth() + 30).setDuration(1 * 500);
					dicoIsClicked = true;
				} else {
					rlDico.animate().translationX(0).setDuration(1 * 500);
					bShowDico.animate().translationX(0).setDuration(1 * 500);
					dicoIsClicked = false;
				}
			}
		});

		ibPreviousWv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (wvVideo.canGoBack()) wvVideo.goBack();
			}
		});

		ibNextWv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (wvVideo.canGoForward()) wvVideo.goForward();
			}
		});

		switch (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) {
			case Configuration.SCREENLAYOUT_SIZE_NORMAL:
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
					rvChannels.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
				else rvChannels.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
				break;
			case Configuration.SCREENLAYOUT_SIZE_LARGE:
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
					rvChannels.setLayoutManager(new GridLayoutManager(getApplicationContext(), 4));
				else rvChannels.setLayoutManager(new GridLayoutManager(getApplicationContext(), 5));
				break;
			case Configuration.SCREENLAYOUT_SIZE_XLARGE:
				if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
					rvChannels.setLayoutManager(new GridLayoutManager(getApplicationContext(), 5));
				else rvChannels.setLayoutManager(new GridLayoutManager(getApplicationContext(), 6));
				break;
		}

		setAlphaOnClick(bTV);
		sharedPreferences.edit().putBoolean("showWebview", true).apply();
		rvChannels.setAdapter(new MassMediaAdapter(channelsList, wvVideo, ibClose, rvChannels, pbWebView, "tv", ibPreviousWv, ibNextWv, clWvGlobal, mediaReader, tvUrl));
		if (!sharedPreferences.getBoolean("radioBackground", false)) {
			nManager.cancelAll();
			stopService(new Intent(getApplicationContext(), RadioPlayer.class));
		}

		bVideo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAlphaOnClick(bVideo);
				sharedPreferences.edit().putBoolean("showWebview", true).apply();
				rvChannels.setAdapter(new MassMediaAdapter(videoList, wvVideo, ibClose, rvChannels, pbWebView, "video", ibPreviousWv, ibNextWv, clWvGlobal, mediaReader, tvUrl));
				if (!sharedPreferences.getBoolean("radioBackground", false)) {
					nManager.cancelAll();
					stopService(new Intent(getApplicationContext(), RadioPlayer.class));
				}
			}
		});

		bRadio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAlphaOnClick(bRadio);
				rvChannels.setAdapter(new MassMediaAdapter(radiosList, ibClose, rvChannels, "radio", ivRadioPlaying, clRadio, getWindowManager()));
			}
		});

		bTV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAlphaOnClick(bTV);
				sharedPreferences.edit().putBoolean("showWebview", true).apply();
				rvChannels.setAdapter(new MassMediaAdapter(channelsList, wvVideo, ibClose, rvChannels, pbWebView, "tv", ibPreviousWv, ibNextWv, clWvGlobal, mediaReader, tvUrl));
				if (!sharedPreferences.getBoolean("radioBackground", false)) {
					nManager.cancelAll();
					stopService(new Intent(getApplicationContext(), RadioPlayer.class));
				}
			}
		});

		bNewsPaper.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAlphaOnClick(bNewsPaper);
				sharedPreferences.edit().putBoolean("showWebview", true).apply();
				rvChannels.setAdapter(new MassMediaAdapter(newspaperList, wvVideo, ibClose, rvChannels, pbWebView, "newspaper", ibPreviousWv, ibNextWv, clWvGlobal, mediaReader, tvUrl));
				if (!sharedPreferences.getBoolean("radioBackground", false)) {
					nManager.cancelAll();
					stopService(new Intent(getApplicationContext(), RadioPlayer.class));
				}
			}
		});

		ibCloseRadio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onHideRadio(clRadio);
				if (!sharedPreferences.getBoolean("radioBackground", false)) {
					nManager.cancelAll();
					if (!RadioPlayer.mediaPlayer.isPlaying()) RadioPlayer.mCancel = true;
					progressBar.setVisibility(View.INVISIBLE);
					SHOW_PROGRESS = 0;
					ibPlay.setImageResource(R.drawable.ic_play);
					radioIsPlayed = false;
					stopService(new Intent(getApplicationContext(), RadioPlayer.class));
					if (RadioPlayer.mediaPlayer != null) {
						RadioPlayer.mediaPlayer.stop();
					}
					if (radioThread != null && radioThread.isAlive()) radioThread.interrupt();
				}

			}
		});

		ibPlusRadio.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				clRadio.shouldDelayChildPressedState();
				onShowRadio(getWindowManager(), clRadio);
				if (!RadioPlayer.mediaPlayer.isPlaying()) {
					ibPlay.setImageResource(R.drawable.ic_play);
				} else ibPlay.setImageResource(R.drawable.ic_stop_black_24dp);
			}
		});

		ibPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = sharedPreferences.getString("radioToPlay", "null");
				if (url != null && Utils.isWifiConnected(getApplicationContext(), true)) {
					if (!radioIsPlayed) {
						onPlayMusic(getApplicationContext(), radiosList);
					} else if (radioIsPlayed) {
						RadioPlayer.mediaPlayer.stop();
						RadioPlayer.mCancel = false;
						radioIsPlayed = false;
						SHOW_PROGRESS = 0;
						ibPlay.setImageResource(R.drawable.ic_play);
					}
					ibPlay.setEnabled(true);
				}
			}
		});

		ivAddToList.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean wordAlreadyInList = false;
				for (int i = 0; i < wordsListToSave.size(); i++) {
					if (wordsListToSave.get(i).getSourceTxt().equals(sourceText[0])) {
						wordAlreadyInList = true;
					}
				}
				if (!wordAlreadyInList || wordsListToSave.isEmpty()) {
					wordsListToSave.add(new WordLocalDB(targetLangage[0], Calendar.getInstance().getTime(), sourceText[0], glosbeTranslation[0], titleTranslation[0]));
					Toast.makeText(getApplicationContext(), sourceText[0] + " " + getString(R.string.word_added), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(getApplicationContext(), "\"" + sourceText[0] + "\" " + getString(R.string.word_already_in_list), Toast.LENGTH_LONG).show();
				}
				v.setVisibility(View.INVISIBLE);
				tvAddFav.setVisibility(View.INVISIBLE);
			}
		});

		ibMute.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (RadioPlayer.mediaPlayer != null) {
					if (!radioMute) {
						radioMute = true;
						RadioPlayer.mute();
						ibMute.setImageResource(R.drawable.ic_volume_off);
					} else {
						ibMute.setImageResource(R.drawable.ic_volume_on);
						radioMute = false;
						RadioPlayer.unmute();
					}
				}
			}
		});

		Adapter_Spinner_Dico mCustomAdapter = new Adapter_Spinner_Dico(this, LANGAGESDICO, R.layout.spinner_dropdown_listitem);
		spinner.setAdapter(mCustomAdapter);

		saveSpinnerState = callDictionnary(getApplicationContext(), etToTranslate, spinner, tvDictionary, ivAddToList, tvAddFav, saveSpinnerState, pbDico);

		ibClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (wvVideo != null) {
					mediaReader.stopMedia();
					wvVideo.onPause();
				}
				clWvGlobal.setVisibility(View.INVISIBLE);
				pbWebView.setVisibility(View.INVISIBLE);
				rvChannels.setVisibility(View.VISIBLE);
			}
		});

		cbRadioBackground.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					sharedPreferences.edit().putBoolean("radioBackground", true).apply();
				} else {
					sharedPreferences.edit().putBoolean("radioBackground", false).apply();

				}
			}
		});


		new AsyncTask<String, String, String>() {
			@Override
			protected String doInBackground(String... strings) {
				datasource = new WordDataSource(getApplicationContext());
				datasource.open();
				wordsListToSave.addAll(datasource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null));
				for (WordLocalDB word : wordsListToSave) {
					Log.d("Affichage WordList", "target language: " + word.getTargetLg() + "    source text: " + word.getSourceTxt() + "    translattion google: " + word.getTargetTxtGoogle() + "    translation glosbe: " + word.getTargetTxtGlosbe());
				}
				datasource.close();
				return null;
			}
		}.execute();

	}

	public static void onShowRadio(WindowManager wm, ConstraintLayout clRadio) {
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		//int height = size.y;
		if (!isClRadioDisplayed) {
			isClRadioDisplayed = true;
			clRadio.setX(width);
			clRadio.animate().translationX(-clRadio.getWidth() - 10).setDuration(1 * 500);
		}
	}

	public static void onHideRadio(ConstraintLayout clRadio) {
		isClRadioDisplayed = false;
		clRadio.animate().translationX(0).setDuration(1 * 500);
	}

	public static void onPlayMusic(Context context, final ArrayList<WebData> radiosLists) {
		final SharedPreferences sharedPreferences = context.getSharedPreferences("radio", 0);
		String url = sharedPreferences.getString("radioToPlay", "null");

		if (url != null && Utils.isWifiConnected(context, true)) {
			ibPlusRadio.setVisibility(View.VISIBLE);
			handler = new Handler();
			progressBar.setVisibility(View.VISIBLE);
			if (radiosList.isEmpty()) radiosList.addAll(radiosLists);
			Intent intent = new Intent("service_broadcast");
			intent.putExtra("ACTION", playAction);
			intent.putExtra("radioToPlay", url);
			MassMedia.instance.sendBroadcast(intent);
			cleanRadioDataView();
			radioThread = new Thread(new Runnable() {
				@Override
				public void run() {
					while (SHOW_PROGRESS < 100) {
						// Update the progress status
						// Try to sleep the thread for 20 milliseconds
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						// Update the progress bar
						handler.post(new Runnable() {
							@Override
							public void run() {
								progressBar.setProgress(SHOW_PROGRESS);
								if (SHOW_PROGRESS == 100) {
									radioIsPlayed = true;
									ibPlay.setImageResource(R.drawable.ic_stop_black_24dp);
									progressBar.setVisibility(View.INVISIBLE);
									tvTitle.setText(sharedPreferences.getString("metaDataTitle", "unknown"));
									tvDescription.setText(sharedPreferences.getString("metaDataDescription", "unknown"));
									ivRadioPlaying.setImageBitmap(radiosList.get(sharedPreferences.getInt("idRadioToPlay", 0)).getImage());
								}
							}
						});
					}
				}
			});
			radioThread.start();
			ibPlay.setEnabled(true);
		}
	}

	public static void cleanRadioDataView() {
		tvTitle.setText("");
		tvDescription.setText("");
		ivRadioPlaying.setImageBitmap(null);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("motDico", etToTranslate.getText().toString());
		outState.putString("textDico", tvDictionary.getText().toString());
		outState.putInt("showWV", clWvGlobal.getVisibility());
		outState.putString("urlToLoadBack", wvVideo.getUrl());
		outState.putBoolean("radioState", radioIsPlayed);
		outState.putBoolean("radioDisplay", isClRadioDisplayed);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		etToTranslate.setText(savedInstanceState.getString("motDico"));
		tvDictionary.setText(savedInstanceState.getString("textDico"));
		clWvGlobal.setVisibility(savedInstanceState.getInt("showWV"));
		if (savedInstanceState.getInt("showWV") == View.VISIBLE) {
			mediaReader = new MediaReader(savedInstanceState.getString("urlToLoadBack"), wvVideo, pbWebView, urlsList);
			mediaReader.setMedia();
		}
		if (savedInstanceState.getBoolean("radioDisplay")) {
			isClRadioDisplayed = false;
			onShowRadio(getWindowManager(), clRadio);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	private ServiceConnection conn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {

		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			radioPlayer = ((RadioPlayer.musicBinder) service).getService();
		}
	};

	public void setAlphaOnClick(ImageButton button) {
		bTV.setAlpha(0.5f);
		bRadio.setAlpha(0.5f);
		bVideo.setAlpha(0.5f);
		bNewsPaper.setAlpha(0.5f);
		button.setAlpha(1f);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		NotificationManager nManager = ((NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE));
		if (wvVideo != null) wvVideo.stopLoading();
		if (!sharedPreferences.getBoolean("radioBackground", false)) {
			nManager.cancelAll();
//			stopService(new Intent(getApplicationContext(), RadioPlayer.class));
			wvVideo.stopLoading();
			wvVideo.destroy();
			if (radioThread != null) radioThread.interrupt();
			stopService(new Intent(getApplicationContext(), RadioPlayer.class));
		}
		finish();
		startActivity(new Intent(getApplicationContext(), MainActivity.class));
	}

	@Override
	protected void onPause() {
		super.onPause();
		NotificationManager nManager = ((NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE));
		new AsyncTask<String, String, String>() {
			@Override
			protected String doInBackground(String... strings) {
				datasource = new WordDataSource(getApplicationContext());
				datasource.open();
				ArrayList<WordLocalDB> wordslistsss = new ArrayList<>();
				wordslistsss.addAll(datasource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null));
				for (int j = 0; j < wordslistsss.size(); j++) {
					Log.d("Show me db", wordslistsss.get(j).getSourceTxt());
				}

				for (int i = 0; i < wordsListToSave.size(); i++) {
					datasource.wordUpdater(new WordLocalDB(wordsListToSave.get(i).getTargetLg(), wordsListToSave.get(i).getDate(), wordsListToSave.get(i).getSourceTxt(), wordsListToSave.get(i).getTargetTxtGlosbe(), wordsListToSave.get(i).getTargetTxtGoogle()));
				}
				datasource.close();
				return null;
			}
		}.execute();
		if (!sharedPreferences.getBoolean("radioBackground", false)) {
			nManager.cancelAll();
			unbindService(conn);
			stopService(new Intent(this, RadioPlayer.class));
			if (RadioPlayer.mediaPlayer != null) {
				RadioPlayer.mediaPlayer.stop();
			}
			if (radioThread != null && showRadioThread != null) {
				radioThread.interrupt();
				showRadioThread.interrupt();
			}
			RadioPlayer.mCancel = false;
			radioIsPlayed = false;
			isClRadioDisplayed = false;
		}
		sharedPreferences.edit().putBoolean("radioIsPlayed", radioIsPlayed).apply();
	}

	@Override
	protected void onResume() {
		startTime = System.currentTimeMillis();
		ibPlay.setEnabled(true);
		SHOW_PROGRESS = 0;
		radioIsPlayed = sharedPreferences.getBoolean("radioIsPlayed", false);
		Intent intent = new Intent(this, RadioPlayer.class);
		bindService(intent, conn, BIND_AUTO_CREATE);
		intent.putExtra("ACTION", resetAction);
		startService(intent);

		if (sharedPreferences.getBoolean("radioBackground", false)) {
			onPlayMusic(getApplicationContext(), radiosList);
			isClRadioDisplayed = false;
			handler = new Handler();
			ibPlusRadio.setVisibility(View.VISIBLE);
			showRadioThread = new Thread(new Runnable() {
				@Override
				public void run() {
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							onShowRadio(getWindowManager(), clRadio);
							isClRadioDisplayed = true;
						}
					}, 1000);
				}
			});
			showRadioThread.start();
			cbRadioBackground.setChecked(true);
			ibPlay.setImageResource(R.drawable.ic_stop_black_24dp);
			clRadio.setVisibility(View.VISIBLE);
		} else {
			cbRadioBackground.setChecked(false);
			ibPlay.setImageResource(R.drawable.ic_play);
		}
		Utils.setupUI(findViewById(R.id.clMassMedia), this);
		Utils.setupUI(findViewById(R.id.rvChannels), this);
		Utils.setupUI(findViewById(R.id.clRadio), this);
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		try {
			Utils.trimCache(getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
		endTime = System.currentTimeMillis();
		long timeSpent = endTime - startTime;
		sharedPreferencesSettings.edit().putLong("timeSpentMedia", sharedPreferencesSettings.getLong("timeSpentMedia", 0) + timeSpent).apply();
		super.onDestroy();
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		SharedPreferences sharedPreferencesSettings = newBase.getSharedPreferences("settings", Context.MODE_PRIVATE);
		DictionnaryFetcher.setLangages();
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
			super.attachBaseContext(Utils.wrap(newBase, LANGAGES[sharedPreferencesSettings.getInt("LanguageSet", 19)].getInitial()));
		} else {
			super.attachBaseContext(newBase);
		}
	}
}
