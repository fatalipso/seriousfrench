package ovh.fatalispo.seriousfrench;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

public class DictionnaryFetcher {
	public final static Langage LANGAGESDICO[] = new Langage[54];
	public final static Langage LANGAGES[] = new Langage[55];
	public static final String[] targetLangage = new String[1];
	public static final String[] sourceText = new String[1];
	public static final String[] titleTranslation = new String[1];
	public static final String[] glosbeTranslation = new String[1];

	public static Parcelable callDictionnary(final Context context, final TextView etToTranslate, final Spinner spinner, final TextView tvDictionary, final ImageButton ivAddToList, final TextView tvAddFav, final Parcelable saveInstanceState, final ProgressBar pbDico) {
		final SharedPreferences sharedPreferences = context.getSharedPreferences("radio", 0);
		final SharedPreferences.Editor editor = context.getSharedPreferences("radio", 0).edit();
		if (saveInstanceState != null) spinner.onRestoreInstanceState(saveInstanceState);
		spinner.setSelection(sharedPreferences.getInt("positionLanguage", 0));

		new AsyncTask<String, String, Parcelable>() {
			Parcelable savedInstanceState;

			@Override
			protected Parcelable doInBackground(String... strings) {
				spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
						editor.putString("language", LANGAGESDICO[position].getInitial()).apply();
						targetLangage[0] = LANGAGESDICO[position].getInitial();
						editor.putInt("positionLanguage", position).apply();
//				Toast.makeText(getApplicationContext(), Utils.LANGAGESDICO.get(position).getLangage(), Toast.LENGTH_SHORT).show();
						if (!etToTranslate.getText().toString().isEmpty())
							etToTranslate.onEditorAction(EditorInfo.IME_ACTION_DONE);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {

					}
				});
				savedInstanceState = spinner.onSaveInstanceState();
				etToTranslate.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						etToTranslate.setText("");
					}
				});

				etToTranslate.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(final TextView v, int actionId, KeyEvent event) {
						final String targetLangage = sharedPreferences.getString("language", Locale.getDefault().getLanguage());
						//urls = "https://www.littre.org/definition/" + v.getText().toString();
						Pattern pattern = Pattern.compile("[\\p{Latin}'\\s-]+", Pattern.CASE_INSENSITIVE);
						if (actionId == EditorInfo.IME_ACTION_DONE && pattern.matcher(v.getText().toString()).matches() && !v.getText().toString().equals("") && Utils.isWifiConnected(context,true)) {
							if(v.getText().charAt(v.getText().length() -1) == ' ') v.setText(v.getText().subSequence(0,v.getText().length()-1));
							if (!v.getText().toString().contains(" ") && v.getText().length() < 20) {
								tvDictionary.setText("");
								new AsyncTask<String, Integer, ArrayList<Definition>>() {
									ArrayList<Definition> definitionFrOther = new ArrayList<>();
									int j = 0;

									@Override
									protected ArrayList<Definition> doInBackground(String... strings) {
										publishProgress(10);
										Document doc = null;
										Elements content = null;
										String className = "";


										// Instantiates a client
//										TranslateOptions options = TranslateOptions.newBuilder().setApiKey(v.getContext().getString(R.string.api_key_google_translate)).build();
//										Translate trService = options.getService();
										publishProgress(30);
										// Translates some text with Google
//										Translation translation = trService.translate(v.getText().toString(), Translate.TranslateOption.sourceLanguage("fr"), Translate.TranslateOption.targetLanguage(targetLangage));
										editor.putString("Source text", v.getText().toString()).apply();
										editor.putString("Google translate", "<h4>" + v.getText().toString() + "</h4>").apply();
//										editor.putString("Google translate", "<h4>" + translation.getTranslatedText() + "</h4>").apply();
										sourceText[0] = v.getText().toString();
										titleTranslation[0] = "<h4>" + v.getText().toString() + "</h4>";
//										titleTranslation[0] = "<h4>" + translation.getTranslatedText() + "</h4>";
										publishProgress(45);
										try {
											doc = Jsoup.connect("https://www.glosbe.com/fr/" + targetLangage + "/" + v.getText()).get();
											content = doc.select("li.phraseMeaning");
											switch (targetLangage) {
												case "zh":
												case "ar":
												case "hy":
												case "bn":
												case "my":
												case "ko":
												case "he":
												case "hi":
												case "ja":
												case "mr":
												case "ur":
												case "fa":
												case "th":
													className = "nobold phr";
													break;
												default:
													className = " phr";
													break;
											}
											for (int j = 0; j < content.size() && j < 20; j++) {
												if (content.get(j).children().size() > 3) {
													try {
														definitionFrOther.add(new Definition(content.get(j).getElementsByClass(className).eachText().get(0), content.get(j).select("div.span6").eachText().get(0), content.get(j).select("div.span6").eachText().get(1)));
													} catch (Exception e) {
														//TODO
													}
												} else
													definitionFrOther.add(new Definition(content.get(j).getElementsByClass(className).eachText().get(0)));
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
										publishProgress(70);

										//h3 pour mp3 voice
										//definitionFrOther.add(new Definition(doc.getElementsByClass(" phr").eq(0)+"",doc.select("div.span6").eq(0)+""+doc.select("div.span6>div").eq(1)))
							/*String eachTitle="",contentFr="",contentOther="";
							List<String> listContent = cleanContent.eachText();
							List<String> listTitle = title.eachText();
							for(int i=0; i<listTitle.size() && i < 6;i++){
								eachTitle = listTitle.get(i);
								contentFr = listContent.get(i*2);
								contentOther = listContent.get(i*2+1);
								definitionFrOther.add(new Definition(eachTitle,contentFr,contentOther));
							}*/
										//definitionFrOther.add(new Definition(doc.getElementsByClass(" phr").eq(2)+"",doc.select("div.span6").eq(0)+""+doc.select("div.span6>div").eq(1)));
										publishProgress(100);
										return definitionFrOther;
									}

									@Override
									protected void onProgressUpdate(Integer... values) {
										super.onProgressUpdate(values[0]);
										pbDico.setVisibility(View.VISIBLE);
										pbDico.setProgress(values[0]);
									}

									@Override
									protected void onPostExecute(ArrayList<Definition> s) {
										String toPrint = "<h3>Exemples:</h3><div>";
										for (Definition i : s) {
											j++;
											if (i.getContentFr() != null && i.getContentOther() != null && i.getContentFr().startsWith("fr "))
												return;
											if (i.getContentFr() != null && i.getContentOther() != null)
												toPrint += "<p><b>" + j + ". " + i.getName() + "</b></p><p>" + i.getContentFr() + "</p><p>" + i.getContentOther() + "</p>";
											else
												toPrint += "<p><b>" + j + ". " + i.getName() + "</b></p>";
										}
										toPrint += "</div>";
										if (s.isEmpty()) toPrint = "";
										pbDico.setVisibility(View.INVISIBLE);
										ivAddToList.setVisibility(View.VISIBLE);
										tvAddFav.setVisibility(View.VISIBLE);
										editor.putString("Glosbe translate", toPrint).apply();
										glosbeTranslation[0] = toPrint;
										tvDictionary.setText(Html.fromHtml("<h3>" + sharedPreferences.getString("Google translate", "Nothing to translate") + "</h3>" + toPrint));
										tvDictionary.setMovementMethod(LinkMovementMethod.getInstance());
										Typeface face = Typeface.createFromAsset(context.getAssets(), "font/sourcesansproregular.otf");
										tvDictionary.setTypeface(face);
										tvDictionary.setMovementMethod(new ScrollingMovementMethod());
										super.onPostExecute(s);
									}
								}.execute();
							} else if (v.getText().length() >= 20) {
								Toast.makeText(context, "Maximum lenght 20 digits", Toast.LENGTH_SHORT).show();
							} else if (v.getText().toString().contains(" ")) {
								Toast.makeText(context, "Spaces not allowed, it's a dictionnary, so only 1 word", Toast.LENGTH_SHORT).show();
							}


							//Utils.mediaReader("https://"+deviceLangage+".wiktionary.org/w/index.php?title="+v.getText().toString()+"&printable=yes", tvDictionary);
							//fetch body .definition
//					try{
//						URL url=new URL(urls);
//						URLConnection urlcon=url.openConnection();
//						InputStream stream=urlcon.getInputStream();
//						int i;
//						while((i=stream.read())!=-1){
//							tvDictionary.setText(stream.read());
//						}
//					}catch(Exception e){System.out.println(e);}
							return false;
						} else if (!Utils.isWifiConnected(context,false)) {
							Toast.makeText(context, "Veuillez noter qu'un accès internet est requis." + "\n" + context.getString(R.string.wifi_required), Toast.LENGTH_LONG).show();
						}
						return false;
					}
				});
				return null;
			}
		}.execute();

		return saveInstanceState;
	}

	public static void setLangages() {
		//A
		LANGAGES[0] = new Langage("Afrikaans", "af", R.drawable.afrikaans);
		LANGAGES[1] = new Langage("Shqip", "sq", R.drawable.albanais);
		LANGAGES[2] = new Langage("Deutsch", "de", R.drawable.allemand);
		LANGAGES[3] = new Langage("English", "en", R.drawable.english);
		LANGAGES[4] = new Langage("العربية", "ar", R.drawable.arabe);
		LANGAGES[5] = new Langage("Գրաբար", "hy", R.drawable.armenian);
		LANGAGES[6] = new Langage("Azərbaycan dili", "az", R.drawable.azeri);
		//B
		LANGAGES[7] = new Langage("বাংলা", "bn", R.drawable.bengali);
		LANGAGES[8] = new Langage("Беларуская мова", "be", R.drawable.bielarussia);
		LANGAGES[9] = new Langage("မြန်မာစာ", "my", R.drawable.birman);
		LANGAGES[10] = new Langage("Bosanski jezik", "bs", R.drawable.bosnia);
		LANGAGES[11] = new Langage("български", "bg", R.drawable.bulgaria);
		//C
		LANGAGES[12] = new Langage("官话", "zh", R.drawable.chinese);
		LANGAGES[13] = new Langage("한국말", "ko", R.drawable.korea);
		LANGAGES[14] = new Langage("Hrvatski", "hr", R.drawable.croatia);
		//D
		LANGAGES[15] = new Langage("Dansk", "da", R.drawable.dansk);
		//E
		LANGAGES[16] = new Langage("Español", "es", R.drawable.espagnol);
		LANGAGES[17] = new Langage("Eesti keel", "et", R.drawable.estonian);
		//F
		LANGAGES[18] = new Langage("Suomi", "fi", R.drawable.finland);
		LANGAGES[19] = new Langage("Français", "fr", R.drawable.french);
		//G
		LANGAGES[20] = new Langage("ქართული", "ka", R.drawable.georgia);
		LANGAGES[21] = new Langage("Ελληνικά", "el", R.drawable.greece);
		//H
		LANGAGES[22] = new Langage("עִבְרִית", "he", R.drawable.hebrew);
		LANGAGES[23] = new Langage("हिन्दी", "hi", R.drawable.hindi);
		LANGAGES[24] = new Langage("Magyar nyelv", "hu", R.drawable.hongry);
		//I
		LANGAGES[25] = new Langage("Bahasa Indonesia", "id", R.drawable.indonesia);
		LANGAGES[26] = new Langage("Gaeilge", "ga", R.drawable.irland);
		LANGAGES[27] = new Langage("íslenska", "is", R.drawable.island);
		LANGAGES[28] = new Langage("Italiano", "it", R.drawable.italian);
		//J
		LANGAGES[29] = new Langage("日本語 ", "ja", R.drawable.japanese);
		//K
		LANGAGES[30] = new Langage("ភាសាខ្មែរ", "km", R.drawable.cambodgia);
		LANGAGES[31] = new Langage("Kurdî", "ku", R.drawable.kurd);
		//L
		LANGAGES[32] = new Langage("Latviešu", "lv", R.drawable.lettonian);
		LANGAGES[33] = new Langage("Lietuvių kalba", "lt", R.drawable.lituanian);
		LANGAGES[34] = new Langage("Lëtzebuergesch", "lb", R.drawable.luxembourg);
		//M
		LANGAGES[35] = new Langage("Македонски јазик", "mk", R.drawable.macedonia);
		LANGAGES[36] = new Langage("Bahasa Melayu", "ms", R.drawable.malaisia);
		//N
		LANGAGES[37] = new Langage("Nederlands", "nl", R.drawable.nederlands);
		LANGAGES[38] = new Langage("Bokmål", "nb", R.drawable.norvegian);
		//O
		LANGAGES[39] = new Langage("لشکری", "ur", R.drawable.ourdou);
		//P
		LANGAGES[40] = new Langage("فارسی", "fa", R.drawable.iran);
		LANGAGES[41] = new Langage("Filipino", "fil", R.drawable.filipino);
		LANGAGES[42] = new Langage("Język polski", "pl", R.drawable.polish);
		LANGAGES[43] = new Langage("Português", "pt", R.drawable.portugese);
		//R
		LANGAGES[44] = new Langage("Română", "ro", R.drawable.romanian);
		LANGAGES[45] = new Langage("Русский", "ru", R.drawable.russian);
		//S
		LANGAGES[46] = new Langage("Srpski", "sr", R.drawable.serbia);
		LANGAGES[47] = new Langage("Slovenčina", "sk", R.drawable.solvakia);
		LANGAGES[48] = new Langage("Slovenščina", "sl", R.drawable.slovenia);
		LANGAGES[49] = new Langage("Svenska", "sv", R.drawable.sweden);
		//T
		LANGAGES[50] = new Langage("Čeština", "cs", R.drawable.tcheque);
		LANGAGES[51] = new Langage("ภาษาไทย", "th", R.drawable.thai);
		LANGAGES[52] = new Langage("Türkçe", "tr", R.drawable.turkish);
		//U
		LANGAGES[53] = new Langage("українська мова", "uk", R.drawable.ukranian);
		//V
		LANGAGES[54] = new Langage("Tiếng Việt", "vi", R.drawable.viet);
	}

	public static void setLangagesForDictionary() {
		//A
		LANGAGESDICO[0] = new Langage("Afrikaans", "af", R.drawable.afrikaans);
		LANGAGESDICO[1] = new Langage("Shqip", "sq", R.drawable.albanais);
		LANGAGESDICO[2] = new Langage("Deutsch", "de", R.drawable.allemand);
		LANGAGESDICO[3] = new Langage("English", "en", R.drawable.english);
		LANGAGESDICO[4] = new Langage("العربية", "ar", R.drawable.arabe);
		LANGAGESDICO[5] = new Langage("Գրաբար", "hy", R.drawable.armenian);
		LANGAGESDICO[6] = new Langage("Azərbaycan dili", "az", R.drawable.azeri);
		//B
		LANGAGESDICO[7] = new Langage("বাংলা", "bn", R.drawable.bengali);
		LANGAGESDICO[8] = new Langage("Беларуская мова", "be", R.drawable.bielarussia);
		LANGAGESDICO[9] = new Langage("မြန်မာစာ", "my", R.drawable.birman);
		LANGAGESDICO[10] = new Langage("Bosanski jezik", "bs", R.drawable.bosnia);
		LANGAGESDICO[11] = new Langage("български", "bg", R.drawable.bulgaria);
		//C
		LANGAGESDICO[12] = new Langage("官话", "zh", R.drawable.chinese);
		LANGAGESDICO[13] = new Langage("한국말", "ko", R.drawable.korea);
		LANGAGESDICO[14] = new Langage("Hrvatski", "hr", R.drawable.croatia);
		//D
		LANGAGESDICO[15] = new Langage("Dansk", "da", R.drawable.dansk);
		//E
		LANGAGESDICO[16] = new Langage("Español", "es", R.drawable.espagnol);
		LANGAGESDICO[17] = new Langage("Eesti keel", "et", R.drawable.estonian);
		//F
		LANGAGESDICO[18] = new Langage("Suomi", "fi", R.drawable.finland);
		//G
		LANGAGESDICO[19] = new Langage("ქართული", "ka", R.drawable.georgia);
		LANGAGESDICO[20] = new Langage("Ελληνικά", "el", R.drawable.greece);
		//H
		LANGAGESDICO[21] = new Langage("עִבְרִית", "he", R.drawable.hebrew);
		LANGAGESDICO[22] = new Langage("हिन्दी", "hi", R.drawable.hindi);
		LANGAGESDICO[23] = new Langage("Magyar nyelv", "hu", R.drawable.hongry);
		//I
		LANGAGESDICO[24] = new Langage("Bahasa Indonesia", "id", R.drawable.indonesia);
		LANGAGESDICO[25] = new Langage("Gaeilge", "ga", R.drawable.irland);
		LANGAGESDICO[26] = new Langage("íslenska", "is", R.drawable.island);
		LANGAGESDICO[27] = new Langage("Italiano", "it", R.drawable.italian);
		//J
		LANGAGESDICO[28] = new Langage("日本語 ", "ja", R.drawable.japanese);
		//K
		LANGAGESDICO[29] = new Langage("ភាសាខ្មែរ", "km", R.drawable.cambodgia);
		LANGAGESDICO[30] = new Langage("Kurdî", "ku", R.drawable.kurd);
		//L
		LANGAGESDICO[31] = new Langage("Latviešu", "lv", R.drawable.lettonian);
		LANGAGESDICO[32] = new Langage("Lietuvių kalba", "lt", R.drawable.lituanian);
		LANGAGESDICO[33] = new Langage("Lëtzebuergesch", "lb", R.drawable.luxembourg);
		//M
		LANGAGESDICO[34] = new Langage("Македонски јазик", "mk", R.drawable.macedonia);
		LANGAGESDICO[35] = new Langage("Bahasa Melayu", "ms", R.drawable.malaisia);
		//N
		LANGAGESDICO[36] = new Langage("Nederlands", "nl", R.drawable.nederlands);
		LANGAGESDICO[37] = new Langage("Bokmål", "nb", R.drawable.norvegian);
		//O
		LANGAGESDICO[38] = new Langage("لشکری", "ur", R.drawable.ourdou);
		//P
		LANGAGESDICO[39] = new Langage("فارسی", "fa", R.drawable.iran);
		LANGAGESDICO[40] = new Langage("Filipino", "fil", R.drawable.filipino);
		LANGAGESDICO[41] = new Langage("Język polski", "pl", R.drawable.polish);
		LANGAGESDICO[42] = new Langage("Português", "pt", R.drawable.portugese);
		//R
		LANGAGESDICO[43] = new Langage("Română", "ro", R.drawable.romanian);
		LANGAGESDICO[44] = new Langage("Русский", "ru", R.drawable.russian);
		//S
		LANGAGESDICO[45] = new Langage("Srpski", "sr", R.drawable.serbia);
		LANGAGESDICO[46] = new Langage("Slovenčina", "sk", R.drawable.solvakia);
		LANGAGESDICO[47] = new Langage("Slovenščina", "sl", R.drawable.slovenia);
		LANGAGESDICO[48] = new Langage("Svenska", "sv", R.drawable.sweden);
		//T
		LANGAGESDICO[49] = new Langage("Čeština", "cs", R.drawable.tcheque);
		LANGAGESDICO[50] = new Langage("ภาษาไทย", "th", R.drawable.thai);
		LANGAGESDICO[51] = new Langage("Türkçe", "tr", R.drawable.turkish);
		//U
		LANGAGESDICO[52] = new Langage("українська мова", "uk", R.drawable.ukranian);
		//V
		LANGAGESDICO[53] = new Langage("Tiếng Việt", "vi", R.drawable.viet);
	}
}
