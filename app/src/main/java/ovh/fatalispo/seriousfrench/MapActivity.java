package ovh.fatalispo.seriousfrench;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import org.mapsforge.map.layer.download.tilesource.TileSource;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.IConfigurationProvider;
import org.osmdroid.tileprovider.MapTileProviderBasic;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.TileSourcePolicy;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.TilesOverlay;

import java.util.ArrayList;
import java.util.Map;

public class MapActivity {
	Context mContext;
	Activity mActivity;
	MapView mapView;
	double wallpaper_lat, wallpaper_lng;
	ArrayList<OverlayItem> items;
	DisplayMetrics dm;
	ScaleBarOverlay mScaleBarOverlay;
	boolean isTaped = false, detachedMode;
	ITileSource mapSource;
	double beginZoomLvl, maxZoomLvl;

	public MapActivity(Context mContext, Activity mActivity, MapView mapView, double wallpaper_lat, double wallpaper_lng, ArrayList<OverlayItem> items, double beginZoomLvl, double maxZoomLvl, ITileSource mapSource) {
		this.mContext = mContext;
		this.mActivity = mActivity;
		this.mapView = mapView;
		this.wallpaper_lat = wallpaper_lat;
		this.wallpaper_lng = wallpaper_lng;
		this.items = items;
		dm = mContext.getResources().getDisplayMetrics();
		mScaleBarOverlay = new ScaleBarOverlay(mapView);
		this.mapSource = mapSource;
		this.beginZoomLvl = beginZoomLvl;
		this.maxZoomLvl = maxZoomLvl;
	}

	public void setMapView() {
		mapView.setTileSource(mapSource);
		mapView.setMaxZoomLevel(maxZoomLvl);
		mapView.setBuiltInZoomControls(false);
		mapView.setMultiTouchControls(false);
		setMapLocation(mapView);
//		items.clear();
	}

	public void setMapLocation(MapView map) {
		final IMapController mapController = map.getController();
		mapController.setZoom(beginZoomLvl);
//		GeoPoint startPoint = new GeoPoint(wallpaper_lat, wallpaper_lng);
		final GeoPoint startPoint = new GeoPoint(46.697267,2.6225373);
		mapController.setCenter(startPoint);

		ItemizedOverlayWithFocus<OverlayItem> mOverlay = new ItemizedOverlayWithFocus<OverlayItem>(items, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
			@Override
			public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
				if(isTaped) {
					mapController.animateTo(startPoint, beginZoomLvl,3000l);
					isTaped = false;
				} else {
					mapController.animateTo(new GeoPoint(wallpaper_lat, wallpaper_lng), maxZoomLvl, 3000l);
					isTaped = true;
				}
				return true;
			}

			@Override
			public boolean onItemLongPress(final int index, final OverlayItem item) {

				return false;
			}
		}, mContext);
		mOverlay.setFocusItemsOnTap(true);
//		mapView.getOverlays().clear();
		mapView.getOverlays().add(mOverlay);

		mScaleBarOverlay.setCentred(true);
//play around with these values to get the location on screen in the right place for your application
		mScaleBarOverlay.setScaleBarOffset(dm.widthPixels / 2, 10);
		mapView.getOverlays().add(this.mScaleBarOverlay);
	}

	public void setDetachedMode(boolean detachedMode) {
		this.detachedMode = detachedMode;
	}

}
