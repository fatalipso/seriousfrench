package ovh.fatalispo.seriousfrench.book;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ovh.fatalispo.seriousfrench.CustomWebview;
import ovh.fatalispo.seriousfrench.DictionnaryFetcher;
import ovh.fatalispo.seriousfrench.MediaReader;
import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;
import ovh.fatalispo.seriousfrench.adapter.MassMediaAdapter;
import ovh.fatalispo.seriousfrench.databases.words.WordDataSource;
import ovh.fatalispo.seriousfrench.databases.words.WordLocalDB;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;
import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.setLangagesForDictionary;
import static ovh.fatalispo.seriousfrench.WelcomeScreen.booksList;


public class Books extends Activity implements View.OnTouchListener {
	private Button bShowDico;
	public static ImageButton ibClose, ivAddToList,ibPreviousPage,ibNextPage, ibUnZoomPage, ibZoomPage;
	public static TextView tvTitleDico, etToTranslate, tvDictionary, tvTitle, tvDescription, tvAddFav, tvUrl;
	public static ArrayList<WordLocalDB> wordsListToSave = new ArrayList<>();
	private RelativeLayout rlDico;
	private Spinner spinner;
	private boolean dicoIsClicked;
	public static Books instance;
	private static Handler handler;
	public static int SHOW_PROGRESS = 0;
	protected static WordDataSource datasource;
	protected static SharedPreferences sharedPreferences, sharedPreferencesSettings;
	public long startTime, endTime;
	private RecyclerView rvBooks;
	public static ProgressBar pbDico;
	private ConstraintLayout clWvGlobal;
	protected static Parcelable saveSpinnerState;
	private int xDelta;
	private int yDelta;
	PDFView pdfView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_books);
		sharedPreferences = getSharedPreferences("settings", 0);

//		tvTitleDico = findViewById(R.id.tvTitleDicoBook);
//		etToTranslate = findViewById(R.id.etDraftZoneBook);
//		tvDictionary = findViewById(R.id.tvDictionaryBook);
//		tvDescription = findViewById(R.id.tvDescriptionBook);
//		tvTitle = findViewById(R.id.tvTitleDicoBook);
		tvUrl = findViewById(R.id.tvUrlBook);
		instance = this;


//		bShowDico = findViewById(R.id.bShowDicoBook);
//		rlDico = findViewById(R.id.rlDicoBook);
		ibClose = findViewById(R.id.ibCloseBook);
		ibPreviousPage = findViewById(R.id.ibPreviousPage);
		ibNextPage = findViewById(R.id.ibNextPage);
		ibZoomPage = findViewById(R.id.ibZoomPage);
		ibUnZoomPage = findViewById(R.id.ibUnZoomPage);
//		pbDico = findViewById(R.id.pbDico);

		//rlDico.animate().translationX().setDuration(1*1000);

		rvBooks = findViewById(R.id.rvBooks);
		clWvGlobal = findViewById(R.id.clWvGlobalBook);
		pdfView = findViewById(R.id.pdfView);
//		spinner = findViewById(R.id.psLanguagesBook);
//		ivAddToList = findViewById(R.id.ivAddToList);
//		tvAddFav = findViewById(R.id.tvAddFav);

		setLangagesForDictionary();


//		bShowDico.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (!dicoIsClicked) {
//					Display display = getWindowManager().getDefaultDisplay();
//					Point size = new Point();
//					display.getSize(size);
//					int orientation = getResources().getConfiguration().orientation;
//					int width = size.x;
//					int height = size.y;
//					rlDico.setX(-rlDico.getWidth());
//					rlDico.animate().translationX(rlDico.getWidth() + 30).setDuration(1 * 500);
//					bShowDico.animate().translationX(rlDico.getWidth() + 30).setDuration(1 * 500);
//					dicoIsClicked = true;
//				} else {
//					rlDico.animate().translationX(0).setDuration(1 * 500);
//					bShowDico.animate().translationX(0).setDuration(1 * 500);
//					dicoIsClicked = false;
//				}
//			}
//		});

//		ivAddToList.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				boolean wordAlreadyInList = false;
//				for (int i = 0; i < wordsListToSave.size(); i++) {
//					if (wordsListToSave.get(i).getSourceTxt().equals(sourceText[0])) {
//						wordAlreadyInList = true;
//					}
//				}
//				if (!wordAlreadyInList || wordsListToSave.isEmpty()) {
//					wordsListToSave.add(new WordLocalDB(targetLangage[0], Calendar.getInstance().getTime(), sourceText[0], glosbeTranslation[0], titleTranslation[0]));
//					Toast.makeText(getApplicationContext(), sourceText[0] + " " + getString(R.string.word_added), Toast.LENGTH_LONG).show();
//				} else {
//					Toast.makeText(getApplicationContext(), "\"" + sourceText + "\" " + getString(R.string.word_already_in_list), Toast.LENGTH_LONG).show();
//				}
//				v.setVisibility(View.INVISIBLE);
//				tvAddFav.setVisibility(View.INVISIBLE);
//			}
//		});

//		Adapter_Spinner_Dico mCustomAdapter = new Adapter_Spinner_Dico(this, LANGAGESDICO, R.layout.spinner_dropdown_listitem);
//		spinner.setAdapter(mCustomAdapter);
//
//		saveSpinnerState = callDictionnary(getApplicationContext(), etToTranslate, spinner, tvDictionary, ivAddToList, tvAddFav, saveSpinnerState, pbDico);

		ibClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (pdfView != null) {
					pdfView.recycle();
				}
				clWvGlobal.setVisibility(View.INVISIBLE);
				rvBooks.setVisibility(View.VISIBLE);
			}
		});

		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(150, 50);
		layoutParams.leftMargin = 50;
		layoutParams.topMargin = 50;
		layoutParams.bottomMargin = -250;
		layoutParams.rightMargin = -250;

		if (booksList.isEmpty()) {
			booksList.addAll(Utils.initializeStreams(getApplicationContext(), "book"));
		}

		rvBooks.setLayoutManager(new GridLayoutManager(this, 2));
		sharedPreferences.edit().putBoolean("showWebview", true).apply();
		rvBooks.setAdapter(new MassMediaAdapter(booksList, ibClose, rvBooks, "book", clWvGlobal, tvUrl, pdfView, ibPreviousPage, ibNextPage, ibZoomPage, ibUnZoomPage));

		((ViewGroup)pdfView.getParent()).setOnTouchListener(this);
//		new AsyncTask<String, String, String>() {
//			@Override
//			protected String doInBackground(String... strings) {
//				datasource = new WordDataSource(getApplicationContext());
//				datasource.open();
//				wordsListToSave.addAll(datasource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null));
//				for (WordLocalDB word : wordsListToSave) {
//					Log.d("Affichage WordList", "target language: " + word.getTargetLg() + "    source text: " + word.getSourceTxt() + "    translattion google: " + word.getTargetTxtGoogle() + "    translation glosbe: " + word.getTargetTxtGlosbe());
//				}
//				datasource.close();
//				return null;
//			}
//		}.execute();
	}

//	@Override
//	protected void onSaveInstanceState(Bundle outState) {
//		outState.putString("motDico", etToTranslate.getText().toString());
//		outState.putString("textDico", tvDictionary.getText().toString());
//		outState.putInt("showWV", clWvGlobal.getVisibility());
//		outState.putString("urlToLoadBack", wvBook.getUrl());
//		super.onSaveInstanceState(outState);
//	}
//
//	@Override
//	protected void onRestoreInstanceState(Bundle savedInstanceState) {
//		super.onRestoreInstanceState(savedInstanceState);
//		etToTranslate.setText(savedInstanceState.getString("motDico"));
//		tvDictionary.setText(savedInstanceState.getString("textDico"));
//		clWvGlobal.setVisibility(savedInstanceState.getInt("showWV"));
//		if (savedInstanceState.getInt("showWV") == View.VISIBLE) {
//			mediaReader = new MediaReader(savedInstanceState.getString("urlToLoadBack"), wvBook, pbWebView, urlsList);
//			mediaReader.setMedia();
//		}
//	}

	@Override
	protected void onResume() {
		super.onResume();
		SHOW_PROGRESS = 0;
		startTime = System.currentTimeMillis();
		Utils.setupUI(findViewById(R.id.rvBooks), this);
		Utils.setupUI(findViewById(R.id.clWvGlobalBook), this);
	}

	@Override
	protected void onPause() {
		super.onPause();
//		new AsyncTask<String, String, String>() {
//			@Override
//			protected String doInBackground(String... strings) {
//				datasource = new WordDataSource(getApplicationContext());
//				datasource.open();
//				ArrayList<WordLocalDB> wordslistsss = new ArrayList<>();
//				wordslistsss.addAll(datasource.getAllStreamOfThisType(MySQLiteHelper.TABLE_WORDS, null, null));
//				for (int j = 0; j < wordslistsss.size(); j++) {
//					Log.d("Show me db", wordslistsss.get(j).getSourceTxt());
//				}
//
//				for (int i = 0; i < wordsListToSave.size(); i++) {
//					datasource.wordUpdater(new WordLocalDB(wordsListToSave.get(i).getTargetLg(), wordsListToSave.get(i).getDate(), wordsListToSave.get(i).getSourceTxt(), wordsListToSave.get(i).getTargetTxtGlosbe(), wordsListToSave.get(i).getTargetTxtGoogle()));
//				}
//				datasource.close();
//				return null;
//			}
//		}.execute();

	}

	@Override
	protected void onDestroy() {
		endTime = System.currentTimeMillis();
		long timeSpent = endTime - startTime;
		sharedPreferences.edit().putLong("timeSpentBook", sharedPreferences.getLong("timeSpentBook", 0) + timeSpent).apply();
		super.onDestroy();
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		SharedPreferences sharedPreferences = newBase.getSharedPreferences("settings", Context.MODE_PRIVATE);
		DictionnaryFetcher.setLangages();
		if (!sharedPreferences.getBoolean("LanguageHasBeenSet", false)) {
			super.attachBaseContext(Utils.wrap(newBase, Utils.languageInit()));
		} else {
			super.attachBaseContext(Utils.wrap(newBase, LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()));
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		final int X = (int) event.getRawX();
		final int Y = (int) event.getRawY();
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
				xDelta = X - lParams.leftMargin;
				yDelta = Y - lParams.topMargin;
				break;
			case MotionEvent.ACTION_UP:
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				break;
			case MotionEvent.ACTION_POINTER_UP:
				break;
			case MotionEvent.ACTION_MOVE:
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
				layoutParams.leftMargin = X - xDelta;
				layoutParams.topMargin = Y - yDelta;
				layoutParams.rightMargin = -250;
				layoutParams.bottomMargin = -250;
				v.setLayoutParams(layoutParams);
				break;
		}
		((ViewGroup)v.getParent()).invalidate();
		return true;
	}
}
