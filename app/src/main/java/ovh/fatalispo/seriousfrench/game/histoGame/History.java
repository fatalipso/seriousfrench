package ovh.fatalispo.seriousfrench.game.histoGame;

public class History {
	private String date, fact_fr, fact_other, period, level;

	public History(String period, String date,String level, String fact_fr, String fact_other) {
		this.date = date;
		this.fact_fr = fact_fr;
		this.fact_other = fact_other;
		this.period = period;
		this.level = level;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFact_fr() {
		return fact_fr;
	}

	public void setFact_fr(String fact_fr) {
		this.fact_fr = fact_fr;
	}

	public String getFact_other() {
		return fact_other;
	}

	public void setFact_other(String fact_other) {
		this.fact_other = fact_other;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
}
