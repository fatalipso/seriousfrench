package ovh.fatalispo.seriousfrench.game.histoGame;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import ovh.fatalispo.seriousfrench.DictionnaryFetcher;
import ovh.fatalispo.seriousfrench.ObjectSerializer;
import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;

public class HistoFragment extends Fragment {

	private SharedPreferences sharedPreferences,sharedPreferencesSettings;
	private TextView tvTimer, tvHisto, tvHistoFr, tvCurrentLevel, tvPointsGeo, tvCorrectDate;
	private Button tvGameWhere1, tvGameWhere2, tvGameWhere3, tvGameWhere4, bNextQuestion, ibLevelUp, easyLevel, mediumLevel, hardLevel;
	private ImageButton ivInfoHisto, ivListenTTSHistoFr;
	protected ImageView ivPointsGeo, ivMapParis, ivParisPhoto;
	private int histoPoints, histoLevel, rand, maxScore, winTime, nmbTry, randForSelectType, selectedLevel = 4, unlockedContent;
	private boolean answer = true, hideInfo = true, isTalking = false, hasCorrectlyAnswered = false, isPaused = false, fromCreateView = false;
	private ConstraintLayout clInfoHisto, clLevels;
	private ArrayList<History> csvHistory, csvHistoryRand;
	private static MyCount histoCounter;
	String correctDisplay;
	ArrayList<Integer> uniqueRand;
	View view;
	Toast toast;
	public long startTime, endTime;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_histo, container, false);
		sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("games", 0);
		sharedPreferencesSettings = getActivity().getApplicationContext().getSharedPreferences("settings", 0);
		csvHistory = new ArrayList<>();
		DictionnaryFetcher.setLangages();

		clInfoHisto = view.findViewById(R.id.clInfoHisto);

		tvHisto = view.findViewById(R.id.tvHisto);
		tvHistoFr = view.findViewById(R.id.tvHistoFrench);
		tvCorrectDate = view.findViewById(R.id.tvCorrectDate);

		tvGameWhere1 = view.findViewById(R.id.tvGameWhere1);
		tvGameWhere2 = view.findViewById(R.id.tvGameWhere2);
		tvGameWhere3 = view.findViewById(R.id.tvGameWhere3);
		tvGameWhere4 = view.findViewById(R.id.tvGameWhere4);

		bNextQuestion = view.findViewById(R.id.bNextQuestion);

		ivInfoHisto = view.findViewById(R.id.ivInfoHisto);
		ivListenTTSHistoFr = view.findViewById(R.id.ivListenTTSHistoFr);

		tvPointsGeo = getActivity().findViewById(R.id.tvPointsGeo);
		tvCurrentLevel = getActivity().findViewById(R.id.tvCurrentLevel);
		ivPointsGeo = getActivity().findViewById(R.id.ivPointsGeo);
		ibLevelUp = getActivity().findViewById(R.id.ibLevel);
		tvTimer = getActivity().findViewById(R.id.tvTimer);
		easyLevel = getActivity().findViewById(R.id.easyLevel);
		mediumLevel = getActivity().findViewById(R.id.mediumLevel);
		hardLevel = getActivity().findViewById(R.id.hardLevel);
		clLevels = getActivity().findViewById(R.id.clLevels);

		unlockedContent = sharedPreferences.getInt("unlockedContent", 4);
		histoLevel = sharedPreferences.getInt("histoLevel", 1);

		nmbTry = 1;
		startGame();

		bNextQuestion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				clInfoHisto.setVisibility(View.INVISIBLE);
				tvCorrectDate.setVisibility(View.INVISIBLE);
				hideInfo = true;
				hasCorrectlyAnswered = false;
				nmbTry = 1;
				startGame();
			}
		});

		ibLevelUp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideInfo) {
					clLevels.setVisibility(View.VISIBLE);
					hideInfo = false;
				} else {
					clLevels.setVisibility(View.INVISIBLE);
					hideInfo = true;
				}
			}
		});

		clInfoHisto.setVisibility(View.VISIBLE);

		ivInfoHisto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideInfo) {
					clInfoHisto.setVisibility(View.VISIBLE);
					hideInfo = false;
				} else {
					clInfoHisto.setVisibility(View.INVISIBLE);
					hideInfo = true;
				}
			}
		});

		if (histoLevel < 4) {
			easyLevel.setEnabled(true);
			mediumLevel.setEnabled(false);
			hardLevel.setEnabled(false);
		}
		if (histoLevel > 3 && histoLevel <= 7) {
			easyLevel.setEnabled(true);
			mediumLevel.setEnabled(true);
			hardLevel.setEnabled(false);
		}
		if (histoLevel > 7) {
			easyLevel.setEnabled(true);
			mediumLevel.setEnabled(true);
			hardLevel.setEnabled(true);
		}

		easyLevel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(selectedLevel!=0) {
					selectedLevel = 0;
					bNextQuestion.performClick();
				} else
					clLevels.setVisibility(View.INVISIBLE);
			}
		});

		mediumLevel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(selectedLevel!=1) {
					selectedLevel = 1;
					bNextQuestion.performClick();
				} else
					clLevels.setVisibility(View.INVISIBLE);
			}
		});

		hardLevel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(selectedLevel!=2) {
					selectedLevel = 2;
					bNextQuestion.performClick();
				} else
					clLevels.setVisibility(View.INVISIBLE);
			}
		});

		return view;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}


	@Override
	public void onResume() {
		super.onResume();
		startTime = System.currentTimeMillis();
		clInfoHisto.setVisibility(View.INVISIBLE);
		histoPoints = sharedPreferences.getInt("histoPoints", 0);
		histoLevel = sharedPreferences.getInt("histoLevel", 1);
		selectedLevel = sharedPreferences.getInt("selectedLvl",0);
		maxScore = sharedPreferences.getInt("histoMaxPoints", 50);
		if(csvHistory.isEmpty()) {
			try {
				csvHistory = (ArrayList<History>) ObjectSerializer.deserialize(sharedPreferences.getString("csvHistory", ObjectSerializer.serialize(new ArrayList<History>())));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		hasCorrectlyAnswered = sharedPreferences.getBoolean("hasCorrectlyAnswered", false);
		if (!isPaused) {
			histoCounter.cancel();
			histoCounter = new HistoFragment.MyCount(30000, 1000);
			hasCorrectlyAnswered = false;
			nmbTry = 1;
			startGame();
		}
		tvPointsGeo.setText(histoPoints + "/" + maxScore);
		tvCurrentLevel.setText(getString(R.string.game_rank) + histoLevel);
		if (histoPoints == 0)
			ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
		else if (histoPoints > 0)
			ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
		else
			ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
	}

	@Override
	public void onPause() {
		super.onPause();
		isPaused = true;
		sharedPreferences.edit().putInt("selectedLvl",selectedLevel).apply();
		sharedPreferences.edit().putInt("histoRand", rand).apply();
		sharedPreferences.edit().putInt("histoPoints", histoPoints).apply();
		sharedPreferences.edit().putInt("histoLevel", histoLevel).apply();
		sharedPreferences.edit().putLong("pauseHistoGame", winTime).apply();
		sharedPreferences.edit().putInt("histoMaxPoints", maxScore).apply();
		sharedPreferences.edit().putBoolean("hasCorrectlyAnswered", hasCorrectlyAnswered).apply();
		try {
			sharedPreferences.edit().putString("csvHistory", ObjectSerializer.serialize(csvHistory)).apply();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (histoCounter != null) histoCounter.cancel();
		view = null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		sharedPreferences.edit().putInt("histoPoints", histoPoints).apply();
		endTime = System.currentTimeMillis();
		long timeSpent = endTime - startTime;
		sharedPreferencesSettings.edit().putLong("timeSpentHisto", sharedPreferencesSettings.getLong("timeSpentHisto",0)+timeSpent).apply();
	}

	private void readCVSFromAssetFolder(String csvFile) {
		String[] content = null;
		try {
			InputStream inputStream = getActivity().getAssets().open("histo/"+csvFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			Log.d("Test", " " + br.readLine());
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				content = line.split(",");
				csvHistory.add(new History(content[0].replaceAll("^\"|\"$", "").replaceAll("null", "").replaceAll("_",","), content[1].replaceAll("^\"|\"$", "").replaceAll("null", "").replaceAll("_",","), content[2].replaceAll("^\"|\"$", "").replaceAll("null", "").replaceAll("_",","), content[3].replaceAll("^\"|\"$", "").replaceAll("null", "").replaceAll("_",","), content[4].replaceAll("^\"|\"$", "").replaceAll("null", "").replaceAll("_",",")));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void printCVSContent() {
		String  hasardous1, hasardous2, hasardous3, todisplay;
		csvHistoryRand = new ArrayList<>();
		ivListenTTSHistoFr.setVisibility(View.VISIBLE);
		if (!fromCreateView) rand = new Random().nextInt(4);
		if (selectedLevel == 0 || histoLevel < 6) {
			if (!fromCreateView) uniqueRand = Utils.uniqueRand(0, 114);
			for (int i = 0; i < 4; i++) {
				csvHistoryRand.add(new History(csvHistory.get(uniqueRand.get(i)).getPeriod(), csvHistory.get(uniqueRand.get(i)).getDate(), csvHistory.get(uniqueRand.get(i)).getLevel(), csvHistory.get(uniqueRand.get(i)).getFact_fr(), csvHistory.get(uniqueRand.get(i)).getFact_other()));
			}
		} else if (selectedLevel == 1 || histoLevel > 5 && histoLevel < 10) {
			if (!fromCreateView) uniqueRand = Utils.uniqueRand(395, 525);
			for (int i = 0; i < 4; i++) {
				csvHistoryRand.add(new History(csvHistory.get(uniqueRand.get(i)).getPeriod(), csvHistory.get(uniqueRand.get(i)).getDate(), csvHistory.get(uniqueRand.get(i)).getLevel(), csvHistory.get(uniqueRand.get(i)).getFact_fr(), csvHistory.get(uniqueRand.get(i)).getFact_other()));
			}
		} else {
			if (!fromCreateView) uniqueRand = Utils.uniqueRand(114,396);
			for (int i = 0; i < 4; i++) {
				csvHistoryRand.add(new History(csvHistory.get(uniqueRand.get(i)).getPeriod(), csvHistory.get(uniqueRand.get(i)).getDate(), csvHistory.get(uniqueRand.get(i)).getLevel(), csvHistory.get(uniqueRand.get(i)).getFact_fr(), csvHistory.get(uniqueRand.get(i)).getFact_other()));
			}
		}

		correctDisplay = csvHistoryRand.get(rand).getDate();
		todisplay = csvHistoryRand.get(rand).getDate();

		if(!Utils.isWifiConnected(getActivity().getApplicationContext(),false)) {
			ivListenTTSHistoFr.setVisibility(View.INVISIBLE);
		} else
			ivListenTTSHistoFr.setVisibility(View.VISIBLE);
		if(!Utils.readTheWord(ivListenTTSHistoFr, csvHistoryRand.get(rand).getFact_fr(), getActivity().getApplicationContext())) {
			try {
				Intent intent = new Intent();
				intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
				startActivityForResult(intent, 0);
			} catch(ActivityNotFoundException exception) {
				if(Utils.isWifiConnected(getActivity().getApplicationContext(),true)) {
					Uri uri = Uri.parse("market://details?id=" + "com.google.android.tts&hl=" + LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()/* + LANGAGESDICO[selectedLanguagePos].getInitial()context.getPackageName()*/);
					Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
					// To count with Play market backstack, After pressing back button,
					// to taken back to our application, we need to add following flags to intent.
					goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
					try {
						startActivity(goToMarket);
					} catch (ActivityNotFoundException e) {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "com.google.android.tts&hl=" + LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()/* + LANGAGESDICO[selectedLanguagePos].getInitial()context.getPackageName()*/)));
					}
				}
			}
		}
		tvHisto.setText(csvHistoryRand.get(rand).getFact_other());
		tvHistoFr.setText(csvHistoryRand.get(rand).getFact_fr());

		if(todisplay.length()>4) todisplay = correctDisplay.substring(todisplay.length()-4).replace(" ","");
		int wordBeforeLast = todisplay.length()-2;
		ArrayList<Integer> listOfRandNum = Utils.uniqueRand(0,99); listOfRandNum.addAll(Utils.uniqueRand(0,99));

		for(int i = 0; i < listOfRandNum.size(); i++) {
			if(listOfRandNum.get(i) < 10) listOfRandNum.set(listOfRandNum.indexOf(listOfRandNum.get(i)), Utils.uniqueRand(10,99).get(0));
		}

		hasardous1 = (todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(0)).equals(todisplay)? ""+(Integer.parseInt(todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(0))-1):todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(0);
		hasardous2 = (todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(1)).equals(todisplay)? ""+(Integer.parseInt(todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(1))-1):todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(1);
		hasardous3 = (todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(2)).equals(todisplay)? ""+(Integer.parseInt(todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(2))-1):todisplay.substring(0, wordBeforeLast)+listOfRandNum.get(2);

		if(Integer.parseInt(todisplay)>2000)  {
			hasardous1 = todisplay.substring(0, wordBeforeLast)+"0"+ new Random().nextInt(10);
			hasardous2 = todisplay.substring(0, wordBeforeLast)+"1"+ new Random().nextInt(10);
			hasardous3 = todisplay.substring(0, wordBeforeLast)+"0"+ new Random().nextInt(10);
		}

		switch(rand) {
			case 0:
				tvGameWhere1.setText(todisplay);
				tvGameWhere2.setText(hasardous1);
				tvGameWhere3.setText(hasardous2);
				tvGameWhere4.setText(hasardous3);
				break;
			case 1:
				tvGameWhere1.setText(hasardous1);
				tvGameWhere2.setText(todisplay);
				tvGameWhere3.setText(hasardous2);
				tvGameWhere4.setText(hasardous3);
				break;
			case 2:
				tvGameWhere1.setText(hasardous1);
				tvGameWhere2.setText(hasardous2);
				tvGameWhere3.setText(todisplay);
				tvGameWhere4.setText(hasardous3);
				break;
			case 3:
				tvGameWhere1.setText(hasardous1);
				tvGameWhere2.setText(hasardous2);
				tvGameWhere3.setText(hasardous3);
				tvGameWhere4.setText(todisplay);
				break;
		}


		tvGameWhere1.setTag("0");
		tvGameWhere1.setEnabled(true);
		tvGameWhere2.setTag("1");
		tvGameWhere2.setEnabled(true);
		tvGameWhere3.setTag("2");
		tvGameWhere3.setEnabled(true);
		tvGameWhere4.setTag("3");
		tvGameWhere4.setEnabled(true);

		histoGame(rand);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
		}
	}

	private boolean histoGame(final int correctAnswerID) {

		View.OnClickListener listener = new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				resetButton();
				if (v.getTag().equals("" + correctAnswerID)) {
					if(correctDisplay.length()>4) {
						tvCorrectDate.setVisibility(View.VISIBLE);
						tvCorrectDate.setText(correctDisplay);
					}
					winner();
					v.setBackgroundColor(getResources().getColor(R.color.green_correct));
					v.animate().setDuration(1000).rotation(360).start();
				} else {
					looser();
					if(nmbTry == 3 && correctDisplay.length()>4) {
						tvCorrectDate.setVisibility(View.VISIBLE);
						tvCorrectDate.setText(correctDisplay);
					}
					v.setBackgroundColor(getResources().getColor(R.color.red_error));
				}
				tvPointsGeo.setText(histoPoints + "/" + maxScore);
				if (histoPoints == 0)
					ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
			}
		};

		tvGameWhere1.setOnClickListener(listener);
		tvGameWhere2.setOnClickListener(listener);
		tvGameWhere3.setOnClickListener(listener);
		tvGameWhere4.setOnClickListener(listener);

		return answer;
	}

	private void startGame() {
		if (histoCounter != null) histoCounter.cancel();
		histoCounter = new MyCount(59000, 1000);

		bNextQuestion.setVisibility(View.INVISIBLE);
		resetButton();
		tvTimer.setTextColor(getResources().getColor(R.color.red_error));

		if(sharedPreferencesSettings.getInt("LanguageSet", 0)!=sharedPreferencesSettings.getInt("LanguageFirstGameLaunch", 0) || csvHistory.isEmpty()) {
			sharedPreferencesSettings.edit().putInt("LanguageFirstGameLaunch", sharedPreferencesSettings.getInt("LanguageSet", 19)).apply();
			readCVSFromAssetFolder("history_" + LANGAGES[sharedPreferencesSettings.getInt("LanguageSet", 19)].getInitial() + ".csv");
		}
		printCVSContent();

		if (!hasCorrectlyAnswered) histoCounter.start();
	}

	private void winner() {
		hasCorrectlyAnswered = true;
		setWinnerPoints();
		histoCounter.onFinish();
		tvTimer.setTextColor(getResources().getColor(R.color.green_correct));
		showCorrectAnswer();
		ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
	}

	private void looser() {
		nmbTry++;
		hasCorrectlyAnswered = false;
		setLoserPoints(5);
		if (nmbTry == 3) {
			histoCounter.onTick(1500);
			histoCounter.onFinish();
			histoCounter.cancel();
			tvTimer.setTextColor(getResources().getColor(R.color.red_error));
			tvTimer.setText(getString(R.string.game_lost));
			showCorrectAnswer();
		}
		ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
	}

	private void showCorrectAnswer() {
		if (tvGameWhere1.getTag().equals("" + rand))
			tvGameWhere1.setBackgroundColor(getResources().getColor(R.color.green_correct));
		if (tvGameWhere2.getTag().equals("" + rand))
			tvGameWhere2.setBackgroundColor(getResources().getColor(R.color.green_correct));
		if (tvGameWhere3.getTag().equals("" + rand))
			tvGameWhere3.setBackgroundColor(getResources().getColor(R.color.green_correct));
		if (tvGameWhere4.getTag().equals("" + rand))
			tvGameWhere4.setBackgroundColor(getResources().getColor(R.color.green_correct));
		tvGameWhere1.setEnabled(false);
		tvGameWhere2.setEnabled(false);
		tvGameWhere3.setEnabled(false);
		tvGameWhere4.setEnabled(false);
		bNextQuestion.setVisibility(View.VISIBLE);
		if (histoPoints > maxScore) {
			histoLevel += 1;
			tvCurrentLevel.setText(getString(R.string.game_rank) + histoLevel);
			if (histoLevel == 4) {
				mediumLevel.setEnabled(true);
				contentUnlocked(Gravity.TOP, R.layout.toast_unlocked_lvl, R.id.relativeLayout1, 0, "");
			} else if (histoLevel == 6) {
				hardLevel.setEnabled(true);
				contentUnlocked(Gravity.TOP, R.layout.toast_unlocked_lvl, R.id.relativeLayout1, 0, "");
			}
			Toast.makeText(getActivity(), getString(R.string.game_nextrank), Toast.LENGTH_LONG).show();
			maxScore *= 2;
		}
	}

	private void setWinnerPoints() {
		int finalPoints = 60 - winTime;
		String points = "";
		if (winTime >= 45) {
			points = "+10";
			histoPoints += 10;
		} else if (winTime < 45 && winTime >= 30) {
			histoPoints += 5;
			points = "+5";
		} else if (winTime < 30 && winTime >= 20) {
			histoPoints += 4;
			points = "+4";
		} else if (winTime < 20 && winTime >= 10) {
			histoPoints += 3;
			points = "+3";
		} else if (winTime < 10 && winTime >= 5) {
			histoPoints += 2;
			points = "+2";
		} else if (winTime < 5 && winTime > 1) {
			histoPoints += 1;
			points = "+1";
		}
		contentUnlocked(Gravity.RIGHT | Gravity.TOP, R.layout.toast_points, R.id.relativeLayout2, R.color.green_correct, points);
		tvTimer.setText(getString(R.string.game_time_won) + finalPoints + "s'");
	}

	private void setLoserPoints(int pointsToLose) {
		if (histoPoints >= 5) {
			contentUnlocked(Gravity.RIGHT | Gravity.TOP, R.layout.toast_points, R.id.relativeLayout2, R.color.red_error, "-"+pointsToLose);
			histoPoints -= 5;
		} else histoPoints = 0;
	}

	private void resetButton() {
		tvGameWhere1.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere2.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere3.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere4.setBackground(getResources().getDrawable(R.drawable.selector_effect));
	}

	private void contentUnlocked(int gravity, int layoutToast, int relativeLayout, int colorTxt, String text) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		TextView tvPointsPlusOrMinus;
		ImageView ivPointsPlusOrMinus;
		View view = inflater.inflate(layoutToast, (ViewGroup) getActivity().findViewById(relativeLayout));
		if (layoutToast == R.layout.toast_points) {
			tvPointsPlusOrMinus = view.findViewById(R.id.tvPointsPlusOrMinus);
			ivPointsPlusOrMinus = view.findViewById(R.id.ivPointsPlusOrMinus);
			tvPointsPlusOrMinus.setTextColor(getResources().getColor(colorTxt));
			tvPointsPlusOrMinus.setText(text);
			switch (colorTxt) {
				case R.color.red_error:
					ivPointsPlusOrMinus.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
					break;
				case R.color.green_correct:
					ivPointsPlusOrMinus.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
					break;
			}
		}
		if(toast != null) toast.cancel();
		toast = new Toast(getActivity().getApplicationContext());
		toast.setView(view);
		toast.setGravity(gravity, 0, 150);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.show();
	}


	public class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			if (!hasCorrectlyAnswered && getActivity() != null || nmbTry == 3) {
				setLoserPoints(10);
				tvPointsGeo.setText(histoPoints + "/" + maxScore);
				if (histoPoints == 0)
					ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
				ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
				tvTimer.setText(getString(R.string.game_lost));
				if (winTime == 1) showCorrectAnswer();
			}
			if (hasCorrectlyAnswered && getActivity() != null) {
				tvPointsGeo.setText(histoPoints + "/" + maxScore);
				ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
			}
		}

		@Override
		public void onTick(long millisUntilFinished) {
			if (!hasCorrectlyAnswered && nmbTry < 3) {
				tvTimer.setText(getString(R.string.game_time)+" " + millisUntilFinished / 1000 + "s'");
				winTime = (int) (millisUntilFinished / 1000);
			}
		}
	}
}
