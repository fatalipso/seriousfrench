package ovh.fatalispo.seriousfrench.game.geoGame;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;

public class GeoFragment extends Fragment {

	private SharedPreferences sharedPreferences,sharedPreferencesSettings;
	private TextView tvTimer, tvNumArr, tvNameArrondissement, tvPopArr, tvSupArr, tvCurrentLevel, tvDensArr, tvQuartiers, tvImgDescription, tvMonuments, tvCopyrights, tvShortDescription, tvPointsGeo;
	private Button tvGameWhere1, tvGameWhere2, tvGameWhere3, tvGameWhere4, bNextQuestion, ibLevels, parisLevel, regionLevel, deptLevel;
	private ImageButton ivInfoCIty, ivListenTTSName, ivListenTTSQuartier, ivListenTTSMonuments, ivListenTTSSummarize;
	protected ImageView ivPointsGeo, ivMapParis, ivParisPhoto;
	private String arrondissement;
	private int geoPoints, geoLevel, rand, maxScore, winTime, nmbTry, randForSelectType, selectedLevel = 4, unlockedContent;
	private boolean answer = true, hideInfo = true, isTalking = false, hasCorrectlyAnswered = false, isPaused = false, fromCreateView = false;
	private ConstraintLayout clInfoCity, clLevels;
	private ArrayList<Paris> csvParis, csvParisRand;
	private ArrayList<Regions> csvReg, csvRegRand;
	private ArrayList<Departements> csvDep, csvDepRand;
	private static MyCount counter;
	private String geoLevels[] = {"paris.csv", "french_reg_bis.csv", "french_dep_bis.csv"};
	ArrayList<Integer> uniqueRand;
	View view;
	Toast toast;
	public long startTime, endTime;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_geo, container, false);
		sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("games", 0);
		sharedPreferencesSettings = getActivity().getApplicationContext().getSharedPreferences("settings", 0);
		csvParis = new ArrayList<>();
		csvReg = new ArrayList<>();
		csvDep = new ArrayList<>();


		clInfoCity = view.findViewById(R.id.clInfoCity);

		tvGameWhere1 = view.findViewById(R.id.tvGameWhere1);
		tvGameWhere2 = view.findViewById(R.id.tvGameWhere2);
		tvGameWhere3 = view.findViewById(R.id.tvGameWhere3);
		tvGameWhere4 = view.findViewById(R.id.tvGameWhere4);
		tvNumArr = view.findViewById(R.id.tvNumArrondissement);
		tvNameArrondissement = view.findViewById(R.id.tvNameArrondissement);
		tvPopArr = view.findViewById(R.id.tvPopArrondissement);
		tvSupArr = view.findViewById(R.id.tvSupArrondissement);
		tvDensArr = view.findViewById(R.id.tvDensPopArrondissement);
		tvQuartiers = view.findViewById(R.id.tvQuartiers);
		tvMonuments = view.findViewById(R.id.tvMonuments);
		tvCopyrights = view.findViewById(R.id.tvCopyrights);
		tvShortDescription = view.findViewById(R.id.tvShortDescription);
		tvImgDescription = view.findViewById(R.id.tvImgDescription);

		bNextQuestion = view.findViewById(R.id.bNextQuestion);

		ivInfoCIty = view.findViewById(R.id.ivInfoCIty);
		ivParisPhoto = view.findViewById(R.id.ivParisPhoto);
		ivMapParis = view.findViewById(R.id.ivMapParis);
		ivListenTTSMonuments = view.findViewById(R.id.ivListenTTSMonuments);
		ivListenTTSName = view.findViewById(R.id.ivListenTTSName);
		ivListenTTSQuartier = view.findViewById(R.id.ivListenTTSQuartier);
		ivListenTTSSummarize = view.findViewById(R.id.ivListenTTSSummarize);

		tvPointsGeo = getActivity().findViewById(R.id.tvPointsGeo);
		tvCurrentLevel = getActivity().findViewById(R.id.tvCurrentLevel);
		ivPointsGeo = getActivity().findViewById(R.id.ivPointsGeo);
		ibLevels = getActivity().findViewById(R.id.ibLevel);
		tvTimer = getActivity().findViewById(R.id.tvTimer);
		parisLevel = getActivity().findViewById(R.id.easyLevel);
		regionLevel = getActivity().findViewById(R.id.mediumLevel);
		deptLevel = getActivity().findViewById(R.id.hardLevel);
		clLevels = getActivity().findViewById(R.id.clLevels);

		unlockedContent = sharedPreferences.getInt("unlockedContent", 4);
		geoLevel = sharedPreferences.getInt("geoLevel", 1);

//		if (savedInstanceState != null) {
//			geoPoints = savedInstanceState.getInt("geoPoints", 0);
//			geoLevel = savedInstanceState.getInt("geoLevel", 1);
//			maxScore = savedInstanceState.getInt("geoMaxPoints", 50);
//			isPaused = savedInstanceState.getBoolean("gamePause", false);
//			rand = sharedPreferences.getInt("geoRand", new Random().nextInt(4));
//			nmbTry = savedInstanceState.getInt("try", 1);
//			winTime = (int) sharedPreferences.getLong("pauseGeoGame", 30);
//			randForSelectType = savedInstanceState.getInt("randForSelectType", 0);
//			hasCorrectlyAnswered = sharedPreferences.getBoolean("hasCorrectlyAnswered", false);
//			if (selectedLevel == 0 || geoLevel < 4) readCVSFromAssetFolder(geoLevels[0]);
//			else if (selectedLevel == 1 || geoLevel < 6) readCVSFromAssetFolder(geoLevels[1]);
//			else if (selectedLevel == 2 || geoLevel > 5) readCVSFromAssetFolder(geoLevels[2]);
//
//			if (winTime > 1 && !hasCorrectlyAnswered) {
//				counter = new MyCount(winTime * 1000, 1000);
//			}
//			try {
//				uniqueRand = (ArrayList<Integer>) ObjectSerializer.deserialize(savedInstanceState.getString("randList", ObjectSerializer.serialize(new ArrayList<Integer>())));
//			} catch (ClassNotFoundException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			if (uniqueRand != null) {
//				fromCreateView = true;
//				printCVSContent();
//				fromCreateView = false;
//
//				geoParisGame(rand);
//				if (!hasCorrectlyAnswered && nmbTry < 3) counter.start();
//				else if (hasCorrectlyAnswered || !hasCorrectlyAnswered && nmbTry == 3) {
//					if (hasCorrectlyAnswered) {
//						setWinnerPoints();
//						tvTimer.setTextColor(getResources().getColor(R.color.green_correct));
//						ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
//					} else {
//						tvTimer.setTextColor(getResources().getColor(R.color.red_error));
//						tvTimer.setText("La prochaine, c'est la bonne ! Courage");
//						ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
//					}
//					showCorrectAnswer();
//				}
//			}
//		} else {
		nmbTry = 1;
		startGame();
//		}

		bNextQuestion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				hasCorrectlyAnswered = false;
				nmbTry = 1;
				startGame();
			}
		});

		ibLevels.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideInfo) {
					clLevels.setVisibility(View.VISIBLE);
					hideInfo = false;
				} else {
					clLevels.setVisibility(View.INVISIBLE);
					hideInfo = true;
				}
			}
		});

		ivInfoCIty.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideInfo) {
					clInfoCity.setVisibility(View.VISIBLE);
					hideInfo = false;
				} else {
					clInfoCity.setVisibility(View.INVISIBLE);
					hideInfo = true;
				}
			}
		});

		if (geoLevel < 4) {
			parisLevel.setEnabled(true);
			regionLevel.setEnabled(false);
			deptLevel.setEnabled(false);
		}
		if (geoLevel > 3 && geoLevel < 7) {
			parisLevel.setEnabled(true);
			regionLevel.setEnabled(true);
			deptLevel.setEnabled(false);
		}
		if (geoLevel > 7) {
			parisLevel.setEnabled(true);
			regionLevel.setEnabled(true);
			deptLevel.setEnabled(true);
		}

		parisLevel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectedLevel = 0;
				bNextQuestion.performClick();
			}
		});

		regionLevel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectedLevel = 1;
				bNextQuestion.performClick();
			}
		});

		deptLevel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				selectedLevel = 2;
				bNextQuestion.performClick();
			}
		});

		return view;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

//	@Override
//	public void onViewStateRestored(Bundle savedInstanceState) {
//		super.onViewStateRestored(savedInstanceState);
//
//		if (savedInstanceState != null) {
//			//Restore the fragment's state here
//			geoPoints = savedInstanceState.getInt("geoPoints", 0);
//			maxScore = savedInstanceState.getInt("geoMaxPoints", 200);
//			isPaused = savedInstanceState.getBoolean("gamePause", false);
//			winTime = savedInstanceState.getInt("pauseGeoGame", 30);
//			rand = savedInstanceState.getInt("geoRand", new Random().nextInt(4));
//			nmbTry = savedInstanceState.getInt("try", 1);
//			randForSelectType = savedInstanceState.getInt("randForSelectType", 0);
//			try {
////					cityTable = (ArrayList<Paris>) ObjectSerializer.deserialize(savedInstanceState.getString("parisList", ObjectSerializer.serialize(new ArrayList<Paris>())));
//				uniqueRand = (ArrayList<Integer>) ObjectSerializer.deserialize(savedInstanceState.getString("randList", ObjectSerializer.serialize(new ArrayList<Integer>())));
//			} catch (ClassNotFoundException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}

//			if (!isPaused) {
//				counter = new MyCount(30000, 1000);
//				startGame();
//			} else {
//				if(counter == null) counter = new MyCount(30000, 1000);
//				counter.onTick(savedInstanceState.getLong("pauseGeoGame", 30000));
////			}
//		}
//	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

//	@Override
//	public void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		outState.putBoolean("gamePause", isPaused);
//		outState.putInt("geoPoints", geoPoints);
//		outState.putInt("geoLevel", geoLevel);
//		outState.putInt("pauseGeoGame", winTime);
//		outState.putInt("geoMaxPoints", maxScore);
//		outState.putInt("geoRand", rand);
//		outState.putInt("try", nmbTry);
//		outState.putInt("randForSelectType", randForSelectType);
//		try {
//			outState.putString("randList", ObjectSerializer.serialize(uniqueRand));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		//Save the fragment's state here
//	}

	@Override
	public void onResume() {
		super.onResume();
		startTime = System.currentTimeMillis();
		clInfoCity.setVisibility(View.INVISIBLE);
		geoPoints = sharedPreferences.getInt("geoPoints", 0);
		geoLevel = sharedPreferences.getInt("geoLevel", 1);
		maxScore = sharedPreferences.getInt("geoMaxPoints", 50);
		hasCorrectlyAnswered = sharedPreferences.getBoolean("hasCorrectlyAnswered", false);
		if (!isPaused) {
			counter.cancel();
			counter = new MyCount(30000, 1000);
			hasCorrectlyAnswered = false;
			nmbTry = 1;
			startGame();
		}

		tvPointsGeo.setText(geoPoints + "/" + maxScore);
		tvCurrentLevel.setText("Rang: " + geoLevel);
		if (geoPoints == 0)
			ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
		else if (geoPoints > 0)
			ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
		else
			ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
	}

	@Override
	public void onPause() {
		super.onPause();
		isPaused = true;
		sharedPreferences.edit().putInt("geoRand", rand).apply();
		sharedPreferences.edit().putInt("geoPoints", geoPoints).apply();
		sharedPreferences.edit().putInt("geoLevel", geoLevel).apply();
		sharedPreferences.edit().putLong("pauseGeoGame", winTime).apply();
		sharedPreferences.edit().putInt("geoMaxPoints", maxScore).apply();
		sharedPreferences.edit().putBoolean("hasCorrectlyAnswered", hasCorrectlyAnswered).apply();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		view = null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (counter != null) counter.cancel();
		sharedPreferences.edit().putInt("geoPoints", geoPoints).apply();
		endTime = System.currentTimeMillis();
		long timeSpent = endTime - startTime;
		sharedPreferencesSettings.edit().putLong("timeSpentGeo", sharedPreferencesSettings.getLong("timeSpentGeo",0)+timeSpent).apply();
	}

	private void readCVSFromAssetFolder(String csvFile) {
		String[] content = null;
		try {
			InputStream inputStream = getActivity().getAssets().open(csvFile);
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			Log.d("Test", " " + br.readLine());
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				content = line.split(",");
				Log.d("Content", "ligne " + i++ + " : " + content[0] + " // " + content[1] + " // " + content[2] + " // " + content[3] + " // " + content[4] + " // " + content[5] + " // " + content[6] + " // " + content[7] + " // " + content[8] + " // " + content[9] + " // " + content[10] + " // " + content[11] + " // " + content[12] + " // " + content[13] + " // ");
				if (csvFile.equals("paris.csv")) {
					csvParis.add(new Paris(content[0].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[1].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[2].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[3].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[4].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[5].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[6].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[7].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[8].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[9].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[10].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[11].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[12].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[13].replaceAll("^\"|\"$", "").replaceAll("null", "")));
				} else if (csvFile.equals("french_reg_bis.csv")) {
					csvReg.add(new Regions(content[0].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[1].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[2].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[3].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[4].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[5].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[6].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[7].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[8].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[9].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[10].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[11].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[12].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[13].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[0].replaceAll("^\"|\"$", "").replaceAll("null", "")));
				} else if (csvFile.equals("french_dep_bis.csv")) {
					csvDep.add(new Departements(content[0].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[1].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[2].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[3].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[4].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[5].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[6].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[7].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[8].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[9].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[10].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[11].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[12].replaceAll("^\"|\"$", "").replaceAll("_", ",").replaceAll("null", ""), content[13].replaceAll("^\"|\"$", "").replaceAll("null", ""), content[14].replaceAll("^\"|\"$", "").replaceAll("null", "")));
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void printCVSContent() {
		String display1 = "", display2 = "", display3 = "", display4 = "";
		csvParisRand = new ArrayList<>();
		csvRegRand = new ArrayList<>();
		csvDepRand = new ArrayList<>();
		ivListenTTSMonuments.setVisibility(View.VISIBLE);
		ivListenTTSSummarize.setVisibility(View.VISIBLE);
		if (!fromCreateView) rand = new Random().nextInt(4);
		if (selectedLevel == 0 || geoLevel < 4) {
			if (!fromCreateView) uniqueRand = Utils.uniqueRand(0, 20);
			for (int i = 0; i < 4; i++) {
				csvParisRand.add(new Paris(csvParis.get(uniqueRand.get(i)).getNum_arrondissement(), csvParis.get(uniqueRand.get(i)).getRom_num_arrondissement(), csvParis.get(uniqueRand.get(i)).getName_arrondissement(), csvParis.get(uniqueRand.get(i)).getCp_arrondissement(), csvParis.get(uniqueRand.get(i)).getDensite_pop(), csvParis.get(uniqueRand.get(i)).getSuperficie(), csvParis.get(uniqueRand.get(i)).getPopulation(), csvParis.get(uniqueRand.get(i)).getQuartiers(), csvParis.get(uniqueRand.get(i)).getMonuments(), csvParis.get(uniqueRand.get(i)).getSummarize(), csvParis.get(uniqueRand.get(i)).getImage(), csvParis.get(uniqueRand.get(i)).getImage_description(), csvParis.get(uniqueRand.get(i)).getCopyright_text(), csvParis.get(uniqueRand.get(i)).getCopyright_url()));
			}
			display1 = csvParisRand.get(0).getName_arrondissement();
			display2 = csvParisRand.get(1).getName_arrondissement();
			display3 = csvParisRand.get(2).getName_arrondissement();
			display4 = csvParisRand.get(3).getName_arrondissement();
			setImagesFromAssets("paris/arrondissement_map_" + csvParisRand.get(rand).getRom_num_arrondissement(), ivMapParis);
			setImagesFromAssets("paris/arrondissement_view_" + csvParisRand.get(rand).getRom_num_arrondissement(), ivParisPhoto);
			Utils.readTheWord(ivListenTTSName, csvParisRand.get(rand).getName_arrondissement(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSQuartier, csvParisRand.get(rand).getQuartiers(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSMonuments, csvParisRand.get(rand).getMonuments(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSSummarize, csvParisRand.get(rand).getSummarize(), getActivity().getApplicationContext());
			if (csvParisRand.get(rand).getMonuments().equals(""))
				ivListenTTSMonuments.setVisibility(View.INVISIBLE);
			if (csvParisRand.get(rand).getSummarize().equals(""))
				ivListenTTSSummarize.setVisibility(View.INVISIBLE);
			arrondissement = "ème arrondissement: </b>";
			if (csvParisRand.get(rand).getNum_arrondissement().equals("1"))
				arrondissement = "er arrondissement: </b>";
			tvNumArr.setText(Html.fromHtml("<b> " + csvParisRand.get(rand).getNum_arrondissement() + arrondissement));
			tvNameArrondissement.setText(csvParisRand.get(rand).getName_arrondissement());
			tvPopArr.setText(Html.fromHtml("<b> Population: </b> " + csvParisRand.get(rand).getPopulation()));
			tvSupArr.setText(Html.fromHtml("<b> Superficie: </b>" + csvParisRand.get(rand).getSuperficie() + "km²"));
			tvDensArr.setText(Html.fromHtml("<b> Habitants/km²: </b>" + csvParisRand.get(rand).getDensite_pop()));
			tvQuartiers.setText(Html.fromHtml("<b> Quartiers: </b><br>" + csvParisRand.get(rand).getQuartiers()));
			tvMonuments.setText(Html.fromHtml("<b> Monuments: </b><br>" + csvParisRand.get(rand).getMonuments()));
			tvShortDescription.setText(csvParisRand.get(rand).getSummarize());
			tvCopyrights.setText(csvParisRand.get(rand).getCopyright_text());
			tvImgDescription.setText(csvParisRand.get(rand).getImage_description());
			tvCopyrights.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!csvParisRand.get(rand).getCopyright_url().equals("null")) {
						Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse(csvParisRand.get(rand).getCopyright_url()));
						startActivity(i);
					}
				}
			});
		} else if (selectedLevel == 1 || geoLevel > 3 && geoLevel < 6) {
			if (!fromCreateView) randForSelectType = new Random().nextInt(2);
			if (randForSelectType == 0) {
				if (!fromCreateView) uniqueRand = Utils.uniqueRand(0, 18);
				for (int i = 0; i < 4; i++) {
					csvRegRand.add(new Regions(csvReg.get(uniqueRand.get(i)).getType(), csvReg.get(uniqueRand.get(i)).getCirco(), csvReg.get(uniqueRand.get(i)).getName(), csvReg.get(uniqueRand.get(i)).getPop(), csvReg.get(uniqueRand.get(i)).getMain_city(), csvReg.get(uniqueRand.get(i)).getSuperficie(), csvReg.get(uniqueRand.get(i)).getDensite(), csvReg.get(uniqueRand.get(i)).getDate(), csvReg.get(uniqueRand.get(i)).getNumero_rom(), csvReg.get(uniqueRand.get(i)).getMonuments(), csvReg.get(uniqueRand.get(i)).getSummarize(), csvReg.get(uniqueRand.get(i)).getImages(), csvReg.get(uniqueRand.get(i)).getImages_expl(), csvReg.get(uniqueRand.get(i)).getCopyright_text(), csvReg.get(uniqueRand.get(i)).getCopyright_url()));
				}
				setImagesFromAssets("reg_map/" + csvRegRand.get(rand).getName(), ivMapParis);
				setImagesFromAssets("reg_heraldic/" + csvRegRand.get(rand).getName(), ivParisPhoto);
				tvNumArr.setText(Html.fromHtml("<b>Région: </b>"));
				tvQuartiers.setText(Html.fromHtml("<b>Chef-lieu de région: </b>" + csvRegRand.get(rand).getMain_city()));
				tvMonuments.setText("");
			} else {
				if (!fromCreateView) uniqueRand = Utils.uniqueRand(18, 38);
				for (int i = 0; i < 4; i++) {
					csvRegRand.add(new Regions(csvReg.get(uniqueRand.get(i)).getType(), csvReg.get(uniqueRand.get(i)).getCirco(), csvReg.get(uniqueRand.get(i)).getName(), csvReg.get(uniqueRand.get(i)).getPop(), csvReg.get(uniqueRand.get(i)).getMain_city(), csvReg.get(uniqueRand.get(i)).getSuperficie(), csvReg.get(uniqueRand.get(i)).getDensite(), csvReg.get(uniqueRand.get(i)).getDate(), csvReg.get(uniqueRand.get(i)).getNumero_rom(), csvReg.get(uniqueRand.get(i)).getMonuments(), csvReg.get(uniqueRand.get(i)).getSummarize(), csvReg.get(uniqueRand.get(i)).getImages(), csvReg.get(uniqueRand.get(i)).getImages_expl(), csvReg.get(uniqueRand.get(i)).getCopyright_text(), csvReg.get(uniqueRand.get(i)).getCopyright_url()));
				}
				setImagesFromAssets("paris/arrondissement_map_" + csvRegRand.get(rand).getNumero_rom(), ivMapParis);
				setImagesFromAssets("paris/arrondissement_view_" + csvRegRand.get(rand).getNumero_rom(), ivParisPhoto);
				arrondissement = "ème arrondissement: </b>";
				if (csvRegRand.get(rand).getCirco().equals("1"))
					arrondissement = "er arrondissement: </b>";
				tvNumArr.setText(Html.fromHtml("<b> " + csvRegRand.get(rand).getCirco() + arrondissement));
				tvQuartiers.setText(Html.fromHtml("<b>Quartiers: </b><br>" + csvRegRand.get(rand).getMain_city()));
				tvMonuments.setText(Html.fromHtml("<b>Monuments: </b><br>" + csvRegRand.get(rand).getMonuments()));
			}
			Utils.readTheWord(ivListenTTSName, csvRegRand.get(rand).getName(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSQuartier, csvRegRand.get(rand).getMain_city(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSMonuments, csvRegRand.get(rand).getMonuments(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSSummarize, csvRegRand.get(rand).getSummarize(), getActivity().getApplicationContext());
			if (csvRegRand.get(rand).getMonuments().equals(""))
				ivListenTTSMonuments.setVisibility(View.INVISIBLE);
			if (csvRegRand.get(rand).getSummarize().equals(""))
				ivListenTTSSummarize.setVisibility(View.INVISIBLE);
			tvNameArrondissement.setText(csvRegRand.get(rand).getName());
			tvPopArr.setText(Html.fromHtml("<b>Population: </b> " + csvRegRand.get(rand).getPop()));
			tvSupArr.setText(Html.fromHtml("<b>Superficie: </b>" + csvRegRand.get(rand).getSuperficie() + "km²"));
			tvDensArr.setText(Html.fromHtml("<b>Habitants/km²: </b>" + csvRegRand.get(rand).getDensite()));
			tvShortDescription.setText(csvRegRand.get(rand).getSummarize());
			tvCopyrights.setText(csvRegRand.get(rand).getCopyright_text());
			tvImgDescription.setText(csvRegRand.get(rand).getImages_expl());
			display1 = csvRegRand.get(0).getName();
			display2 = csvRegRand.get(1).getName();
			display3 = csvRegRand.get(2).getName();
			display4 = csvRegRand.get(3).getName();
			tvCopyrights.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!csvRegRand.get(rand).getCopyright_url().equals("null")) {
						Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse(csvRegRand.get(rand).getCopyright_url()));
						startActivity(i);
					}
				}
			});
		} else {
			if (!fromCreateView) randForSelectType = new Random().nextInt(3);
			if (randForSelectType == 0) {
				if (!fromCreateView) uniqueRand = Utils.uniqueRand(0, 100);
				for (int i = 0; i < 4; i++) {
					csvDepRand.add(new Departements(csvDep.get(uniqueRand.get(i)).getType(), csvDep.get(uniqueRand.get(i)).getName_reg(), csvDep.get(uniqueRand.get(i)).getName_dep(), csvDep.get(uniqueRand.get(i)).getPopulation(), csvDep.get(uniqueRand.get(i)).getMain_dep(), csvDep.get(uniqueRand.get(i)).getSuperficie(), csvDep.get(uniqueRand.get(i)).getDensite(), csvDep.get(uniqueRand.get(i)).getDate_creation(), csvDep.get(uniqueRand.get(i)).getNum_dep(), csvDep.get(uniqueRand.get(i)).getMain_reg(), csvDep.get(uniqueRand.get(i)).getSummarize(), csvDep.get(uniqueRand.get(i)).getImages(), csvDep.get(uniqueRand.get(i)).getImages_expl(), csvDep.get(uniqueRand.get(i)).getCopyright_txt(), csvDep.get(uniqueRand.get(i)).getCopyright_url()));
				}
				setImagesFromAssets("dep_map/map_departement_" + csvDepRand.get(rand).getNum_dep(), ivMapParis);
				tvNumArr.setText(Html.fromHtml("<b>Département: </b>"));
				tvQuartiers.setText(Html.fromHtml("<b>Préfecture: </b>" + csvDepRand.get(rand).getMain_dep()));
				tvMonuments.setText(Html.fromHtml("<b>Région: </b>" + csvDepRand.get(rand).getName_reg() + "   <b>Chef-lieu de région: </b>" + csvDepRand.get(rand).getMain_reg()));
			} else if (randForSelectType == 1) {
				if (!fromCreateView) uniqueRand = Utils.uniqueRand(101, 118);
				for (int i = 0; i < 4; i++) {
					csvDepRand.add(new Departements(csvDep.get(uniqueRand.get(i)).getType(), csvDep.get(uniqueRand.get(i)).getName_reg(), csvDep.get(uniqueRand.get(i)).getName_dep(), csvDep.get(uniqueRand.get(i)).getPopulation(), csvDep.get(uniqueRand.get(i)).getMain_dep(), csvDep.get(uniqueRand.get(i)).getSuperficie(), csvDep.get(uniqueRand.get(i)).getDensite(), csvDep.get(uniqueRand.get(i)).getDate_creation(), csvDep.get(uniqueRand.get(i)).getNum_dep(), csvDep.get(uniqueRand.get(i)).getMain_reg(), csvDep.get(uniqueRand.get(i)).getSummarize(), csvDep.get(uniqueRand.get(i)).getImages(), csvDep.get(uniqueRand.get(i)).getImages_expl(), csvDep.get(uniqueRand.get(i)).getCopyright_txt(), csvDep.get(uniqueRand.get(i)).getCopyright_url()));
				}
				setImagesFromAssets("reg_map/" + csvDepRand.get(rand).getName_dep(), ivMapParis);
				tvNumArr.setText(Html.fromHtml("<b>Région (" + csvDepRand.get(rand).getName_reg() + "):</b> "));
				tvQuartiers.setText(Html.fromHtml("<b>Chef-lieu de région: </b><br>" + csvDepRand.get(rand).getMain_dep()));
				tvMonuments.setText("");
			} else if (randForSelectType == 2) {
				if (!fromCreateView) uniqueRand = Utils.uniqueRand(119, 138);
				for (int i = 0; i < 4; i++) {
					csvDepRand.add(new Departements(csvDep.get(uniqueRand.get(i)).getType(), csvDep.get(uniqueRand.get(i)).getName_reg(), csvDep.get(uniqueRand.get(i)).getName_dep(), csvDep.get(uniqueRand.get(i)).getPopulation(), csvDep.get(uniqueRand.get(i)).getMain_dep(), csvDep.get(uniqueRand.get(i)).getSuperficie(), csvDep.get(uniqueRand.get(i)).getDensite(), csvDep.get(uniqueRand.get(i)).getDate_creation(), csvDep.get(uniqueRand.get(i)).getNum_dep(), csvDep.get(uniqueRand.get(i)).getMain_reg(), csvDep.get(uniqueRand.get(i)).getSummarize(), csvDep.get(uniqueRand.get(i)).getImages(), csvDep.get(uniqueRand.get(i)).getImages_expl(), csvDep.get(uniqueRand.get(i)).getCopyright_txt(), csvDep.get(uniqueRand.get(i)).getCopyright_url()));
				}
				setImagesFromAssets("paris/arrondissement_map_" + csvDepRand.get(rand).getNum_dep(), ivMapParis);
				setImagesFromAssets("paris/arrondissement_view_" + csvDepRand.get(rand).getNum_dep(), ivParisPhoto);
				tvNumArr.setText(Html.fromHtml("<b>Région (" + csvDepRand.get(rand).getName_reg() + "):</b> "));
				tvQuartiers.setText(Html.fromHtml("<b>Quartiers: </b><br>" + csvDepRand.get(rand).getMain_dep()));
				tvMonuments.setText(Html.fromHtml("<b>Monuments: </b><br>" + csvDepRand.get(rand).getMain_reg()));
			}
			String numDepartement = "";
			if (csvDepRand.get(rand).getNum_dep() != null)
				numDepartement = " (" + csvDepRand.get(rand).getNum_dep() + ")";
			tvNameArrondissement.setText(csvDepRand.get(rand).getName_dep() + numDepartement);
			tvPopArr.setText(Html.fromHtml("<b>Population: </b> " + csvDepRand.get(rand).getPopulation()));
			tvSupArr.setText(Html.fromHtml("<b>Superficie: </b>" + csvDepRand.get(rand).getSuperficie() + "km²"));
			tvDensArr.setText(Html.fromHtml("<b>Habitants/km²: </b>" + csvDepRand.get(rand).getDensite()));
			tvShortDescription.setText(csvDepRand.get(rand).getSummarize());
			tvCopyrights.setText(csvDepRand.get(rand).getCopyright_txt());
			tvImgDescription.setText(csvDepRand.get(rand).getImages_expl());
			display1 = csvDepRand.get(0).getName_dep();
			display2 = csvDepRand.get(1).getName_dep();
			display3 = csvDepRand.get(2).getName_dep();
			display4 = csvDepRand.get(3).getName_dep();
			tvCopyrights.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!csvDepRand.get(rand).getCopyright_url().equals("null")) {
						Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse(csvDepRand.get(rand).getCopyright_url()));
						startActivity(i);
					}
				}
			});
			Utils.readTheWord(ivListenTTSName, csvDepRand.get(rand).getName_dep(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSQuartier, csvDepRand.get(rand).getMain_dep(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSMonuments, csvDepRand.get(rand).getMain_reg(), getActivity().getApplicationContext());
			Utils.readTheWord(ivListenTTSSummarize, csvDepRand.get(rand).getSummarize(), getActivity().getApplicationContext());
			if (csvDepRand.get(rand).getMain_reg().equals(""))
				ivListenTTSMonuments.setVisibility(View.INVISIBLE);
			if (csvDepRand.get(rand).getSummarize().equals(""))
				ivListenTTSSummarize.setVisibility(View.INVISIBLE);

		}

		tvGameWhere1.setText(display1);
		tvGameWhere1.setTag("0");
		tvGameWhere1.setEnabled(true);
		tvGameWhere2.setText(display2);
		tvGameWhere2.setTag("1");
		tvGameWhere2.setEnabled(true);
		tvGameWhere3.setText(display3);
		tvGameWhere3.setTag("2");
		tvGameWhere3.setEnabled(true);
		tvGameWhere4.setText(display4);
		tvGameWhere4.setTag("3");
		tvGameWhere4.setEnabled(true);

		geoParisGame(rand);
	}

	private boolean geoParisGame(final int correctAnswerID) {

		View.OnClickListener listener = new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				resetButton();
				if (v.getTag().equals("" + correctAnswerID)) {
					winner();
					v.setBackgroundColor(getResources().getColor(R.color.green_correct));
					v.animate().setDuration(1000).rotation(360).start();
				} else {
					v.setBackgroundColor(getResources().getColor(R.color.red_error));
					looser();
				}
				tvPointsGeo.setText(geoPoints + "/" + maxScore);
				if (geoPoints == 0)
					ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
			}
		};

		tvGameWhere1.setOnClickListener(listener);
		tvGameWhere2.setOnClickListener(listener);
		tvGameWhere3.setOnClickListener(listener);
		tvGameWhere4.setOnClickListener(listener);

		return answer;
	}

	private void startGame() {
		if (counter != null) counter.cancel();
		counter = new MyCount(30000, 1000);

		ivInfoCIty.setVisibility(View.INVISIBLE);
		clInfoCity.setVisibility(View.INVISIBLE);
		bNextQuestion.setVisibility(View.INVISIBLE);
		resetButton();
		tvTimer.setTextColor(getResources().getColor(R.color.red_error));
		if (selectedLevel == 0 || geoLevel < 4) {
			readCVSFromAssetFolder(geoLevels[0]);
			printCVSContent();
		} else if (selectedLevel == 1 || geoLevel > 3 && geoLevel < 6) {
			readCVSFromAssetFolder(geoLevels[1]);
			printCVSContent();
		} else if (selectedLevel == 2 || geoLevel > 5) {
			readCVSFromAssetFolder(geoLevels[2]);
			printCVSContent();
		}
		if (!hasCorrectlyAnswered) counter.start();
	}

	private void winner() {
		hasCorrectlyAnswered = true;
		setWinnerPoints();
		counter.onFinish();
		tvTimer.setTextColor(getResources().getColor(R.color.green_correct));
		showCorrectAnswer();
		ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
	}

	private void looser() {
		nmbTry++;
		hasCorrectlyAnswered = false;
		setLoserPoints(5);
		if (nmbTry == 3) {
			counter.onTick(1500);
			counter.onFinish();
			counter.cancel();
			tvTimer.setTextColor(getResources().getColor(R.color.red_error));
			tvTimer.setText("La prochaine, c'est la bonne ! Courage");
			showCorrectAnswer();
		}
		ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
	}

	private void showCorrectAnswer() {
		if (tvGameWhere1.getTag().equals("" + rand))
			tvGameWhere1.setBackgroundColor(getResources().getColor(R.color.green_correct));
		if (tvGameWhere2.getTag().equals("" + rand))
			tvGameWhere2.setBackgroundColor(getResources().getColor(R.color.green_correct));
		if (tvGameWhere3.getTag().equals("" + rand))
			tvGameWhere3.setBackgroundColor(getResources().getColor(R.color.green_correct));
		if (tvGameWhere4.getTag().equals("" + rand))
			tvGameWhere4.setBackgroundColor(getResources().getColor(R.color.green_correct));
		tvGameWhere1.setEnabled(false);
		tvGameWhere2.setEnabled(false);
		tvGameWhere3.setEnabled(false);
		tvGameWhere4.setEnabled(false);
		bNextQuestion.setVisibility(View.VISIBLE);
		ivInfoCIty.setVisibility(View.VISIBLE);
		if (geoPoints > maxScore) {
			geoLevel += 1;
			tvCurrentLevel.setText("Rang: " + geoLevel);
			if (geoLevel == 4) {
				regionLevel.setEnabled(true);
				contentUnlocked(Gravity.TOP, R.layout.toast_unlocked_lvl, R.id.relativeLayout1, 0, "");
			} else if (geoLevel == 6) {
				deptLevel.setEnabled(true);
				contentUnlocked(Gravity.TOP, R.layout.toast_unlocked_lvl, R.id.relativeLayout1, 0, "");
			}
			Toast.makeText(getActivity(), "Rang Suivant", Toast.LENGTH_LONG).show();
			maxScore *= 2;
		}
	}

	private void setWinnerPoints() {
		int finalPoints = 30 - winTime;
		String points = "";
		if (winTime >= 25) {
			points = "+10";
			geoPoints += 10;
		} else if (winTime < 25 && winTime >= 20) {
			geoPoints += 5;
			points = "+5";
		} else if (winTime < 20 && winTime >= 15) {
			geoPoints += 4;
			points = "+4";
		} else if (winTime < 15 && winTime >= 10) {
			geoPoints += 3;
			points = "+3";
		} else if (winTime < 10 && winTime >= 5) {
			geoPoints += 2;
			points = "+2";
		} else if (winTime < 5 && winTime > 1) {
			geoPoints += 1;
			points = "+1";
		}
		contentUnlocked(Gravity.RIGHT | Gravity.TOP, R.layout.toast_points, R.id.relativeLayout2, R.color.green_correct, points);
		tvTimer.setText("Tu as gagné en " + finalPoints + "s'");
	}

	private void setLoserPoints(int pointsToLose) {
		if (geoPoints >= 5) {
			contentUnlocked(Gravity.RIGHT | Gravity.TOP, R.layout.toast_points, R.id.relativeLayout2, R.color.red_error, "-"+pointsToLose);
			geoPoints -= pointsToLose;
		} else geoPoints = 0;
	}

	private void resetButton() {
		tvGameWhere1.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere2.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere3.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere4.setBackground(getResources().getDrawable(R.drawable.selector_effect));
	}

	private void setImagesFromAssets(final String spotName, ImageView ivImage) {
		InputStream is = null;
		try {
			is = getActivity().getAssets().open(spotName + ".png");
			Bitmap bitmap = BitmapFactory.decodeStream(is);
			ivImage.setImageBitmap(bitmap);
		} catch (IOException e) {
			ivImage.setBackgroundColor(Color.TRANSPARENT);
			e.printStackTrace();
		}
	}

	private void contentUnlocked(int gravity, int layoutToast, int relativeLayout, int colorTxt, String text) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		TextView tvPointsPlusOrMinus;
		ImageView ivPointsPlusOrMinus;
		View view = inflater.inflate(layoutToast, (ViewGroup) getActivity().findViewById(relativeLayout));
		if (layoutToast == R.layout.toast_points) {
			tvPointsPlusOrMinus = view.findViewById(R.id.tvPointsPlusOrMinus);
			ivPointsPlusOrMinus = view.findViewById(R.id.ivPointsPlusOrMinus);
			tvPointsPlusOrMinus.setTextColor(getResources().getColor(colorTxt));
			tvPointsPlusOrMinus.setText(text);
			switch (colorTxt) {
				case R.color.red_error:
					ivPointsPlusOrMinus.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
					break;
				case R.color.green_correct:
					ivPointsPlusOrMinus.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
					break;
			}
		}

		if(toast != null) toast.cancel();
		toast = new Toast(getActivity().getApplicationContext());
		toast.setView(view);
		toast.setGravity(gravity, 0, 100);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.show();
	}


	public class MyCount extends CountDownTimer {

		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			if (!hasCorrectlyAnswered && getActivity() != null || nmbTry == 3) {
				setLoserPoints(10);
				tvPointsGeo.setText(geoPoints + "/" + maxScore);
				if (geoPoints == 0)
					ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
				ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
				tvTimer.setText("La prochaine, c'est la bonne ! Courage");
				if (winTime == 1) showCorrectAnswer();
			}
			if (hasCorrectlyAnswered && getActivity() != null) {
				tvPointsGeo.setText(geoPoints + "/" + maxScore);
				ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
			}
		}

		@Override
		public void onTick(long millisUntilFinished) {
			if (!hasCorrectlyAnswered && nmbTry < 3) {
				tvTimer.setText("Temps restant: " + millisUntilFinished / 1000);
				winTime = (int) (millisUntilFinished / 1000);
			}
		}
	}
}
