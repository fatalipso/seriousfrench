package ovh.fatalispo.seriousfrench.game.geoGame;

public class Regions {
	String type, circo, name, pop, main_city, superficie, densite, date, numero_rom, monuments, summarize, images, images_expl, copyright_text, copyright_url;

	public Regions(String type, String circo, String name, String pop, String main_city, String superficie, String densite, String date, String numero_rom, String monuments, String summarize, String images, String images_expl, String copyright_text, String copyright_url) {
		this.type = type;
		this.circo = circo;
		this.name = name;
		this.pop = pop;
		this.main_city = main_city;
		this.superficie = superficie;
		this.densite = densite;
		this.date = date;
		this.numero_rom = numero_rom;
		this.monuments = monuments;
		this.summarize = summarize;
		this.images = images;
		this.images_expl = images_expl;
		this.copyright_text = copyright_text;
		this.copyright_url = copyright_url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCirco() {
		return circo;
	}

	public void setCirco(String circo) {
		this.circo = circo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPop() {
		return pop;
	}

	public void setPop(String pop) {
		this.pop = pop;
	}

	public String getMain_city() {
		return main_city;
	}

	public void setMain_city(String main_city) {
		this.main_city = main_city;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	public String getDensite() {
		return densite;
	}

	public void setDensite(String densite) {
		this.densite = densite;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNumero_rom() {
		return numero_rom;
	}

	public void setNumero_rom(String numero_rom) {
		this.numero_rom = numero_rom;
	}

	public String getMonuments() {
		return monuments;
	}

	public void setMonuments(String monuments) {
		this.monuments = monuments;
	}

	public String getSummarize() {
		return summarize;
	}

	public void setSummarize(String summarize) {
		this.summarize = summarize;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getImages_expl() {
		return images_expl;
	}

	public void setImages_expl(String images_expl) {
		this.images_expl = images_expl;
	}

	public String getCopyright_text() {
		return copyright_text;
	}

	public void setCopyright_text(String copyright_text) {
		this.copyright_text = copyright_text;
	}

	public String getCopyright_url() {
		return copyright_url;
	}

	public void setCopyright_url(String copyright_url) {
		this.copyright_url = copyright_url;
	}
}
