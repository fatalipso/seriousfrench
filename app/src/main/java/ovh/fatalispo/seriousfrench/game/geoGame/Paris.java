package ovh.fatalispo.seriousfrench.game.geoGame;

public class Paris {
	String num_arrondissement, rom_num_arrondissement, name_arrondissement, cp_arrondissement, densite_pop, superficie, population, quartiers, monuments, religious_spot, summarize, image, image_description, copyright_text, copyright_url;

	public Paris() {};

	public Paris(String num_arrondissement, String rom_num_arrondissement, String name_arrondissement, String cp_arrondissement, String densite_pop, String superficie, String population, String quartiers, String monuments, String summarize, String image, String image_description, String copyright_text, String copyright_url) {
		this.num_arrondissement = num_arrondissement;
		this.rom_num_arrondissement = rom_num_arrondissement;
		this.name_arrondissement = name_arrondissement;
		this.cp_arrondissement = cp_arrondissement;
		this.densite_pop = densite_pop;
		this.superficie = superficie;
		this.population = population;
		this.quartiers = quartiers;
		this.monuments = monuments;
		this.summarize = summarize;
		this.image = image;
		this.image_description = image_description;
		this.copyright_text = copyright_text;
		this.copyright_url = copyright_url;
	}

	public String getNum_arrondissement() {
		return num_arrondissement;
	}

	public void setNum_arrondissement(String num_arrondissement) {
		this.num_arrondissement = num_arrondissement;
	}

	public String getRom_num_arrondissement() {
		return rom_num_arrondissement;
	}

	public void setRom_num_arrondissement(String rom_num_arrondissement) {
		this.rom_num_arrondissement = rom_num_arrondissement;
	}

	public String getName_arrondissement() {
		return name_arrondissement;
	}

	public void setName_arrondissement(String name_arrondissement) {
		this.name_arrondissement = name_arrondissement;
	}

	public String getCp_arrondissement() {
		return cp_arrondissement;
	}

	public void setCp_arrondissement(String cp_arrondissement) {
		this.cp_arrondissement = cp_arrondissement;
	}

	public String getDensite_pop() {
		return densite_pop;
	}

	public void setDensite_pop(String densite_pop) {
		this.densite_pop = densite_pop;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getQuartiers() {
		return quartiers;
	}

	public void setQuartiers(String quartiers) {
		this.quartiers = quartiers;
	}

	public String getMonuments() {
		return monuments;
	}

	public void setMonuments(String monuments) {
		this.monuments = monuments;
	}

	public String getSummarize() {
		return summarize;
	}

	public void setSummarize(String summarize) {
		this.summarize = summarize;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage_description() {
		return image_description;
	}

	public void setImage_description(String image_description) {
		this.image_description = image_description;
	}

	public String getCopyright_text() {
		return copyright_text;
	}

	public void setCopyright_text(String copyright_text) {
		this.copyright_text = copyright_text;
	}

	public String getCopyright_url() {
		return copyright_url;
	}

	public void setCopyright_url(String copyright_url) {
		this.copyright_url = copyright_url;
	}
}
