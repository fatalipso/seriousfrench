package ovh.fatalispo.seriousfrench.game.geoGame;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.app.Fragment;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.osmdroid.tileprovider.tilesource.TileSourcePolicy;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import ovh.fatalispo.seriousfrench.MapActivity;
import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.Utils;

public class CitiesFragment extends Fragment {

	private SharedPreferences sharedPreferences;
	protected ArrayList<City> cityTable;
	private TextView tvNameCity, tvPopulation, tvRegion, tvDepartment, tvPointsGeo;
	private Button tvGameWhere1, tvGameWhere2, tvGameWhere3, tvGameWhere4, bNextQuestion;
	private ImageButton backButtonGeoGame, ivInfoCIty, ivListenTTS;
	protected ImageView ivRegHeraldic, ivPointsGeo;
	protected ArrayList<OverlayItem> items;
	protected double beginZoomLvl, maxZoomLvl;
	private int rand, geoPoints;
	private org.osmdroid.views.MapView mapGame;
	private boolean answer = true, hideInfo = true, isTalking = false;
	protected OverlayItem overlay;
	private ConstraintLayout clInfoCity;


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_cities,container,false);
		sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("games", 0);
		final ArrayList<City> csvContent = readCVSFromAssetFolder();
		items = new ArrayList<OverlayItem>();

		clInfoCity = view.findViewById(R.id.clInfoCity);

		tvGameWhere1 = view.findViewById(R.id.tvGameWhere1);
		tvGameWhere2 = view.findViewById(R.id.tvGameWhere2);
		tvGameWhere3 = view.findViewById(R.id.tvGameWhere3);
		tvGameWhere4 = view.findViewById(R.id.tvGameWhere4);
		tvNameCity = view.findViewById(R.id.tvNameCity);
		tvPopulation = view.findViewById(R.id.tvQuartiers);
		tvRegion = view.findViewById(R.id.tvMonuments);
		tvDepartment = view.findViewById(R.id.tvReligiousPlace);
		tvPointsGeo = view.findViewById(R.id.tvPointsGeo);
		bNextQuestion = view.findViewById(R.id.bNextQuestion);
		ivInfoCIty = view.findViewById(R.id.ivInfoCIty);
		ivRegHeraldic = view.findViewById(R.id.ivRegHeraldic);
		ivPointsGeo = view.findViewById(R.id.ivPointsGeo);
		mapGame = view.findViewById(R.id.mapGame);
		ivListenTTS = view.findViewById(R.id.ivListenTTSMonuments);

		switch (Utils.sizeScreen(getActivity().getApplicationContext())) {
			case "normal":
				beginZoomLvl = 4.7;
				maxZoomLvl = 4.7;
				break;
			case "large":
			case "xlarge":
				beginZoomLvl = 5.8;
				maxZoomLvl = 5.8;
				break;
		}
		printCVSContent(csvContent);
		bNextQuestion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ivInfoCIty.setVisibility(View.INVISIBLE);
				clInfoCity.setVisibility(View.INVISIBLE);
				bNextQuestion.setVisibility(View.INVISIBLE);
				resetButton();
				printCVSContent(csvContent);
			}
		});

		ivInfoCIty.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideInfo) {
					clInfoCity.setVisibility(View.VISIBLE);
					hideInfo = false;
				} else {
					clInfoCity.setVisibility(View.INVISIBLE);
					hideInfo = true;
				}
				tvNameCity.setText(cityTable.get(rand).getName());
				tvPopulation.setText("Population: " + cityTable.get(rand).getPopulation());
				tvRegion.setText("Région: " + cityTable.get(rand).getRegion());
				tvDepartment.setText("Numéro de département: " + cityTable.get(rand).getDepartment());
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		mapGame.onResume();
		clInfoCity.setVisibility(View.INVISIBLE);
		ivInfoCIty.setVisibility(View.INVISIBLE);
		bNextQuestion.setVisibility(View.INVISIBLE);
		geoPoints = sharedPreferences.getInt("geoPoints",0);
		tvPointsGeo.setText(""+geoPoints);
		if(geoPoints==0) ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
		else if(geoPoints>0) ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
		else ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
	}

	@Override
	public void onPause() {
		super.onPause();
		mapGame.onPause();
		sharedPreferences.edit().putInt("geoPoints",geoPoints).apply();
	}

	private ArrayList<City> readCVSFromAssetFolder() {
		ArrayList<City> csvLine = new ArrayList<>();
		String[] content = null;
		try {
			InputStream inputStream = getActivity().getAssets().open("french_cities.csv");
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			Log.d("Test", " " + br.readLine());
			String line = "";
			while ((line = br.readLine()) != null) {
				content = line.split(",");
				csvLine.add(new City(content[0], content[1], String.valueOf(Math.round(Double.parseDouble(content[7]))), content[3] + " " + content[4], content[5], content[6]));
//				createCSV((""+content[4].charAt(0)).toUpperCase()+content[4].substring(1).toLowerCase());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return csvLine;
	}

	private void printCVSContent(ArrayList<City> result) {
		cityTable = new ArrayList<>();

		int[] randTable = {new Random().nextInt(261), new Random().nextInt(261), new Random().nextInt(261), new Random().nextInt(261)};
		for(int j=0;j<randTable.length;j++) {
			for (int k = j + 1; k < randTable.length; k++) {
				if(randTable[j]==randTable[k]) {
					randTable[j] = new Random().nextInt(261);
				}
			}
		}
		//		tvGameWhere1.setText(""+result.size());
		for (int i = 0; i < 4; i++) {
			cityTable.add(new City(result.get(randTable[i]).getRegion(), result.get(randTable[i]).getDepartment(), result.get(randTable[i]).getPopulation(), result.get(randTable[i]).getName(), result.get(randTable[i]).getLatitude(), result.get(randTable[i]).getLongitude()));
		}

		tvGameWhere1.setText(cityTable.get(0).getName());
		tvGameWhere1.setTag("0");
		tvGameWhere1.setEnabled(true);
		tvGameWhere2.setText(cityTable.get(1).getName());
		tvGameWhere2.setTag("1");
		tvGameWhere2.setEnabled(true);
		tvGameWhere3.setText(cityTable.get(2).getName());
		tvGameWhere3.setTag("2");
		tvGameWhere3.setEnabled(true);
		tvGameWhere4.setText(cityTable.get(3).getName());
		tvGameWhere4.setTag("3");
		tvGameWhere4.setEnabled(true);


		rand = new Random().nextInt(4);
		mapDisplay(Double.parseDouble(cityTable.get(rand).getLongitude()), Double.parseDouble(cityTable.get(rand).getLatitude()));
		geoCitiesGame(rand);
	}

	private void mapDisplay(final double wallpaper_lat, final double wallpaper_lng) {
		MapActivity mapGActivity;
		overlay = new OverlayItem("?", "", new GeoPoint(wallpaper_lat, wallpaper_lng));
		items.add(overlay);
		if (items.size() > 1) items.remove(0);
		mapGActivity = new MapActivity(getActivity().getApplicationContext(), getActivity(), mapGame, wallpaper_lat, wallpaper_lng, items, beginZoomLvl, maxZoomLvl, new XYTileSource("osmfr", 5, 5, 256, ".png", new String[]{"http://a.tile.openstreetmap.fr/osmfr/", "http://b.tile.openstreetmap.fr/osmfr/", "http://c.tile.openstreetmap.org/osmfr/"}, String.valueOf(R.string.copyright_osm), new TileSourcePolicy(2, TileSourcePolicy.FLAG_NO_BULK | TileSourcePolicy.FLAG_NO_PREVENTIVE | TileSourcePolicy.FLAG_USER_AGENT_MEANINGFUL | TileSourcePolicy.FLAG_USER_AGENT_NORMALIZED)));
		mapGActivity.setMapView();

	}

	private boolean geoCitiesGame(final int correctAnswerID) {
		setHeraldic(cityTable.get(rand).getRegion());
		View.OnClickListener listener = new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				resetButton();
				if (v.getTag().equals("" + correctAnswerID)) {
					geoPoints++;
					Utils.readTheWord(ivListenTTS, cityTable.get(rand).getName(),getActivity().getApplicationContext());
					v.setBackgroundColor(Color.GREEN);
					v.animate().setDuration(1000).rotation(360).start();
					tvGameWhere1.setTag("");
					tvGameWhere2.setTag("");
					tvGameWhere3.setTag("");
					tvGameWhere4.setTag("");
					tvGameWhere1.setEnabled(false);
					tvGameWhere2.setEnabled(false);
					tvGameWhere3.setEnabled(false);
					tvGameWhere4.setEnabled(false);
					bNextQuestion.setVisibility(View.VISIBLE);
					ivInfoCIty.setVisibility(View.VISIBLE);
				} else {
					geoPoints--;
					v.setBackgroundColor(Color.RED);
				}
				tvPointsGeo.setText(""+geoPoints);
				if(geoPoints==0) ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumbs_up_down_black_24dp));
				else if(geoPoints>0) ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_up_black_24dp));
				else ivPointsGeo.setBackground(getResources().getDrawable(R.drawable.ic_thumb_down_black_24dp));
			}
		};
		tvGameWhere1.setOnClickListener(listener);
		tvGameWhere2.setOnClickListener(listener);
		tvGameWhere3.setOnClickListener(listener);
		tvGameWhere4.setOnClickListener(listener);

		return answer;
	}

	private void resetButton() {
		tvGameWhere1.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere2.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere3.setBackground(getResources().getDrawable(R.drawable.selector_effect));
		tvGameWhere4.setBackground(getResources().getDrawable(R.drawable.selector_effect));
	}

	private void setHeraldic(final String regName) {
		InputStream is = null;
		try {
			is = getActivity().getAssets().open("reg_heraldic/" + regName + ".png");
			Bitmap bitmap = BitmapFactory.decodeStream(is);
			ivRegHeraldic.setImageBitmap(bitmap);
		} catch (IOException e) {
			ivRegHeraldic.setBackgroundColor(Color.TRANSPARENT);
			e.printStackTrace();
		}
	}
}
