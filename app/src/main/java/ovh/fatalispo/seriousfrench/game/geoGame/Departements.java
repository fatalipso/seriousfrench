package ovh.fatalispo.seriousfrench.game.geoGame;

public class Departements {
	String type, name_reg, name_dep, population, main_dep, superficie, densite, date_creation, num_dep, main_reg, summarize, images, images_expl, copyright_txt, copyright_url;

	public Departements(String type, String name_reg, String name_dep, String population, String main_dep, String superficie, String densite, String date_creation, String num_dep, String main_reg, String summarize, String images, String images_expl, String copyright_txt, String copyright_url) {
		this.type = type;
		this.name_reg = name_reg;
		this.name_dep = name_dep;
		this.population = population;
		this.main_dep = main_dep;
		this.superficie = superficie;
		this.densite = densite;
		this.date_creation = date_creation;
		this.num_dep = num_dep;
		this.main_reg = main_reg;
		this.summarize = summarize;
		this.images = images;
		this.images_expl = images_expl;
		this.copyright_txt = copyright_txt;
		this.copyright_url = copyright_url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName_reg() {
		return name_reg;
	}

	public void setName_reg(String name_reg) {
		this.name_reg = name_reg;
	}

	public String getName_dep() {
		return name_dep;
	}

	public void setName_dep(String name_dep) {
		this.name_dep = name_dep;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getMain_dep() {
		return main_dep;
	}

	public void setMain_dep(String main_dep) {
		this.main_dep = main_dep;
	}

	public String getSuperficie() {
		return superficie;
	}

	public void setSuperficie(String superficie) {
		this.superficie = superficie;
	}

	public String getDensite() {
		return densite;
	}

	public void setDensite(String densite) {
		this.densite = densite;
	}

	public String getDate_creation() {
		return date_creation;
	}

	public void setDate_creation(String date_creation) {
		this.date_creation = date_creation;
	}

	public String getNum_dep() {
		return num_dep;
	}

	public void setNum_dep(String num_dep) {
		this.num_dep = num_dep;
	}

	public String getMain_reg() {
		return main_reg;
	}

	public void setMain_reg(String main_reg) {
		this.main_reg = main_reg;
	}

	public String getSummarize() {
		return summarize;
	}

	public void setSummarize(String summarize) {
		this.summarize = summarize;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getImages_expl() {
		return images_expl;
	}

	public void setImages_expl(String images_expl) {
		this.images_expl = images_expl;
	}

	public String getCopyright_txt() {
		return copyright_txt;
	}

	public void setCopyright_txt(String copyright_txt) {
		this.copyright_txt = copyright_txt;
	}

	public String getCopyright_url() {
		return copyright_url;
	}

	public void setCopyright_url(String copyright_url) {
		this.copyright_url = copyright_url;
	}
}