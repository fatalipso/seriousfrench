package ovh.fatalispo.seriousfrench.game.geoGame;

public class City {
	String region, department, population, name, latitude, longitude;

	public City() {};

	public City(String region, String department, String population, String name, String latitude, String longitude) {
		this.region = region;
		this.department = department;
		this.population = population;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
