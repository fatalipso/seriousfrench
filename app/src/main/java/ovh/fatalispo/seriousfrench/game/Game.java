package ovh.fatalispo.seriousfrench.game;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;

import ovh.fatalispo.seriousfrench.DictionnaryFetcher;
import ovh.fatalispo.seriousfrench.Utils;
import ovh.fatalispo.seriousfrench.game.geoGame.GeoFragment;
import ovh.fatalispo.seriousfrench.R;
import ovh.fatalispo.seriousfrench.game.histoGame.HistoFragment;

import static ovh.fatalispo.seriousfrench.DictionnaryFetcher.LANGAGES;

public class Game extends Activity {
	private SharedPreferences sharedPreferences,sharedPreferencesSettings;
	protected ImageView ivRegHeraldic, ivPointsGeo;
	private Spinner spinner;
	private String levelSelected;
	private ScrollView svMenu;
	private ConstraintLayout clPointsGames, clLevels;
	private Button whereIsIt, whoIsIt, whatMeaning, whenWasIt, ibLevelUp, bGameMenu;
	private boolean hideInfo = true;
	private static String games[] = {"Géo", "Histoire", "Personnages", "Départements", "Préfectures", "Villes"};
	FragmentTransaction ft;
	private static int whichGame;
	private Fragment fragment;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_game);
		sharedPreferencesSettings = getApplicationContext().getSharedPreferences("settings", 0);
		sharedPreferences = getApplicationContext().getSharedPreferences("games", 0);

		whereIsIt = findViewById(R.id.whereIsIt);
		whoIsIt = findViewById(R.id.whoIsIt);
		whatMeaning = findViewById(R.id.whatMeaning);
		whenWasIt = findViewById(R.id.whenWasIt);
		ibLevelUp = findViewById(R.id.ibLevel);
		svMenu = findViewById(R.id.svMenu);
		clLevels = findViewById(R.id.clLevels);
		clPointsGames = findViewById(R.id.clPointsGames);
		bGameMenu = findViewById(R.id.bGameMenu);

		if(savedInstanceState != null) {
			showUI();
		}

		findViewById(R.id.fragGeo).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(ft != null) {
					svMenu.setVisibility(View.INVISIBLE);
					clLevels.setVisibility(View.INVISIBLE);
				}
			}
		});

		bGameMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (hideInfo) {
					svMenu.setVisibility(View.VISIBLE);
					clLevels.setVisibility(View.INVISIBLE);
					hideInfo = false;
				} else {
					svMenu.setVisibility(View.INVISIBLE);
					hideInfo = true;
				}
			}
		});



		whereIsIt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bGameMenu.setVisibility(View.VISIBLE);
				svMenu.setVisibility(View.INVISIBLE);
				ibLevelUp.setVisibility(View.VISIBLE);
				clPointsGames.setVisibility(View.VISIBLE);
				whichGame = 1;
				ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.fragGeo, new GeoFragment(), "Geo Fragment");
				ft.commit();
			}
		});

		whenWasIt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				bGameMenu.setVisibility(View.VISIBLE);
				svMenu.setVisibility(View.INVISIBLE);
				ibLevelUp.setVisibility(View.VISIBLE);
				clPointsGames.setVisibility(View.VISIBLE);
				whichGame = 2;
				ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.fragGeo, new HistoFragment(), "Histo Fragment");
				ft.commit();
			}
		});
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private void showUI() {
		bGameMenu.setVisibility(View.VISIBLE);
		svMenu.setVisibility(View.INVISIBLE);
		ibLevelUp.setVisibility(View.VISIBLE);
		clPointsGames.setVisibility(View.VISIBLE);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		SharedPreferences sharedPreferences = newBase.getSharedPreferences("settings", Context.MODE_PRIVATE);
		DictionnaryFetcher.setLangages();
		if(!sharedPreferences.getBoolean("LanguageHasBeenSet",false)) {
			super.attachBaseContext(Utils.wrap(newBase,Utils.languageInit()));
		} else {
			super.attachBaseContext(Utils.wrap(newBase, LANGAGES[sharedPreferences.getInt("LanguageSet", 19)].getInitial()));
		}
	}
}
