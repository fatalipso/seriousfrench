package ovh.fatalispo.seriousfrench.scheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.AsyncTask;
import android.os.Build;
import androidx.annotation.RequiresApi;

import ovh.fatalispo.seriousfrench.databases.DBAsync;

public class DailyJobScheduler extends JobService {

	private static final String TAG = "SyncService";
	private static AsyncTask downloadJob;

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public boolean onStartJob(final JobParameters params) {
		if(params.getJobId()==666) {

//			downloadJob = new DBAsync(this) {
//				protected void onPostExecute(Boolean success) {
//					jobFinished(params, !success);
//				}
//			}.execute();
		}

		// reschedule the job
		return true;
	}

	@Override
	public boolean onStopJob(JobParameters params) {
		if (downloadJob != null) {
			downloadJob.cancel(true);
		}
		return true;
	}
}
