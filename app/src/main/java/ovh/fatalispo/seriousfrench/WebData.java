package ovh.fatalispo.seriousfrench;

import android.graphics.Bitmap;

public class WebData {
	private String name;
	private String url;
	private Bitmap image;
	private String metadata;
	private String className;

	public WebData(String name, String url, Bitmap image) {
		this.name = name;
		this.image = image;
		this.url = url;
	}

	public WebData(String name, String url, Bitmap image, String metadata) {
		this.name = name;
		this.url = url;
		this.image = image;
		this.metadata = metadata;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
}
