French Immersion

© All rights reserved to Benjamin Dumay Monday, September 30th, 2019  https://fatalispo.fr

Les resources rassemblés, comme les documents CSV, les cartes de Paris et France, etc. m'ont value des heures de travail mais je laisse tout ça en utilisation libre.
 

Cette application n'est pas destinée à être utilisée par le grand public mais seulement à démontrer mes compétences après  quelques mois d'expériences pro. 
Cela permet avant tout de pouvoir suivre ma progression et donc de situer mon niveau à ce moment T par rapport à d'autres applications que je vais être amener à réaliser.

Résumé:
Application où l'on peut visionner/écouter différents médias pour apprendre de la culture française. 
Avant j'avais inclus google translate dans le dictionnaire mais étant facturé, je préfère éviter les mauvaises surprises et ai donc pris la liberté de désactiver la facturation et de passer en commentaire l'appel à l'API. 
En plus de la partie, il y a une partie jeux, liste de mots (ajouter depuis le dictionnaire) et un tableau de bord. 
 
